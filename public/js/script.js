$(".calendar").datepicker({
    dateFormat: "dd-mm-yy",
    maxDate: "now"
});
$(".calendar_1").datepicker({
    dateFormat: "dd-mm-yy",
    minDate: "now" 
});
$(".datepicker").datepicker({
    dateFormat: "dd-mm-yy",
});
$(".from_date").datepicker({
    dateFormat: "dd-mm-yy",
    onSelect: function (selected) {
        var dt = new Date(selected);
        //dt.setDate(dt.getDate() + 1);
        //$(".to_date").datepicker("option", "minDate", dt);
    },
});
$(".to_date").datepicker({
    dateFormat: "dd-mm-yy",
    onSelect: function (selected) {
        var dt = new Date(selected);
        // dt.setDate(dt.getDate() - 1);
        // $(".from_date").datepicker("option", "maxDate", dt);
    },
});
$(".select2").select2();
$(document).on("click", "[data-toggle]", function (event) {
    var dataToggle = $(this).attr("data-toggle");
    if (dataToggle == "modal") {
        var dataTarget = $(this).attr("data-target");
        if (dataTarget) {
            $(dataTarget).modal("show");
        }
    }
});

$(document).on("click", "[data-dismiss]", function (event) {
    var modelParent = $(this).parent().parent().parent().parent().modal("hide");
});

$.validator.setDefaults({
    focusCleanup: false,
    errorClass: "is-invalid",
    errorElement: "div",
    validClass: "is-valid",
});

  

$(function() { 
$("body").on('keydown', '.digitonly',  function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
}); 
