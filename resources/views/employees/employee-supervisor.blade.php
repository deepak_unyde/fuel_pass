<!-- Layout  -->
@extends('layouts.admin')
@php
$page_title = __('messages.list', ['title' => $title]);
$tableHeader = ['#', 'Avatar', 'Emp Code', 'Name', 'Mobile', 'Email', 'Status'];
@endphp

@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <x-card :title="$page_title">
        <x-form with_button="search" method="get" action="{{ route('employee.supervisor') }}">
            <div class="row">
                <x-input name="s" title="Search" placeholder="name" class="col-md-3 col-lg-3 col-xl-2" form_type="v" />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$users" pagination="true">
            @forelse ($users  as $user )

                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @php
                            $avatar = $user->getFirstMediaUrl('avatar', 'thumb');
                        @endphp
                        @if ($avatar)

                            <div class="avatar"><img class="avatar-img avatar-img-small"
                                    src="{{ $avatar }}"><span class=""></span>
                            </div>
                        @endif
                    </td>
                    <td>
                        {{ $user->emp_code }}
                    </td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->mobile }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <x-status text="{{ $user->status }}" />
                    </td>


                </tr>
            @empty
                <x-norecord colspan="11" />
            @endforelse

        </x-table>
    </x-card>


@endsection
