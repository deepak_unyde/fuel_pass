<!-- Layout  -->
@extends('layouts.admin')
@php
$page_title = __('messages.edit', ['title' => $title]);
$href = route('employees.index');
$roles = ['surveyor' => 'Surveyor', 'supervisor' => 'Supervisor'];
$avatar = $user->getFirstMediaUrl('avatar', 'thumb');
$user_has_role = null;
if ($user->roles->first()) {
    $user_has_role = $user->roles->first()->id;
}

@endphp
@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <x-card :title="$page_title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>

        {!! Form::model($user, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form', 'route' => ['employees.update', $user->id]]) !!}
        <x-input name="name" title="Name" label_class="required" />
        <x-input name="emp_code" title="Employee Code" />
        <x-input name="mobile" title="Mobile" type="number" label_class="required" />
        <x-input name="email" title="Email" type="email" label_class="required" />
        <x-input name="avatar" title="Profile Image" type="file" :selected="$avatar" />
        <x-input name="about_me" title="About Me" type="textarea" />
        <x-select name="role" title="Employee Type" :options="$roles" selected="{{ getEmployeeRole($user_has_role) }}" />
        <x-status />

        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {

                }

            });
        });
    </script>
@endpush
