<!-- Layout  -->
@extends('layouts.admin')
@php
$page_title = __('messages.list', ['title' => $title]);
$href = route('employees.create');
$tableHeader = ['#', 'Avatar', 'Emp Code', 'Name', 'Mobile', 'Email', 'Employee Type', 'Status', 'Created At', 'Action'];
$roles = ['3' => 'Surveyor', 'Supervisor'];
@endphp

@section('title')
    {{ $page_title }}
@endsection
@section('content')
    <x-card :title="$page_title">
        @can('vendor_level')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => $title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('employees.index') }}">
            <div class="row">
                <x-select name="role" title="Employee Type" :options="$roles" class="col-md-3 col-lg-3 col-xl-2"
                    form_type="v" />
                <x-input name="s" title="Search" placeholder="name,email" class="col-md-3 col-lg-3 col-xl-2" form_type="v" />
                <x-status search />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$users" pagination="true">
            @forelse ($users  as $user )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @php
                            $avatar = $user->getFirstMediaUrl('avatar', 'thumb');
                        @endphp
                        @if ($avatar)

                            <div class="avatar"><img class="avatar-img avatar-img-small"
                                    src="{{ $avatar }}"><span class=""></span>
                            </div>
                        @endif
                    </td>
                    <td>
                        {{ $user->emp_code }}
                    </td>
                    <td>
                        <div>{{ $user->name }}</div>
                    </td>
                    <td>{{ $user->mobile }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if (!empty($user->getRoleNames()))
                            @foreach ($user->getRoleNames() as $v)
                                <label class="mr-4">{{ $v }}</label>
                            @endforeach
                        @endif
                    </td>



                    <td>

                        <x-status text="{{ $user->status }}" />

                    </td>
                    <td>
                        <x-dt :date_time="$user->created_at" format="j M y" />


                    </td>

                    <td class="w-20">

                        @can('vendor_level')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('employees.edit', $user->id) }}" />
                        @endcan

                        @can('vendor_level')
                            <x-delete class="mt-2" action="{{ route('employees.destroy', $user->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="11" />
            @endforelse

        </x-table>
    </x-card>
@endsection
