@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {
                    name: {
                        required: true
                    },
                    mobile: {
                        minlength: 10,
                        maxlength: 10,
                        digits: true,
                        required: true
                    },
                    email: {
                        email: true,
                        required: true,
                    },
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 30,
                    },
                    password_confirmation: {
                        equalTo: "#password",
                        minlength: 8,
                        maxlength: 30,
                    },
                    role: {
                        required: true
                    },
                }

            });
        });
    </script>
@endpush
