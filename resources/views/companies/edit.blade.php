<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('companies.index');
$company_logo = $company->getFirstMediaUrl('company_logo');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($company, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form', 'route' => ['companies.update', $company->id]]) !!}
        <x-input name="company_name" title="Company name" label_class="required" />
            <x-input name="company_mobile" title="Company Mobile" label_class="required" input_class="digitonly" />
            <x-input name="company_email" title="Company Emailid (Username)" label_class="required" readonly="readonly" />
            <x-input name="company_logo" title="Company Logo" type="file" :selected="$company_logo" />
            <x-input type="textarea" name="company_address" title="Company address" />
            <x-status />
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('companies.common')
