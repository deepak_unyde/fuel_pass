@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({

                rules: {
                    company_name: {
                        required: true,
                    },
                    company_email: {
                        email: true,
                        required: true,
                    },
                    
                    company_mobile: {
                        minlength: 10,
                        maxlength: 10,
                        digits: true
                    },

                    company_address: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                    
                }
                 
            });
        });
         
    </script>
@endpush
