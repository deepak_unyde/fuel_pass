<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('companies.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        <x-form method="post" enctype="multipart/form-data" action="{{ route('companies.store') }}">
            <x-input name="company_name" title="Company name" label_class="required" />
            <x-input name="company_mobile" title="Company Mobile" label_class="required" input_class="digitonly" />
            <x-input name="company_email" title="Company Emailid (Username)" label_class="required" />
            <x-input name="company_logo" title="Company Logo" type="file" label_class="required" />
            <x-input name="password" title="Password" type="password" label_class="required" />
            <x-input name="password_confirmation" title="Confirm Password" type="password" label_class="required" />
            <x-input type="textarea" name="company_address" title="Company address" />
            <x-status />
            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label"></label>
                <div class="col-sm-8">
                    <div class="form-check">
                        <input class="form-check-input" id="login_detail" name="login_detail" value="1" type="checkbox">
                        <label class="form-check-label" for="login_detail">Login detail send</label>
                    </div>
                </div>
            </div>
            
        </x-form>
    </x-card>
@endsection
@include('companies.common')
