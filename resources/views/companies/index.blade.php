<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('companies.create');

$tableHeader = ['#', 'Logo', 'Name','Mobile','Email Id', 'Address', 'Status', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('company-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => $title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        @if(loginUserRoleId()==1)
        <x-form with_button="search" method="get" action="{{ route('companies.index') }}">
            <div class="row">
                <x-input name="s" title="Company name" class="col-md-4 col-lg-4 col-xl-3" form_type="v" />
                <x-status search />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>
        </x-form>
        @endif
        <x-table :table_header="$tableHeader" :data="$companies" pagination="true">
            @forelse ($companies  as $company )
                @php
                
                 $company_logo =   ($company->getFirstMediaUrl('company_logo'))?$company->getFirstMediaUrl('company_logo'):asset('img/default.png');
                @endphp
                <tr>
                    <td>{{ ++$i }}</td>
                    <td> <img class="avatar-img avatar-img-small"
                        src="{{ $company_logo }}"><span class=""></span></td>
                    <td>{{ $company->company_name }}</td>
                    <td>{{ $company->company_mobile }}</td>
                    <td>{{ $company->company_email }}</td>
                    <td class="col-md-2">{{ $company->company_address }}</td>
                    <td>
                        <x-status text="{{ $company->status }}" />
                    </td>
                    <td>
                        <x-dt :date_time="$company->created_at" />

                    </td>
                    <td>
                        @can('company-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('companies.edit', $company->id) }}" />
                        @endcan
                        @can('company-delete')
                            <x-delete action="{{ route('companies.destroy', $company->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="9" />
            @endforelse

        </x-table>
    </x-card>
@endsection
