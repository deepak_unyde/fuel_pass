<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => 'Permission']);
$href = route('permissions.index');
$options = $group_names;
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($permission, ['method' => 'PUT', 'class' => 'form', 'route' => ['permissions.update', $permission->id]]) !!}
        <x-select name="group_name" title="Group Name" label_class="required" type="select" :options="$options" />
        <x-input name="group_name_new" title="Enter new group name" label="false" container_class="d-none" />
        <x-input name="name" title="Permission name" label_class="required" />
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $('#group_name').change(function(e) {
                if ($(this).val() == "add_new") {

                    $('#group_name_new-input-container').removeClass('d-none').show();
                } else {
                    $('#group_name_new-input-container').hide();
                }
            });

            $(".form").validate({
                rules: {
                    group_name: "required",
                    group_name_new: 'required',
                    name: "required",
                }

            });
        });
    </script>
@endpush
