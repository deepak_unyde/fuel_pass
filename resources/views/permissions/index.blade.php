<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => 'Permission']);
$href = route('permissions.create');

$tableHeader = ['#', 'Group Name', 'Permission', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('permission-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => 'Permission']) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('permissions.index') }}">
            <div class="row">
                <x-select name="group_name" title="Group" :options="$group_names" form_type="v" />
                <x-input name="s" title="Permission" class="col-md-3 col-lg-3 col-xl-2" form_type="v" />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$permissions" pagination="true">
            @forelse ($permissions  as $permission )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $permission->group_name }}</td>
                    <td>{{ $permission->name }}</td>
                    <td>
                        <x-dt :date_time="$permission->created_at" />


                    </td>
                    <td>
                        @can('permission-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('permissions.edit', $permission->id) }}" />
                        @endcan
                        @can('permission-delete')
                            <x-delete action="{{ route('permissions.destroy', $permission->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="5" />
            @endforelse

        </x-table>
    </x-card>
@endsection
