<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => 'Role']);
$href = route('roles.create');

$tableHeader = ['#', 'Role', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('role-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => 'role']) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('roles.index') }}">
            <div class="row">
                <x-input name="s" title="Role" class="col-md-3 col-lg-3 col-xl-2" form_type="v" />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$roles" pagination="true">
            @forelse ($roles  as $role )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $role->name }}</td>
                    <td>
                        <x-dt :date_time="$role->created_at" />


                    </td>
                    <td>
                        @can('role-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('roles.edit', $role->id) }}" />
                        @endcan
                        @can('role-delete')
                            <x-delete action="{{ route('roles.destroy', $role->id) }}" />
                        @endcan

                    </td>
                </tr>
            @empty
                <x-norecord colspan="4" />
            @endforelse

        </x-table>
    </x-card>
@endsection
