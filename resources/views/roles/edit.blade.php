<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => 'Role']);
$href = route('roles.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($role, ['method' => 'PUT', 'class' => 'form', 'route' => ['roles.update', $role->id]]) !!}
        <x-input name="name" title="Role name" label_class="required" />
        <div class="mb-3 row">
            <label class="col-sm-4 col-form-label required">Permission</label>
            <div class="col-sm-8">

                @if ($permission_lists)
                    @foreach ($permission_lists as $group_name => $permission_list)
                        <ul class="list-group mb-3">
                            <li class="list-group-item">{{ $group_name }}</li>
                            <li class="list-group-item">
                                @foreach ($permission_list as $permission)
                                    <div class="form-check form-check-inline">
                                        {{ Form::checkbox('permission[]',$permission->id,in_array($permission->id, $role_has_permissions) ? true : false,['class' => 'form-check-input', 'id' => $permission->id]) }}


                                        <label class="form-check-label"
                                            for="{{ $permission->id }}">{{ $permission->name }}</label>
                                    </div>
                                @endforeach
                            </li>
                        </ul>
                    @endforeach
                @endif






            </div>
        </div>
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {
                    name: "required",
                    "permission[]": "required",
                }

            });
        });
    </script>
@endpush
