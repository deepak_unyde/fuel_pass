<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => 'Role']);
$href = route('roles.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        <x-form method="post" action="{{ route('roles.store') }}">
            <x-input name="name" title="Role name" label_class="required" />
            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label required">Permission</label>
                <div class="col-sm-8">

                    @if ($permission_lists)
                        @foreach ($permission_lists as $group_name => $permission_list)
                            <ul class="list-group mb-3">
                                <li class="list-group-item">{{ $group_name }}</li>
                                <li class="list-group-item">
                                    @foreach ($permission_list as $permission)
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" name="permission[]" id="{{ $permission->id }}"
                                                type="checkbox" value="{{ $permission->id }}">
                                            <label class="form-check-label"
                                                for="{{ $permission->id }}">{{ $permission->name }}</label>
                                        </div>
                                    @endforeach
                                </li>
                            </ul>
                        @endforeach
                    @endif






                </div>
            </div>
        </x-form>
    </x-card>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {
                    name: "required",
                    "permission[]": "required",
                }

            });
        });
    </script>
@endpush
