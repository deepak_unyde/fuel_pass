<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        
        <x-form with_button="search" method="get" action="{{ route('transactions.index') }}">
            <div class="row">
           
                @if(loginUserRoleId()==1)
               
                <x-select name="package_id" title="Package" :options="$package" form_type="v"  class="col-md-3" />
                <x-input name="s" title="Payment Reciept number" class="col-md-3 col-lg-4 col-xl-3" form_type="v" />
                <x-select name="company_id" title="Company" :options="$companies" form_type="v"  class="col-md-3"  input_class="company_petrol_pump" />

                @endif
                <x-select name="petrol_pump_id" title="Petrol pump" :options="$petrolPump" form_type="v"  class="col-md-3" input_class="petrol_pump"  />
                <div class="col-md-2 form-group col-md-2 col-lg-2 col-xl-2">
                    <label for="form_date" class="">From Date</label>
                    <input type="text" class="form-control calendar" placeholder="From Date" name="from_date" value="<?= (app('request')->input('from_date'))?app('request')->input('from_date'):date('d-m-Y',strtotime('-7 days'))?>"  />
                </div>

                <div class="col-md-2 form-group col-md-3 col-lg-3 col-xl-2">
                    <label for="to_date" class="">To Date </label>
                    <input type="text" class="form-control calendar" placeholder="To Date" name="to_date" value="<?= (app('request')->input('to_date'))?app('request')->input('to_date'):date('d-m-Y')?>"  />
                </div>
                <x-payment_status search />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>

        <div class="table-responsive">
         
        <table id="myTable" class="table table-striped table-hover mt-4" pagination="true">
            <thead>
			       <tr class="table-dark">
						<?php 
						if(!empty($transactions))
						 		if(!empty($tableHeader)){
									$i=0;
									foreach($tableHeader as $title){
                                        if($i==0)
                                        echo "<th>#</th>";
                                        else
								    	echo "<th>".ucwords(str_replace("_"," ",$title))."</th>";
									$i++;}
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php 
					for($i=0;$i<count($transactions);$i++){
                        echo "<tr>";
                      $recorddata=$transactions[$i];
                      for($j=0;$j<count($tableHeader);$j++){
                            $key = $tableHeader[$j];
                            if($j==0) 
                            echo "<td>". $i+1 ."</td>";
                            else
                            echo "<td>".$recorddata->$key."</td>";
                      }
							echo "</tr>";
					}
				    ?>
			    </tbody>
			</table>
        </div> 
        </x-card>
        
@endsection 


