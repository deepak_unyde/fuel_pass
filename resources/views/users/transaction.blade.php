<!-- Layout  -->
@php
$tableHeader = ['#',  'Package','Petrol Pump', 'Amount','Reward','Cashback','Status', 'Payment Id', 'Created At'];
$status = array("pending"=>"Pending","success"=>"Success","failed"=>"Failed");
$transaction_title = 'User Transaction History (Cashback & Rewards)';
@endphp
    <x-card :title="$transaction_title">
    <x-form with_button="search" method="get" action="{{ route('users.show', $user->id) }}">
            <div class="row">
            <x-select name="package_id" title="Package" :options="$package" form_type="v"  class="col-md-2" />
            <x-select name="petrol_pump_id" title="Petrol Pump" :options="$petrolPumps" form_type="v"  class="col-md-2" />
                <div class="col-md-2 form-group col-md-3 col-lg-3 col-xl-2">
                    <label for="form_date" class="">From Date</label>
                    <input type="text" class="form-control from_date" placeholder="From Date" name="from_date" value="<?= app('request')->input('from_date')?>"  />
                
                </div>

                <div class="col-md-2 form-group col-md-3 col-lg-3 col-xl-2">
                    <label for="to_date" class="">To Date </label>
                    <input type="text" class="form-control to_date" placeholder="To Date" name="to_date" value="<?= app('request')->input('to_date')?>"  />
                
                </div>
                <x-select name="status" title="Status" :options="$status" form_type="v"  class="col-md-2" />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$rewards" pagination="true">
            @forelse ($rewards  as $reward )
                <tr>
                    <td>{{ ++$i }}</td>
                    
                    <td>{{ $reward->package->package_name }}</td>
                    <td>
                        {{ $reward->petrolPump->name }}
                     ({{ $reward->petrolPump->company->company_name }})
                    </td> 
                    
                    <td>{{ $reward->amount }}</td>
                    <td>{{ $reward->reward_point }}</td>
                    <td>{{ $reward->cashback }}</td>
                    <td>{{ Ucfirst($reward->status) }}</td> 
                    <td>{{ $reward->payment_no }}</td>
                      
                    <td>
                        <x-dt :date_time="$reward->created_at" />

                    </td>
                     
                </tr>
            @empty
                <x-norecord colspan="11" />
            @endforelse

        </x-table>
    </x-card> 