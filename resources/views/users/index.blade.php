<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => 'User']);
$href = route('users.create');
$tableHeader = ['#', 'Avatar', 'Name', 'Mobile', 'Email', 'Role', 'Status', 'Created At', 'Last Login', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('user-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => 'user']) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('users.index') }}">
            <div class="row">
                <x-select name="role" title="Role" :options="$roles" class="col-md-3 col-lg-3 col-xl-2" form_type="v" />
                <x-input name="s" title="Search" placeholder="name,email" class="col-md-3 col-lg-3 col-xl-2" form_type="v" />
                <x-input name="mobile" title="Mobile" placeholder="Mobile No." class="col-md-3 col-lg-3 col-xl-2" form_type="v" />
                <x-status search />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$users" pagination="true">
            @forelse ($users  as $user)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @php
                            $avatar = $user->getFirstMediaUrl('avatar', 'thumb');
                        @endphp
                        @if ($avatar)
                            <div class="avatar"><img class="avatar-img avatar-img-small"
                                    src="{{ $avatar }}"><span class=""></span>
                            </div>
                        @endif
                    </td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->mobile }}</td>
                    <td>{{ $user->email }}</td>
                    <td>

                        @if (!empty($user->getRoleNames()))
                            @foreach ($user->getRoleNames() as $v)
                                <label class="mr-4">{{ $v }}</label>
                            @endforeach
                        @endif
                    </td>
                    <td>

                        <x-status text="{{ $user->status }}" />

                    </td>
                    <td>
                        <x-dt :date_time="$user->created_at" format="j M y" />


                    </td>
                    <td>
                        <x-dt :date_time="$user->lastLoginAt()" />

                    </td>
                    <td>
                        @can('user-edit')
                        <x-link title="View" icon="fa-eye" href="{{ route('users.show', $user->id) }}" />
                            <x-link title="Edit" icon="fa-edit" href="{{ route('users.edit', $user->id) }}" />
                        @endcan
                        @can('user-delete')
                            <x-delete action="{{ route('users.destroy', $user->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="10" />
            @endforelse

        </x-table>
    </x-card>
@endsection
