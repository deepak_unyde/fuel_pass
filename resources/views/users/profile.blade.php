@extends('backend.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    @include('backend.shared.alert')


                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            Profile
                        </div>
                        @php
                            $avatar = $user->getFirstMediaUrl('avatar', 'thumb');
                        @endphp

                        {!! Form::model($user, ['method' => 'patch', 'enctype' => 'multipart/form-data', 'route' => ['profile.update']]) !!}
                        <div class="card-body">
                            @include('backend.shared.ui.text',['label'=>'Name','name'=>'name','required'=>true])
                            @include('backend.shared.ui.email',['label'=>'Email','name'=>'email','required'=>true])
                            @include('backend.shared.ui.file',['label'=>'Profile
                            Image','name'=>'avatar','url'=>$avatar])
                            @include('backend.shared.ui.textarea',['label'=>'About Me','name'=>'about_me'])
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit"> Save</button>
                        </div>
                        {!! Form::close() !!}

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
