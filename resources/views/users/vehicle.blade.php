<!-- Layout  -->
@php
$tableHeader = ['#',  'Registration No', 'Fuel Type', 'Vehicle Type', 'Created At'];
$vehicle_title = 'User Vehicle List';
@endphp
    <x-card :title="$vehicle_title">
       
    <x-table :table_header="$tableHeader" :data="$vehicles" pagination="true">
            @forelse ($vehicles  as $vehicle )
                <tr>
                    <td>{{ ++$i }}</td>
                     
                    <td>{{ $vehicle->registration_number }}</td>
                    <td>{{ $vehicle->fuel_type }}</td>
                    <td>{{ $vehicle->vehicle_type }}</td>
                      
                    <td>
                        <x-dt :date_time="$vehicle->created_at" />
                    </td>
                     
                </tr>
            @empty
                <x-norecord colspan="5" />
            @endforelse

        </x-table>
    </x-card> 