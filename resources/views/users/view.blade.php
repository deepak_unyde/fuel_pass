<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.detail', ['title' => 'User Details']);
$href = route('users.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
             
        <div class="mb-3 row">
            
            <div class="col-sm-2"><strong>Name :</strong></div>
            <div class="col-sm-2">{{$userDetails->name}}</div>

            <div class="col-sm-2"><strong>Email Id :</strong></div>
            <div class="col-sm-2">{{$userDetails->email}}</div>

            <div class="col-sm-2"><strong>Contact No :</strong></div>
            <div class="col-sm-2">{{$userDetails->mobile}}</div>
            
        </div>

        <div class="mb-3 row">
            
            <div class="col-sm-2"><strong>Active Package :</strong></div>
            <div class="col-sm-2">{{$userDetails->package_name}}</div>

            <div class="col-sm-2"><strong>Package Active From :</strong></div>
            <div class="col-sm-2">{{$userDetails->from_date}}</div>

            <div class="col-sm-2"><strong>Package Expire To :</strong></div>
            <div class="col-sm-2">{{$userDetails->to_date}}</div>

            <div class="col-sm-2"><strong>Package Price :</strong></div>
            <div class="col-sm-2">{{$userDetails->cost}}</div>

            <div class="col-sm-2"><strong>Package Status :</strong></div>
            <div class="col-sm-2">{{$userDetails->planStatus}}</div>

            <div class="col-sm-2"><strong>Package Remaining Days :</strong></div>
            <div class="col-sm-2">{{$userDetails->remainingDays}} Days</div>

            <div class="col-sm-2"><strong>Package Reword point per ltr :</strong></div>
            <div class="col-sm-2">{{$userDetails->package_reward}} </div>

            <div class="col-sm-2"><strong>Package Cashback per ltr :</strong></div>
            <div class="col-sm-2">{{$userDetails->package_cashback}} </div>
            
        </div>

        <div class="mb-3 row">
            
            <div class="col-sm-2"><strong>User Wallet :</strong></div>
            <div class="col-sm-2">{{$userDetails->wallet_amount}}</div>

            <div class="col-sm-2"><strong>Reward Point :</strong></div>
            <div class="col-sm-2">{{$userDetails->reward_point}}</div>

            <div class="col-sm-2"><strong>Cashback Amount :</strong></div>
            <div class="col-sm-2">{{$userDetails->cashback}}</div>
            
        </div>
         <hr>
 

        <div class="mt-3 mb-4 row">
            <div class="col-sm-12">
            <ul class="nav nav-tabs"  role="tablist">
                <li class="nav-item">
                <a class="nav-link active" data-coreui-toggle="tab" role="tab" href="#wallet">Wallet</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" data-coreui-toggle="tab" role="tab" href="#vehicle">Vehicle</a>
                </li>
                <li class="nav-item">
                <a class="nav-link"  data-coreui-toggle="tab" role="tab"  href="#mileage">Mileage</a>
                </li>
                <li class="nav-item">
                <a class="nav-link"  data-coreui-toggle="tab" role="tab"  href="#transaction">Transactions</a>
                </li>
                <li class="nav-item">
                <a class="nav-link"  data-coreui-toggle="tab" role="tab"  href="#package">User Package</a>
                </li>

                <li class="nav-item">
                <a class="nav-link"  data-coreui-toggle="tab" role="tab"  href="#redeem">Redeem Gift</a>
                </li>

                <li class="nav-item">
                <a class="nav-link"  data-coreui-toggle="tab" role="tab"  href="#services">Services</a>
                </li>
            </ul>
            </div>
            <div class="mb-3 col-md-12">

            <div class="tab-content">
            <div id="wallet" class="container tab-pane active"><br>
                 @include('users.wallet')
            </div>
            <div id="vehicle" class="container tab-pane"><br>
            @include('users.vehicle')
            </div>
            <div id="mileage" class="container tab-pane"><br>
            @include('users.mileage')
            </div>

            <div id="transaction" class="container tab-pane"><br>
            @include('users.transaction')
            </div>
            <div id="package" class="container tab-pane"><br>
            @include('users.package')
            </div>
            <div id="redeem" class="container tab-pane"><br>
            
            @include('users.redeem')
            </div>

            <div id="services" class="container tab-pane"><br>
            @include('users.services')
            </div>
            

        </div>
            </div>
        </div>

    </x-card>
@endsection
 
