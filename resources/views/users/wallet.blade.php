<!-- Layout  -->
@php
$tableHeader = ['#',  'Wallet Amount', 'Created At'];
$wallte_title = 'User Wallet List';
@endphp
    <x-card :title="$wallte_title">
        <x-table :table_header="$tableHeader" :data="$wallets" pagination="true">
            @forelse ($wallets  as $wallet )
                <tr>
                    <td>{{ ++$i }}</td>
                     
                    <td>{{ $wallet->amount }}</td>
                      
                    <td>
                        <x-dt :date_time="$wallet->created_at" />
                    </td>
                    
                </tr>
            @empty
                <x-norecord colspan="3" />
            @endforelse

        </x-table>
    </x-card> 