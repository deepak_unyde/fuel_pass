<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => 'User']);
$href = route('users.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        <x-form method="post" enctype="multipart/form-data" action="{{ route('users.store') }}">
            <x-input name="name" title="Name" label_class="required" />
            <x-input name="mobile" title="Mobile" type="number" />
            <x-input name="email" title="Email" type="email" label_class="required" />
            <x-input name="password" title="Password" type="password" label_class="required" />
            <x-input name="password_confirmation" title="Confirm Password" type="password" label_class="required" />
            <x-input name="avatar" title="Profile Image" type="file" />
            <x-input name="about_me" title="About Me" type="textarea" />
            <x-select name="role" title="Role" label_class="required" :options="$roles" />
            <x-status />
            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label"></label>
                <div class="col-sm-8">
                    <div class="form-check">
                        <input class="form-check-input" id="login_detail" name="login_detail" value="1" type="checkbox">
                        <label class="form-check-label" for="login_detail">Login detail send</label>
                    </div>
                </div>
            </div>
        </x-form>
    </x-card>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {

                }

            });
        });
    </script>
@endpush
