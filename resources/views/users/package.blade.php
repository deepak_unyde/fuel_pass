<!-- Layout  -->
@php
$tableHeader = ['#','Package', 'Payment No', 'Cashback', 'Reward', 'Cost', 'From date', 'To date', 'Created At'];
$package_title = 'User Purchase Package List';
@endphp
    <x-card :title="$package_title">
       
    <x-table :table_header="$tableHeader" :data="$userPackage" pagination="true">
        @forelse ($userPackage   as $package )
            <?php   
           if($package->packages->count() > 0){
            $myPackage = array();
            $myPackage = $package->packages;
           
                for($j=0; $j < count($myPackage);$j++)
                {  ?>
                <tr>
                    <td>{{ $j+1 }}</td>
                    <td>{{ $myPackage[$j]->package_name }}</td>
                    <td>{{ $myPackage[$j]->payment_no }}</td>
                    <td>{{ $myPackage[$j]->cashback }}</td>
                    <td>{{ $myPackage[$j]->reward_point }}</td>
                    <td>{{ $myPackage[$j]->cost }}</td>
                    <td>{{ $myPackage[$j]->from_date }}</td>
                    <td>{{ $myPackage[$j]->to_date }}</td>
                    <td>
                        <x-dt :date_time="$myPackage[$j]->created_at" />
                    </td>
                </tr>
              <?php  } } else { ?>
                <x-norecord colspan="9" />
               <?php  }?> 
            @empty
                <x-norecord colspan="9" />
            @endforelse

        </x-table>
    </x-card> 