<!-- Layout  -->
@php
$serviceTableHeader = ['#','Service','Cost','Pickup Address','Service Date','Payment Status','Booking Date'];
$services = $user->service;
$service_title = 'User Service List';
@endphp
    <x-card :title="$service_title"> 
    <div class="table-responsive">
        <table class="table table-striped table-hover mt-4">
            <thead>
                <tr class="table-dark ">
                    <th>#</th>
                    <th>Service</th>
                    <th>Cost</th>
                    <th>Pickup Address</th>
                    <th>Service Date</th>
                    <th>Payment Status</th>
                    <th>Booking Date</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($services  as $service )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $service->service_name }}</td>
                    <td>{{ $service->amount }}</td>
                    <td>{{ $service->pivot->pickup_address }}</td>
                    <td>{{ $service->pivot->service_date }}</td>
                    <td>{{ $service->pivot->payment_status }}</td>
                    <td><x-dt :date_time="$service->created_at" /></td>
                </tr>
            @empty
                <x-norecord colspan="7" />
            @endforelse
            </tbody>
        </table>
    </div>

        
    </x-card> 