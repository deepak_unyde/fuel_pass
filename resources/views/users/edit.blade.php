<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => 'User']);
$href = route('users.index');
$avatar = $user->getFirstMediaUrl('avatar', 'thumb');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>

        {!! Form::model($user, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form', 'route' => ['users.update', $user->id]]) !!}
        <x-input name="name" title="Name" label_class="required" />
        <x-input name="mobile" title="Mobile" type="number" />
        <x-input name="email" title="Email" type="email" label_class="required" />
        <x-input name="avatar" title="Profile Image" type="file" :selected="$avatar" />
        <x-input name="about_me" title="About Me" type="textarea" />
        <!--x-select name="role" title="Role" label_class="required" :options="$roles" :selected="$user_has_role" /-->
        <x-status />
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {

                }

            });
        });
    </script>
@endpush
