<!-- Layout  -->
@php
$tableHeader = ['#','Redeem Code', 'Redeem Gift', 'Redeem Points', 'Used Date', 'Redeem Status', 'Created At'];
$package_title = 'User Redeem List';
@endphp
    <x-card :title="$package_title">
       
    <x-table :table_header="$tableHeader" :data="$userPackage" pagination="true">
        @forelse ($userPackage   as $package )
            <?php 
           if($package->redeem->count() > 0){
            $myRedeemList = array();
            $myRedeemList = $package->redeem;
                for($j=0; $j < count($myRedeemList);$j++)
                { if($myRedeemList[$j]->pivot->status==0) $status= "Not Use" ;else $status =  "Used" ; ?>
                <tr>
                    <td>{{ $j+1 }}</td>
                    <td>{{ $myRedeemList[$j]->pivot->redeem_code }}</td>
                    <td>{{ $myRedeemList[$j]->gift_name }}</td>
                    <td>{{ $myRedeemList[$j]->pivot->redeem_point }}</td>
                    <td>@if($myRedeemList[$j]->pivot->use_date==null || $myRedeemList[$j]->pivot->use_date=='0000-00-00') {{ 'NA' }} @else {{$myRedeemList[$j]->pivot->use_date}} @endif </td>
                    <td>{{$status}}</td> 
                    <td>
                        <x-dt :date_time="$myRedeemList[$j]->created_at" />
                    </td>
                </tr>
              <?php  } } else { ?>
                <x-norecord colspan="8" />
               <?php  }?> 
            @empty
                <x-norecord colspan="8" />
            @endforelse

        </x-table>
    </x-card> 