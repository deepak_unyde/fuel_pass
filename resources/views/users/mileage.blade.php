<!-- Layout  -->
@php
$tableHeader = ['#',  'Meter Reading', 'Created At' ];
$mileage_title = 'User Mileage List';
@endphp
    <x-card :title="$mileage_title">
       
    <x-table :table_header="$tableHeader" :data="$mileages" pagination="true">
            @forelse ($mileages  as $mileage )
                <tr>
                    <td>{{ ++$i }}</td>
                     
                    <td>{{ $mileage->meter_reading }}</td>
                      
                    <td>
                        <x-dt :date_time="$mileage->created_at" />
                    </td>
                     
                </tr>
            @empty
                <x-norecord colspan="3" />
            @endforelse

        </x-table>
    </x-card> 