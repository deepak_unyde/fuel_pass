<!-- Layout  -->
@extends('layouts.admin')
@php
$title = 'Change Password';
$href = route('users.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-form method="post" action="{{ route('update-password') }}">
            <x-input name="password" id="password" title="Password" type="password" label_class="required" />
            <x-input name="password_confirmation" title="Confirm Password" type="password" label_class="required" />
        </x-form>
    </x-card>
@endsection

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 30
                    },
                    password_confirmation: {
                        equalTo: "#password"
                    }
                }

            });
        });
    </script>
@endpush
