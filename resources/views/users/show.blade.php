@extends('backend.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    


                    <div class="card">
                        @include('backend.shared.card-header',['card_type'=>'show','title'=>'User','url'=>'users.index'])
                        @php
                            $avatar = $user->getFirstMediaUrl('avatar', 'thumb');
                        @endphp
                        <div class="card-body">
                            @include('backend.shared.ui.show',['label'=>'Name','value'=>$user->name])
                            @include('backend.shared.ui.show',['label'=>'Email','value'=>$user->email])
                            @include('backend.shared.ui.show',['label'=>'Profile
                            Image','name'=>'avatar','url'=>$avatar])
                        </div>
                        <div class="card-footer">

                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection


@section('javascript')

@endsection
