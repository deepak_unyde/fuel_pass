<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$tableHeader = ['#', 'Name','Phone','Package','Petrol Pump', 'Amount','Reward','Cashback','Status', 'Payment Id', 'Created At'];
$status = array("success"=>"Success","pending"=>"Pending","failed"=>"Failed");



@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
    <!--<x-slot name="header_right">
                 Total Amount: 0 &nbsp;&nbsp;&nbsp;&nbsp; Total Reward Points: 0 &nbsp;&nbsp;&nbsp;&nbsp; Total Cashback: 0
            
       
            </x-slot>-->
        <x-form with_button="search" method="get" action="{{ route('transactions.index') }}">
            <div class="row">
            <x-select name="user_id" title="User" :options="$users" form_type="v"  class="col-md-2" />
            <x-select name="petrol_pump_id" title="Petrol Pump" :options="$petrolPumps" form_type="v"  class="col-md-2" />
                

                <div class="col-md-2 form-group col-md-3 col-lg-3 col-xl-2">
                    <label for="form_date" class="">From Date</label>
                    <input type="text" class="form-control from_date" placeholder="From Date" name="from_date" value="<?= app('request')->input('from_date')?>"  />
                
                </div>

                <div class="col-md-2 form-group col-md-3 col-lg-3 col-xl-2">
                    <label for="to_date" class="">To Date </label>
                    <input type="text" class="form-control to_date" placeholder="To Date" name="to_date" value="<?= app('request')->input('to_date')?>"  />
                
                </div>
                

    <div class="form-group col-md-2">
        <label for="status" class="">Status</label>
        <select class=" form-select  col-md-2" id="status" name="status">
            <option value="success"<?php  if(app('request')->input('status')=='success') echo "selected='selected'";?> >Success</option>
            <option value="pending" <?php  if(app('request')->input('status')=='pending') echo "selected='selected'";?>>Pending</option>
            <option value="failed" <?php  if(app('request')->input('status')=='failed') echo "selected='selected'";?>>Failed</option>
        </select>
         
    </div>
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$rewards" pagination="true">
            @forelse ($rewards  as $reward )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $reward->user->name }}</td>
                    <td>{{ $reward->user->mobile }}</td>
                    <td>{{ $reward->package->package_name }}</td>
                    <td>
                        {{ $reward->petrolPump->name }}
                     ({{ $reward->petrolPump->company->company_name }})
                    </td> 
                    
                    <td>{{ $reward->amount }}</td>
                    <td>{{ $reward->reward_point }}</td>
                    <td>{{ $reward->cashback }}</td>
                    <td>{{ Ucfirst($reward->status) }}</td> 
                    <td>{{ $reward->payment_no }}</td>
                      
                    <td>
                        <x-dt :date_time="$reward->created_at" />
                    </td>
                     
                </tr>
            @empty
                <x-norecord colspan="14" />
            @endforelse

        </x-table>
    </x-card>
@endsection
