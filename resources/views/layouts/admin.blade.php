<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <base href="{{ url('/') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>@yield('title') | {{ config('app.name') }}</title>
    @stack('before-styles')
    <!-- Vendors styles-->
    <link rel="stylesheet" href="{{ asset('css/simplebar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery/jquery-ui.css') }}">
    <link rel="shortcut icon" href="{{ url('/fav.png') }}">
    <!-- Main styles for this application-->
    <link href="{{ asset('css/style.css?v=') }}{{ time() }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    @stack('after-styles')
</head>

<body>
    @include('partials.sidebar')
    <div class="wrapper d-flex flex-column min-vh-100 bg-light">
        @include('partials.header')
        <div class="body flex-grow-1 px-3">
            <div class="container-lg">
                @if (Session::has('success'))
                    <x-alert>{{ Session::get('success') }}</x-alert>
                @elseif (Session::has('error'))
                    <x-alert type="danger">{{ Session::get('error') }}</x-alert>
                @endif
                @if ($errors->any())
                    <x-alert type="danger">
                        <p><strong>Opps Something went wrong</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </x-alert>
                @endif
                @yield('content')
            </div>
        </div>
        @include('partials.footer')
    </div>

    @stack('before-scripts')
    <script src="{{ asset('js/coreui.bundle.min.js') }}"></script>
    <script src="{{ asset('js/simplebar.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('vendor/lightbox/css/lightbox.min.css') }}">
    <script src="{{ asset('vendor/lightbox/js/lightbox.js') }}"></script>
     
<!-- Select2 Js -->
<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
<script src="{{ asset('js/script.js') }}?v={{ time() }}"></script>

    <script>
        $(document).ready(function() {
            /** 
             * Ajax Setup
             */
            $.ajaxSetup({
                headers: {
                    _token: "{{ csrf_token() }}"
                }
            });

            $('.company_petrol_pump').change(function(e) {
                e.preventDefault();
                getPetrolPump($(this).val(), $(this).attr('data-create'));
            });

        });
        /**
         * Get getPetrolPump list by company id
         */
        function getPetrolPump(company_id, create = false) {
            company_id = +company_id;
            var html = '<option value="">Select Petrol Pump</option>';
            if (company_id) {
                addDisabled('.petrol_pump');
                $.ajax({
                    url: "{{ route('ajax.request') }}",
                    method: 'GET',
                    data: {
                        company_id: company_id,
                        create: create,
                        request_from:"petrolPump"
                    },
                    dataType: "JSON",
                    success: function(result) {
                        for (const data in result) {
                            html += '<option value="' + data + '">' +
                                result[data] + '</option>';
                        }
                        $(".petrol_pump").html(html);
                        var petrolPumpSelectedId = $('#petrol_pump_id').attr('data-selected');
                        if (petrolPumpSelectedId) {
                            $(`#petrol_pump_id option[value=${petrolPumpSelectedId}]`).attr('selected', 'selected');
                        }

                        removeDisabled('.petrol_pump');
                    }
                });
            } else {
                $(".petrol_pump").html(html);
            }
        }

       

        function addDisabled($selector) {
            var loader = $($selector).parent().find('.loader');
            loader.removeClass('d-none');
            $($selector).prop("disabled", true);
        }

        function removeDisabled($selector) {
            var loader = $($selector).parent().find('.loader');
            loader.addClass('d-none');
            $($selector).prop("disabled", false);
        }

        // for const
        function emptyArr(arr) {
            //Optional if condition
            if (Array.isArray(arr)) {
                arr.length = 0;
            } else {
                console.error("Arguement must be an array")
            }

        }
    </script>


    @stack('after-scripts')

</body>

</html>
