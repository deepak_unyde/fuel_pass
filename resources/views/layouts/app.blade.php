<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ url('/fav.png') }}">
    <title>Login | {{ config('app.name') }} </title>

    <!-- vendors styles-->
    <link rel="stylesheet" href="{{ asset('css/simplebar.css') }}">
    <!-- Main styles for this application-->
    <link href="{{ asset('css/style.css?v=') }}{{ time() }}" rel="stylesheet">
</head>
<body>
    <div class="bg-light min-vh-100 d-flex flex-row align-items-center">
        @yield('content')
    </div>

    @stack('before-scripts')
    <script src="{{ asset('js/jquery.min.js') }}"></script> 
    @stack('after-scripts')
</body>
</html>
