@extends('layouts.admin')
@php
$ntype=array('daily'=>'Daily','weekly'=>'Weekly','monthly'=>'Monthly');
@endphp
@section('title')
{{ $title }}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- __('You are logged in!') --}}


                    <!---- Dashlet Start---->    
    <div class="row">

                    <div class="col-sm-6 col-lg-3 mb-2 ">
                        <div class="card text-white bg-primary">
                            <div class="card-body">
                                <div class="fs-4 fw-semibold"><i class="fas fa-gas-pump"></i> {{$data['petrol_pump_count']}}</div>
                                    <div>Total Petrol Pump</div>
                            </div>
                            <div class="card-footer px-3 py-2">
                                    <a class="btn-block text-medium-emphasis d-flex  align-items-center "
                                        href="{{ route('petrol-pumps.index') }}">More info</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3 mb-2 ">
                        <div class="card text-white bg-success">
                            <div class="card-body">
                                <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i> {{$data['today_petrol_transaction']}}</div>
                                    <div>Today Transaction</div>
                            </div>
                            <div class="card-footer px-3 py-2">
                                    <a class="btn-block text-medium-emphasis d-flex  align-items-center "
                                        href="{{ route('transactions.index') }}?from_date={{date('d-m-Y')}}&to_date={{date('d-m-Y')}}&status=success">More info</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3 mb-2 ">
                        <div class="card text-white bg-warning">
                            <div class="card-body">
                                <div class="fs-4 fw-semibold"><i class="fas fa-award"></i> {{$data['today_petrol_reward']}}</div>
                                    <div>Today Rewards</div>
                            </div>
                            <div class="card-footer px-3 py-2">
                                    <a class="btn-block text-medium-emphasis d-flex  align-items-center "
                                        href="{{ route('transactions.index') }}?from_date={{date('d-m-Y')}}&to_date={{date('d-m-Y')}}&status=success">More info</a>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6 col-lg-3 mb-2 ">
                        <div class="card text-white bg-danger">
                            <div class="card-body">
                                <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i> {{$data['today_petrol_cashback']}}</div>
                                    <div>Today Cashback</div>
                            </div>
                            <div class="card-footer px-3 py-2">
                                    <a class="btn-block text-medium-emphasis d-flex  align-items-center "
                                        href="{{ route('transactions.index') }}?from_date={{date('d-m-Y')}}&to_date={{date('d-m-Y')}}&status=success">More info</a>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6 col-lg-3 mb-2 ">
                        <div class="card text-white bg-info">
                            <div class="card-body">
                                <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i> {{$data['total_petrol_transaction']}}</div>
                                    <div>Total Transaction</div>
                            </div>
                            <div class="card-footer px-3 py-2">
                                    <a class="btn-block text-medium-emphasis d-flex  align-items-center "
                                        href="{{ route('transactions.index') }}">More info</a>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6 col-lg-3 mb-2 ">
                        <div class="card text-white bg-secondary">
                            <div class="card-body">
                                <div class="fs-4 fw-semibold"><i class="fas fa-award"></i> {{$data['total_petrol_reward']}}</div>
                                    <div>Total Rewards</div>
                            </div>
                            <div class="card-footer px-3 py-2">
                                    <a class="btn-block text-medium-emphasis d-flex  align-items-center "
                                        href="{{ route('transactions.index') }}">More info</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3 mb-2 ">
                        <div class="card text-white bg-primary">
                            <div class="card-body">
                                <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i> {{$data['total_petrol_cashback']}}</div>
                                    <div>Total Cashback</div>
                            </div>
                            <div class="card-footer px-3 py-2">
                                    <a class="btn-block text-medium-emphasis d-flex  align-items-center "
                                        href="{{ route('transactions.index') }}">More info</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3 mb-2 ">
                        <div class="card text-white bg-dark">
                            <div class="card-body">
                                <div class="fs-4 fw-semibold">{{date('Md,y h:i ')}}</div>
                                    <div>Datetime</div>
                            </div>
                            <div class="card-footer px-3 py-2">
                                    <a class="btn-block text-medium-emphasis d-flex  align-items-center "
                                        href="#">Refresh</a>
                            </div>
                        </div>
                    </div> 
        
<!---Graph---->
<!-- Chart start --->
           <div class="row">
             
            <div class="col" style="width:100%">
              <div class="card mb-4">
               
            
            <div class="card-header">
                <div class="row">
                    <div class="col-md-4">Petrol Pump Transaction</div> 
                    <div class="col-md-2"><input type="text" class="form-control calendar" placeholder="From Date" name="from_date" id="from_date" value="{{date('d-m-Y', strtotime('-7 days'))}}" /></div> 
                    <div class="col-md-2"><input type="text" class="form-control calendar" placeholder="To Date" name="to_date" id="to_date" value="{{date('d-m-Y')}}"  /></div> 
                    <div class="col-md-2"><select class=" form-select" type="select" id="ntype" name="ntype"  style="margin-left:0px;">
              <option value="date">Daily</option>
              <option value="week">Weekly</option>
              <option value="month">Monthly</option>
            </select></div> 
                    <div class="col-md-2"><button onclick="getSearch()" class="btn btn-primary transaction_graph_search_btn" id="transaction_graph_search_btn">Search</button></div> 
                </div>        
           
             
           
            </div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="canvas-2"></canvas>
                  </div>
                </div>
              </div>
            </div>
               
      
<!----END---->
                </div>
            </div>
        </div>
    </div>
    
@endsection
@push('after-scripts')
<script src="{{ asset('js/chart.js/js/chart.min.js') }}"></script>
<script src="{{ asset('js/@coreui/chartjs/js/coreui-chartjs.js') }}"></script>
<!--script src="{{ asset('js/chart/charts.js') }}"></script-->
<script>
  function getSearch() 
  {
    $("#from_date").removeClass('is-invalid');
    $("#to_date").removeClass('is-invalid');
    $("#ntype").removeClass('is-invalid');
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var ntype = $('#ntype').val();
    var ntypeText = $('#ntype option:selected').text();
    var error = false;
    if(from_date.length < 5)
    {
         error =true;
        $("#from_date").addClass('is-invalid');
    }
    if(to_date.length < 5)
    {
    $("#to_date").addClass('is-invalid');
    error =true;
    }
    if(ntype.length < 4)
    {
        $("#ntype").addClass('is-invalid');
        error =true;
    }
    if (error==false)
     {
    var labelName = ntypeText+" Transaction";
    var labelArrKey = [];
    var labelArrVal = [];
                addDisabled('.transaction_graph_search_btn');
                $('.transaction_graph_search_btn').html('Wait..');
                $.ajax({
                    url: "{{ route('ajax.request') }}",
                    method: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date,
                        ntype:ntype,
                        request_from:"Home"
                    },
                    dataType: "JSON",
                    success: function(result) {
                        for(var i=0; i<result.length; i++)
                        {
                            labelArrKey.push(result[i].Date);
                            labelArrVal.push(result[i].amount);
                           // console.log(result[i].Date);
                        }

                       /* for (const data in result) {
                            
                                console.log(data);
                                console.log(result[data]);
                        }*/
                        generateChart(labelName ,labelArrKey ,labelArrVal);
                        console.log(result);
                        removeDisabled('.transaction_graph_search_btn');
                        $('.transaction_graph_search_btn').html('Search');
                    }
                });
            } 
    }


 $(document ).ready(function()
 {
     
    console.log( "ready!" );
    var labelName ="Daily Transaction";
    var labelArrKey = [];
    var labelArrVal = [];
    <?php if($data['transaction_graph']){ for($i=0;$i<count($data['transaction_graph']);$i++){ ?>
        labelArrKey.push('<?=$data['transaction_graph'][$i]->Date?>');
        labelArrVal.push('<?=$data['transaction_graph'][$i]->amount?>');
    <?php }} ?>
    generateChart(labelName ,labelArrKey ,labelArrVal );
    
    console.log(labelArrKey);
});   
function generateChart(labelName='Transaction',labelArrKey=[],labelArrVal=[])
 {
    $('.c-chart-wrapper').html('');    
    $('.c-chart-wrapper').html(`<canvas id="canvas-2"></canvas>`);
      
const barChart = new Chart(document.getElementById('canvas-2'), {
  type: 'bar',
  data: {
    labels: labelArrKey,
    datasets: [{
        label:labelName, 
      backgroundColor: 'rgba(220, 220, 220, 0.5)',
      borderColor: 'rgba(220, 220, 220, 0.8)',
      highlightFill: 'rgba(220, 220, 220, 0.75)',
      highlightStroke: 'rgba(220, 220, 220, 1)',
      data: labelArrVal
    }]
  },
  options: {
    responsive: true
  }
    }); 
 }  
function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
                }
</script>
@endpush
