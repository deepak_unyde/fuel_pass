@php
$default = [];
$required = false;
if ($attributes['label_class']) {
    $required = true;
}

if (!$attributes->has('class')) {
    $default['class'] = ' form-group col-md-3 ';
}
$control_class = '';
if ($errors->has($name)) {
    $control_class = ' is-invalid ';
}

if (empty($selected)) {
    $selected = app('request')->input($name);
}

$additional = ['required' => $required, 'class' => ' form-select  ' . $control_class . $attributes['input_class']];
foreach ($attributes as $key => $val) {
    if (Arr::exists($additional, $key)) {
        $additional[$key] = $additional[$key] . '  ' . $val;
    } else {
        Arr::set($additional, $key, $val);
    }
}
$label = Form::label($name, $title, ['class' => 'col-sm-4 col-form-label ' . $attributes['label_class']]);

@endphp


@if ($form_type === 'h')
    <div class="mb-3 row">
        {!! $label !!}
        <div class="col-sm-8">
            @if ($options)
                @foreach ($options as $key => $value)
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" name="{{ $name }}" type="checkbox"
                            id="{{ $key }}" @if (is_array($selected) && in_array($key, $selected)) checked @endif value="{{ $key }}">
                        <label class="form-check-label" for="{{ $key }}">{{ $value }}</label>
                    </div>
                @endforeach
            @endif

            @error($name)
                <div class="invalid-feedback">{{ $message }}
                </div>
            @enderror
        </div>
    </div>
@elseif ($form_type === 'v')
    <div {{ $attributes->class(['form-group'])->merge($default) }}>
        {!! Form::label($name, $title, ['class' => '' . $attributes['label_class']]) !!}
        {!! $input !!}
    </div>
@endif
