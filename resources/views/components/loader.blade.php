<div class="spinner-border text-dark mt-3 d-none loader" {{ $attributes }} role="status">
    <span class="sr-only">Loading...</span>
</div>
