@if ($attributes->has('text'))
    @if ($attributes->get('text') == 1)
        <span class="badge bg-success text-white">Active</span>
    @elseif ($attributes->get('text') == 0)
        <span class="badge bg-danger text-white">InActive</span>
    @endif
@elseif($attributes->has('search'))
    <div class="col-md-3 form-group col-md-3 col-lg-3 col-xl-2">
        <label for="status" class="">Status</label>
        {!! Form::select('status', [1 => 'Active', 0 => 'InActive'], app('request')->input('status'), ['class' => 'form-select', 'placeholder' => 'Select Status']) !!}
    </div>
@else
    <div class="mb-3 row">
        <label for="status" class="col-sm-4 col-form-label required">Status</label>
        <div class="col-sm-8">
            {!! Form::select('status', [1 => 'Active', 0 => 'InActive'], null, ['class' => 'form-select']) !!}

        </div>
    </div>

@endif
