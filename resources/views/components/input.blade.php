@php
$control_class = '';
$required = false;
$readonly = false;
$disabled = true;
if ($attributes['label_class']) {
    $required = true;
}

if ($attributes['readonly']) {
    $readonly = "readonly=true";
}

if ($attributes['disabled']) {
    $disabled = 'disabled';
}

if ($errors->has($name)) {
    $control_class = ' is-invalid';
}

if (empty($selected)) {
    $selected = app('request')->input($name);
}

$placeholder = $title;
if ($attributes->has('placeholder')) {
    $placeholder = $attributes->get('placeholder');
}
if ($type == 'number') {
    $input = Form::number($name, $selected, ['required' => $required, 'placeholder' => $placeholder, 'class' => 'form-control ' . $control_class . $attributes['input_class']]);
} elseif ($type == 'email') {
    $input = Form::email($name, $selected, ['required' => $required, 'placeholder' => $placeholder, 'class' => 'form-control ' . $control_class . $attributes['input_class']]);
} elseif ($type == 'password') {
    $input = Form::password($name, ['required' => $required, 'placeholder' => $placeholder, 'id' => $name, 'class' => 'form-control ' . $control_class . $attributes['input_class']]);
} elseif ($type == 'file') {
    $input = Form::file($name, ['required' => $required, 'placeholder' => $placeholder, 'class' => 'form-control ' . $control_class . $attributes['input_class']]);
    if ($selected) {
        $input .=
            '<div class="mt-2">
             <a href="' .
            $selected .
            '" data-lightbox="example-1">
                <img width="100" height="100" class=" thumbnail rounded img-thumbnail" 
            src="' .
            $selected .
            '"></a><span class=""></span>
                            </div>';
    }
} elseif ($type == 'textarea') {
    $input = Form::textarea($name, $selected, ['required' => $required, 'placeholder' => $placeholder, 'rows' => 3, 'class' => 'form-control ' . $control_class . $attributes['input_class']]);
} else {
    $input = Form::text($name, $selected, ['required'=>$required,'placeholder'=>$placeholder, 'id' => $name, 'class'=>'form-control ' . $control_class . $attributes['input_class'], $readonly, $disabled]);
}

@endphp

@if ($form_type === 'h')
    <div class="mb-3 row {{ $attributes->get('container_class') }}" id="{{ $name }}-input-container">
        @if ($attributes->has('label'))
            <div class="col-sm-4">&nbsp;</div>
        @else
            {!! Form::label($name, $title, ['class' => 'col-sm-4 col-form-label ' . $attributes['label_class']]) !!}
        @endif
        <div class="col-sm-8">
            {!! $input !!}
            @error($name)
                <div class="invalid-feedback">{{ $message }}
                </div>
            @enderror
        </div>
    </div>
@elseif ($form_type === 'v')
    <div {{ $attributes->class(['form-group'])->merge(['class' => 'col-md-3']) }}>
        {!! Form::label($name, $title, ['class' => '' . $attributes['label_class']]) !!}
        {!! $input !!}
    </div>
@endif
