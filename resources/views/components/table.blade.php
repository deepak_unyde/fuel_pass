<div class="table-responsive">
    <table class="table table-striped table-hover mt-4">
        <thead>
            <tr class="table-dark ">
                @if ($tableHeaders)
                    @foreach ($tableHeaders as $tableHeader)
                        <th>{{ $tableHeader }}</th>
                    @endforeach
                @endif
            </tr>

        </thead>
        <tbody>
            {{ $slot }}
        </tbody>
    </table>
</div>
@if ($pagination == true)
    {{ $paginationData }}
@endif
