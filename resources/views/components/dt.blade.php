@php
if (is_object($datetime) && $datetime) {
    if ($format) {
        echo $datetime->format($format);
    } else {
        echo $datetime->format('j M y, g:i a');
    }
} else {
    if ($datetime) {
        if ($format) {
            echo date($format, strtotime($datetime));
        } else {
            echo date('j M y, g:i a', strtotime($datetime));
        }
    }
}
@endphp
