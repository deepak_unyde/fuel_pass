@if ($attributes->has('text'))
    @if ($attributes->get('text') == 'success')
        <span class="badge bg-success text-white">Success</span>
    @elseif ($attributes->get('text') == 'failed')
        <span class="badge bg-danger text-white">Failed</span>
    @elseif ($attributes->get('text') == 'initialize')
        <span class="badge bg-waring text-white">Initialize</span>    
    @elseif ($attributes->get('text') == 'pending')
        <span class="badge bg-waring text-white">Pending</span>        
    @endif
@elseif($attributes->has('search'))
    <div class="col-md-3 form-group col-md-3 col-lg-3 col-xl-2">
        <label for="status" class="">Payment Status</label>
        {!! Form::select('status', ['success' => 'Success', 'failed' => 'Failed','pending' => 'Pending'], app('request')->input('status'), ['class' => 'form-select' ]) !!}
    </div>
@else
    <div class="mb-3 row">
        <label for="status" class="col-sm-4 col-form-label required">Payment Status</label>
        <div class="col-sm-8">
            {!! Form::select('status', ['success' => 'Success', 'failed' => 'Failed','pending' => 'Pending'], null, ['class' => 'form-select']) !!}

        </div>
    </div>

@endif
