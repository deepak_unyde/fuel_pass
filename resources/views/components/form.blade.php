<form autocomplete="off" {{ $attributes->merge(['class' => 'form']) }}>
    @if ($attributes['method'] != 'get')
        @csrf
    @endif

    {{ $slot }}

    @if ($with_button == 'save')
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
    @endif

    {!! Form::close() !!}
