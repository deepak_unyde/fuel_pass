<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        {{ $title }}
        <div class="d-flex justify-content-between align-items-center">
            @isset($header_right)
                {{ $header_right }}
            @endisset

        </div>
    </div><!-- //.card-header -->

    <div class="card-body">
        {{ $slot }}
    </div><!-- //.card-body -->

</div><!-- //.card -->
