@if ($button_type)
    <div class="form-group col-md-4 col-lg-3 col-xl-2">
        <label for="">
            &nbsp;
        </label>
@endif
<button class="btn btn-{{ $type }}" type="submit" {{ $attributes }}>
    @if ($attributes->has('icon'))
        <i class="fa {!! $attributes->get('icon') !!}" aria-hidden="true"></i>
    @endif
    {{ $text }}
</button>
@if ($button_type)
    </div>
@endif
