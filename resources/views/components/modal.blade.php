    <!-- Modal -->
    <div class="modal fade" {{ $attributes }} tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable " role="document">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 class="modal-title">{{ $title }}</h5>
                    <button type="button" class="close" title="Close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{ $slot }}
            </div>
        </div>
    </div>
    {{-- <!-- Button trigger modal -->
 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    Launch demo modal
</button>
<x-modal title="Add" id="exampleModalCenter">
    <div class="modal-body">
        My Name here
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
    </div>
</x-modal> --}}
