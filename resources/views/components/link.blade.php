@php
$icon = 'fa-chevron-left';
$title = __('messages.back');
$iconOnly = false;
if ($attributes->has('title')) {
    $title = $attributes->get('title');
}
if ($attributes->has('icon')) {
    $icon = $attributes->get('icon');
}
if ($attributes->has('iconOnly')) {
    $iconOnly = $attributes->get('iconOnly');
}

@endphp
<a {{ $attributes->class(['text-white btn btn-sm'])->merge(['title' => $title, 'class' => $class_name]) }}>
    @if ($icon !== false)
        <i class="fas {{ $icon }}"></i>
    @endif
    @if ($title != 'Edit' && !$iconOnly)
        {{ $title }}
    @endif

</a>
