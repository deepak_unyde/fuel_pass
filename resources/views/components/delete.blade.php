<form method="post" {{ $attributes }} class="deleteForm"
    onsubmit="return confirm('Are you sure want to delete this record?');" style="display: inline-block">
    @method('delete')
    @csrf
    <button type="submit" title="Delete" class="btn btn-sm btn-danger text-white ">
        <i class="fa fa-trash" aria-hidden="true"></i>
    </button>
</form>
