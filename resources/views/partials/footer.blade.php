<footer class="footer mt-5">
    <div> © {{ date('Y') }} {{ config('app.name') }}</div>

</footer>
