<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">

            <div class="card-header d-flex justify-content-between align-items-center">
                @yield('title')
                <div class="d-flex justify-content-between align-items-center">
                    @yield('action')


                </div>
            </div>
            <div class="card-body">
                @hasSection('searchForm')
                    <form method="GET" action="#" accept-charset="UTF-8">
                        <div class="row">
                            @yield('searchForm')
                            <div class="form-group col-sm-3 col-md-4 col-lg-3 col-xl-2 ">
                                <label for="name">&nbsp;</label>
                                <button title="Search" class="btn  btn-primary form-control" type="search">
                                    <i class="fa fa-search" aria-hidden="true"></i> Search</button>
                            </div>
                        </div>
                    </form>
                @endif
                @hasSection('card_content')
                    @yield('card_content')
                @endif
                @isset($paging)
                    @includeWhen($paging,'partials.pagination')
                @endisset




            </div>

        </div>
    </div>
</div>
