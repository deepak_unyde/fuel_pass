@can('company_level')

    @can('company-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['CompanyController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('companies.index') }}"><span class="nav-icon fas fa-building"></span> Company</a></li>
    @endcan
    @can('petrol-pump-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['PetrolPumpController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('petrol-pumps.index') }}"><span class="nav-icon fas fa-gas-pump"></span> Petrol Pump</a></li>
    @endcan

    @can('transaction-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['TransactionController']) && in_array($action, ['index'])) active @endif"
                        href="{{ route('transactions.index') }}"><span class="nav-icon fas fa-money-check"></span> Transaction History </a></li>
    @endcan

     
    
@endcan
