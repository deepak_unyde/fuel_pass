<a href="{{ url($url) }}" class="btn btn-primary" title="{{ __('messages.create', ['title' => $title]) }}">
    <i class="fas fa-plus-circle"></i> {{ __('messages.create', ['title' => $title]) }} </a>
