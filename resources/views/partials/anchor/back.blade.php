<a href="{{ url($url) }}" class="btn btn-primary" title="{{ __('messages.back') }}">
    <i class="fas fa-chevron-left"></i> {{ __('messages.back') }} </a>
