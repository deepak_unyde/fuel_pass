@canany(['user-list', 'role-list', 'permission-list'])
        @can('price-edit')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['PriceController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('prices.index') }}"><span class="nav-icon fas fa-rupee-sign"></span> Fuel Price</a></li>
        @endcan
        @can('coupon-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['PackageController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('redeem-gifts.index')}}"><span class="nav-icon fas fa-handshake"></span> Redeem Gift</a></li>
        @endcan
        @can('package-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['PackageController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('packages.index')}}"><span class="nav-icon fas fa-gift"></span> Package Plan</a></li>
        @endcan

        @can('company-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['CompanyController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('companies.index') }}"><span class="nav-icon fas fa-building"></span> Company</a></li>
        @endcan
        @can('petrol-pump-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['PetrolPumpController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('petrol-pumps.index') }}"><span class="nav-icon fas fa-gas-pump"></span> Petrol Pump</a></li>
        @endcan
    <li class="nav-group @if (in_array($controller, ['UserController', 'RoleController', 'PermissionController']) && in_array($action, ['create', 'edit'])) show @endif">
        <a class="nav-link nav-group-toggle" href="{{ route('users.index') }}">
            <i class="fas fa-users nav-icon"></i> User Management</a>
        <ul class="nav-group-items">
            @can('user-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['UserController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('users.index') }}"><span class="nav-icon fas fa-users "></span> Users</a></li>
            @endcan

            @can('role-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['RoleController']) && in_array($action, ['create', 'edit'])) active @endif "
                        href="{{ route('roles.index') }}"><span class="nav-icon fas fa-briefcase"></span> Roles</a></li>
            @endcan
            @can('permission-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['PermissionController']) && in_array($action, ['create', 'edit'])) active @endif"
                        title="Permission" href="{{ route('permissions.index') }}"><span
                            class="nav-icon fas fa-unlock-alt "></span>
                        Permissions</a></li>
            @endcan
        </ul>
</li> 
        @can('mileage-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['MileageController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('mileages.index') }}"><span class="nav-icon fas fa-tachometer-alt"></span>Mileage</a></li>
        @endcan
        @can('reward-list')
                <!--li class="nav-item"><a class="nav-link @if (in_array($controller, ['RewardController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('rewards.index') }}"><span class="nav-icon fas fa-award"></span>Rewards & Cashback</a></li-->
        @endcan
        @can('wallet-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['WalletController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('wallets.index') }}"><span class="nav-icon fas fa-wallet"></span>Wallet</a></li>
        @endcan
        @can('vehicle-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['VehicleController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('vehicles.index')}}"><span class="nav-icon fas fa-bus"></span>Vehicle</a></li>
        @endcan 
        @can('transaction-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['TransactionController']) && in_array($action, ['index'])) active @endif"
                        href="{{ route('transactions.index') }}"><span class="nav-icon fas fa-money-check"></span> Transaction History</a></li>
        @endcan  
        
        @can('promotion-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['PromotionController']) && in_array($action, [])) active @endif"
                        href="{{ route('promotions.index') }}"><span class="nav-icon fas fa-bullhorn"></span>Promotion</a></li>
        @endcan  

        @can('wallet-list')
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['ServiceController']) && in_array($action, ['create', 'edit'])) active @endif"
                        href="{{ route('services.index') }}"><span class="nav-icon fas fa-tasks"></span>Service</a></li>
        @endcan

       
@endcan
        