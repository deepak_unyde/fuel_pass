<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2cfJO3G7l9BwoZLI_jkakcZY7zpc8vrE&libraries=places&v=weekly">
</script>

<script src="{{ asset('js/maplabel.js') }}"></script>
<script>
    @php
    $default_lat = 19.091604;
    $default_lng = 72.912727;
    if (isset($latitude)) {
        $default_lat = $latitude;
    }
    if (isset($longitude)) {
        $default_lng = $longitude;
    }
    @endphp
    const myLatLng = {
        lat: {{ $default_lat }},
        lng: {{ $default_lng }}
    };



    function defaultPolygonDraw(map) {
        updateMapCenter(map, myLatLng.lat, myLatLng.lng);
        defaultPolygon(map, polygonConfig, all_overlays);
        console.log('[defaultPolygonDraw]');
    }
    //console.log('myLatLng', myLatLng);
    // Define the LatLng coordinates for the polygon's path.
    const polygon_points = [{
            lat: 19.091604,
            lng: 72.912727
        },
        {
            lat: 19.09250294154627,
            lng: 72.91190400764475
        },
        {
            lat: 19.091920028703214,
            lng: 72.9118469885191
        },

    ];
    const polygonConfig = {
        paths: polygon_points,
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        editable: true,
        draggable: true,
        title: 'Title here',
        label: 'Label here',
    }
    const polygonOption = {
        strokeColor: "#33B8FF",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#33B8FF",
        fillOpacity: 0.35,
    }

    const sceneReadOnly = {
        strokeColor: "#",
        strokeWeight: .5,
        fillColor: "#FFFF00",
        fillOpacity: 0.5,
    }





    function initMap() {
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 18,
            center: myLatLng,
        });

        mapLoad(map);

    }

    function defaultPolygon(map, polygonConfig, allPolygons = []) {
        const polygon = new google.maps.Polygon(polygonConfig);
        polygon.setMap(map);
        allPolygons.push(polygon);
    }


    function editablePolygonDraw(map, polygonConfig, allPolygons = []) {
        const polygon = new google.maps.Polygon(polygonConfig);
        polygon.setMap(map);
        allPolygons.push(polygon);

        //Add the click listener
        google.maps.event.addListener(polygon.getPath(), 'set_at', polygonLatLng);
        //Add the click listener
        google.maps.event.addListener(polygon.getPath(), 'insert_at', polygonLatLng);

        function polygonLatLng() {
            var lat_long_Arr = []
            for (var i = 0; i < polygon.getPath().getLength(); i++) {
                if (i == polygon.getPath().getLength() - 1) {
                    lat_long_Arr.push(polygon.getPath().getAt(i).toUrlValue(6));
                } else {
                    lat_long_Arr.push(polygon.getPath().getAt(i).toUrlValue(6) + '|');
                }
            }
            $('.polygon_area').val(lat_long_Arr);

        }


        searchPlace(map, polygon);
    }



    function searchPlace(map, polygon) {
        // console.log('searchBox', searchBox);
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener("places_changed", () => {
            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            const places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();

            places.forEach((place) => {

                if (!place.geometry || !place.geometry.location) {
                    //console.log("Returned place contains no geometry");
                    return;
                }

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    position: place.geometry.location
                }));
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });

            var polybnds = new google.maps.LatLngBounds();
            for (var i = 0; i < polygon.getPath().getLength(); i++) {
                polybnds.extend(polygon.getPath().getAt(i));
            }
            var polyCenter = polybnds.getCenter();
            // center polygon on first marker
            var distance = google.maps.geometry.spherical.computeDistanceBetween(polyCenter, markers[0]
                .getPosition());
            var heading = google.maps.geometry.spherical.computeHeading(polyCenter, markers[0].getPosition());
            var path = [];
            polybnds = new google.maps.LatLngBounds();
            for (var i = 0; i < polygon.getPath().getLength(); i++) {
                var pt = google.maps.geometry.spherical.computeOffset(polygon.getPath().getAt(i), distance,
                    heading);
                path.push(pt);
                polybnds.extend(pt);
            }
            polygon.setPath(path);

            google.maps.event.addListener(polygon.getPath(), 'set_at', polygonLatLng);
            //Add the click listener
            google.maps.event.addListener(polygon.getPath(), 'insert_at', polygonLatLng);

            function polygonLatLng() {
                var lat_long_Arr = []
                for (var i = 0; i < polygon.getPath().getLength(); i++) {
                    if (i == polygon.getPath().getLength() - 1) {
                        lat_long_Arr.push(polygon.getPath().getAt(i).toUrlValue(6));
                    } else {
                        lat_long_Arr.push(polygon.getPath().getAt(i).toUrlValue(6) + '|');
                    }
                }
                $('.polygon_area').val(lat_long_Arr);
            }

            map.fitBounds(bounds);
        });
    }

    function showArrays(event) {

        //Change the color here
        // toggle it
        if (this.get("fillColor") != '#0000ff') {
            this.setOptions({
                fillColor: '#0000ff'
            });
        } else {
            this.setOptions({
                fillColor: '#ff0000'
            });
        }

        // Since this polygon has only one path, we can call getPath()
        // to return the MVCArray of LatLngs.
        var vertices = this.getPath();

        var contentString = '<b>My polygon</b><br>' +
            'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
            '<br>';

        // Iterate over the vertices.
        for (var i = 0; i < vertices.getLength(); i++) {
            var xy = vertices.getAt(i);
            contentString += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' + xy.lng();
        }
        //  console.log(contentString);
        // Replace the info window's content and position.
        infoWindow.setContent(contentString);
        infoWindow.setPosition(event.latLng);

        infoWindow.open(map);
    }
    google.maps.event.addDomListener(window, 'load', initMap);
</script>
