<div class="table-responsive">
    <table class="table table-striped table-hover mt-4">
        <thead>
            <tr class="table-dark ">
                @if ($table_headers)
                    @foreach ($table_headers as $table_header)
                        <th scope="col">{{ $table_header }}</th>
                    @endforeach
                @endif
            </tr>
        </thead>

        <tbody>
            @php
                $edit_button = true;
                $view_button = true;
                $delete_button = true;
                $created_at_col = true;
                $updated_at_col = true;
                
                if (isset($edit)) {
                    if ($edit === false) {
                        $edit_button = false;
                    }
                }
                
                if (isset($view)) {
                    if ($view === false) {
                        $view_button = false;
                    }
                }
                
                if (isset($delete)) {
                    if ($delete === false) {
                        $delete_button = false;
                    }
                }
                if (isset($created)) {
                    if ($created === false) {
                        $created_at_col = false;
                    }
                }
                
                if (isset($updated)) {
                    if ($updated === false) {
                        $updated_at_col = false;
                    }
                }
                
            @endphp
            @if ($rows)
                @foreach ($rows as $row)
                    <tr>
                        <td>{{ ++$i }}</td>
                        @foreach ($fields as $field)
                            <td>{{ $row->$field }}</td>
                        @endforeach
                        @if ($created_at_col)

                            <td>
                                @if ($row->created_at)
                                    {{ $row->created_at->format('j M y, h:i a') }}
                                @endif
                            </td>


                        @endif
                        @if ($updated_at_col)

                            <td>
                                @if ($row->updated_at)
                                    {{ $row->updated_at->format('j M y, h:i a') }}
                                @endif
                            </td>


                        @endif

                        @if ($actions)
                            <td>
                                @foreach ($actions as $action_key => $action_url)
                                    @if ($action_key == 'edit')
                                        <a href="#" class="btn  btn-sm btn-primary" title="Edit">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                    @elseif ($action_key=="destroy")
                                        <a class="btn btn-sm text-white  btn-danger" title="Delete" href="#"
                                            data-toggle="modal" data-target=".delete-1">
                                            <i class="fas fa-trash"></i>
                                            Delete</a>
                                    @endif

                                @endforeach
                            </td>
                        @endif
                    </tr>
                @endforeach
            @endif

        </tbody>

    </table>
