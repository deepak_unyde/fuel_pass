@php
$l_class = 'col-sm-4 col-form-label ';
$i_class = 'form-control';
if (isset($label_class)) {
    $l_class .= $label_class;
}
if (isset($input_class)) {
    $i_class .= $input_class;
}
@endphp
<div class="mb-3 row">
    {!! Form::label($name, $title, ['class' => $l_class]) !!}
    <div class="col-sm-8">
        {!! Form::text($name, null, ['title' => $title, 'class' => $i_class, 'placeholder' => $title]) !!}
    </div>
</div>
