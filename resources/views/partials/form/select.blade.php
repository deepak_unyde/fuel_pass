@php
$l_class = 'col-sm-4 col-form-label ';
if (isset($label_class)) {
    $l_class .= $label_class;
}
@endphp
<div class="mb-3 row">
    {!! Form::label($name, $title, ['class' => $l_class]) !!}
    <div class="col-sm-8">
        {!! Form::select($name, $options, null, ['title' => $title, 'class' => 'form-select', 'placeholder' => 'Select']) !!}
    </div>
</div>
