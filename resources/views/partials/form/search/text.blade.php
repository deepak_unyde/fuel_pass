<div class="form-group col-3 col-sm-3 col-md-4 col-lg-3 col-xl-3">
    <label for="name">
        {!! Form::label($name, $title) !!}
    </label>
    {!! Form::text($name, null, ['title' => $title, 'class' => 'form-control', 'placeholder' => $title]) !!}
</div>
