<div class="form-group col-3 col-sm-3 col-md-4 col-lg-3 col-xl-2">
    <label for="name">
        {!! Form::label($name, $title) !!}
    </label>
    {!! Form::select($name, $options, null, ['title' => $title, 'class' => 'form-select', 'placeholder' => 'Select']) !!}
</div>
