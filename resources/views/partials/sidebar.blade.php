@php
$routeArray = app('request')
    ->route()
    ->getAction();
$controllerAction = class_basename($routeArray['controller']);
[$controller, $action] = explode('@', $controllerAction);
@endphp


<div class="sidebar sidebar-dark sidebar-fixed" id="sidebar">
    <div class="sidebar-brand d-none d-md-flex bg-white">
        <img class="sidebar-brand-full" src="{{ asset('img/logo.png') }}?v={{ time() }}" width="64" alt="">
        <img class="sidebar-brand-narrow" src="{{ asset('img/logo.png') }}?v={{ time() }}" width="46" alt="">
    </div>
    <ul class="sidebar-nav" data-coreui="navigation" data-simplebar="">
        <li class="nav-item">
            <a class="nav-link" href="#"><i class="fas fa-tachometer-alt nav-icon"></i> Dashboard</a>
        </li>
        {{-- Admin Menu --}}
        @include('partials.admin_menu')
        @include('partials.company_menu')
        @include('partials.petrol_pump_menu')

        <!--li class="nav-item"><a class="nav-link @if (in_array($controller, ['LoggerController']) && in_array($action, [])) active @endif"
                        href="{{ route('logger.index') }}"><span class="nav-icon fas fa-bullhorn"></span>Vivek Data</a></li-->


        <li class="nav-item"><a class="nav-link" title="Logout"
                onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                href="{{ route('logout') }}">
                <i class="fas fa-sign-out-alt nav-icon"></i>
                Logout </a></li>



    </ul>
    {{-- <button class="sidebar-toggler" type="button" data-coreui-toggle="unfoldable"></button> --}}
</div>
