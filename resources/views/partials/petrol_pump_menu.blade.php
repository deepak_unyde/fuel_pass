@can('supervisor_level')
    <li class="nav-group @if (in_array($controller, ['SurveySupervisorController']) && in_array($action, ['assets', 'assetEdit'])) show @endif ">
        <a class="nav-link nav-group-toggle" href="#">
            <i class="fas fa-th-list nav-icon"></i>Assets</a>
        <ul class="nav-group-items">
            @foreach (categoryBySupervisor() as $key => $value)
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['SurveySupervisorController']) && in_array($action, ['assets', 'assetEdit']) && app('request')->input('category_id') == $key) active @endif"
                        href="{{ route('supervisor.assets.index', ['category_id' => $key]) }}">
                        <i class="fas fa-list nav-icon"></i> {{ $value }} </a></li>
            @endforeach

        </ul>
    </li>
    <li class="nav-group @if (in_array($controller, ['SurveySupervisorController']) && in_array($action, ['existingAssets', 'existingAssetShow'])) show @endif ">
        <a class="nav-link nav-group-toggle" href="#">
            <i class="fas fa-warehouse nav-icon"></i>Existing Asset</a>
        <ul class="nav-group-items">
            @foreach (categoryBySupervisor() as $key => $value)
                <li class="nav-item"><a class="nav-link @if (in_array($controller, ['SurveySupervisorController']) && in_array($action, ['existingAssets', 'existingAssetShow']) && app('request')->input('category_id') == $key) active @endif"
                        href="{{ route('supervisor.existing-assets.index', ['category_id' => $key]) }}">
                        <i class="fas fa-list nav-icon"></i> {{ $value }}</a></li>
            @endforeach



        </ul>
    </li>


    <li class="nav-group">
        <a class="nav-link nav-group-toggle" href="#">
            <i class="fas fa-poll nav-icon"></i> Survey Managment</a>
        <ul class="nav-group-items">
            <li class="nav-item"><a class="nav-link" href="{{ route('supervisor.scene') }}">
                    <i class="fas fa-poll nav-icon"></i> Survey Scenes </a></li>
        </ul>
    </li>
    <li class="nav-item"><a class="nav-link" href="{{ route('employee.supervisor') }}">
            <i class="fas fa-list nav-icon"></i> Employees </a></li>
@endcan
