<header class="header header-sticky mb-4">
    <div class="container-fluid">
        <button class="header-toggler px-md-0 me-md-3" title="Toggle Menu" type="button"
            onclick="coreui.Sidebar.getInstance(document.querySelector('#sidebar')).toggle()">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <a class="header-brand d-md-none" href="#">
            <img class="sidebar-brand-full" src="{{ asset('img/logo.png') }}" width="38" alt="">
        </a>
        <ul class="header-nav ms-3">
            <li class="nav-item dropdown"><a class="nav-link py-0" data-coreui-toggle="dropdown" href="#" role="button"
                    aria-haspopup="true" aria-expanded="false">
                    @auth
                        {{ Auth::user()->name }}
                    @endauth
                    @php
                        $avatar = Auth::user()->getFirstMediaUrl('avatar', 'thumb');
                    @endphp

                    <div class="avatar avatar-md">
                        <img class="avatar-img avatar-img-small" src="{{ $avatar }}" alt="">
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-end pt-0">

                    <div class="dropdown-header bg-light py-2">
                        <div class="fw-semibold">
                            @auth
                                {{ Auth::user()->email }}
                            @else
                                &nbsp;
                            @endauth

                        </div>
                    </div>
                    <a class="dropdown-item" href="{{ route('change-password') }}">
                        <i class="fas fa-cog"></i> Change Password</a>


                    @auth


                        <div class="dropdown-divider"></div>


                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();                             document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    @endauth

                </div>
            </li>
        </ul>
    </div>
    {{-- <div class="header-divider"></div>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb my-0 ms-2">
                <li class="breadcrumb-item">
                    <!-- if breadcrumb is single--><span>Home</span>
                </li>
                <li class="breadcrumb-item active"><span>Dashboard</span></li>
            </ol>
        </nav>
    </div> --}}
</header>
