<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('redeem-gifts.index');
$readonly = 'readonly';
$gift_image = $redeemGift->getFirstMediaUrl('gift_image');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($redeemGift, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form', 'route' => ['redeem-gifts.update', $redeemGift->id]]) !!}
        <x-input name="gift_name" title="Gift name" label_class="required" />
        <x-input type="textarea" name="short_description" title="Short Description"  label_class="required" />
        <div class="mb-3 row">
                <label class="col-sm-4 col-form-label required">Start Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control from_date" placeholder="Start Date" name="from_date" value="<?=($redeemGift->from_date)?date('d-m-Y',strtotime($redeemGift->from_date)):''?>" required />
                </div>
        </div>
        <div class="mb-3 row">
            <label class="col-sm-4 col-form-label required">End Date</label>
            <div class="col-sm-8">
                <input type="text" class="form-control to_date" placeholder="End Date" name="to_date" value="<?=($redeemGift->to_date)?date('d-m-Y',strtotime($redeemGift->to_date)):''?>" required />
            </div>
        </div>
        <x-input name="min_redeem_point" title="Minimum redeem points" label_class="required" input_class="digitonly" />    
        <x-input name="min_gift_cost" title="Redeem points cost" label_class="required" input_class="digitonly" />
        <x-input name="redeem_cost" title="Gift Price" label_class="required" input_class="digitonly" />
        <x-input name="valid_no_of_day" title="No of valid days after redeem" label_class="required" input_class="digitonly"  />
        
        @php if($redeemGift->use_coupon_qty>0) { @endphp
        <x-input name="total_coupon_qty" input_class="digitonly" title="Coupon Qty" label_class="required" readonly />
        @php } else { @endphp
        <x-input name="total_coupon_qty" input_class="digitonly" title="Coupon Qty" label_class="required" />
        @php } @endphp
        <x-input name="gift_image" title="Image" type="file"  :selected="$gift_image"  />
        <x-status />
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('redeem-gifts.common')
