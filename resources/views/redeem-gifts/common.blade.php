@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({

                rules: {
                   gift_name: {
                        required: true,
                    },
                    short_description: {
                        required: true,
                    },
                    from_date: {
                        required: true,
                         
                    },
                    to_date: {
                        required: true,
                        
                    },
                    redeem_cost: {
                        required: true,
                        number: true,
                        min: 0
                        },
                    min_gift_cost: {
                    required: true,
                    number: true,
                    min: 0
                    },

                    min_redeem_point: {
                    required: true,
                    number: true,
                    min: 0
                    },
                    valid_no_of_day: {
                        minlength: 1,
                       // maxlength: 10,
                        digits: true
                    },
                    total_coupon_qty: {
                        minlength: 1,
                        digits: true
                    },
                    status: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                    
                }
                 
            });
        }); 
    </script>
@endpush
