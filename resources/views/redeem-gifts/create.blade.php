<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('redeem-gifts.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        <x-form method="post"  enctype="multipart/form-data" action="{{ route('redeem-gifts.store') }}">
        <x-input name="gift_name" title="Gift name" label_class="required" />
        <x-input type="textarea" name="short_description" title="Short Description"  label_class="required" />
        <div class="mb-3 row">
                <label class="col-sm-4 col-form-label required">Start Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control calendar_1" placeholder="Start Date" name="from_date" value="<?= app('request')->input('from_date')?>" required />
                </div>
        </div>
        <div class="mb-3 row">
            <label class="col-sm-4 col-form-label required">End Date</label>
            <div class="col-sm-8">
                <input type="text" class="form-control calendar_1" placeholder="End Date" name="to_date" value="<?= app('request')->input('to_date')?>" required />
            </div>
        </div>
        <x-input name="min_redeem_point" title="Minimum redeem points" label_class="required" input_class="digitonly"  />    
        <x-input name="min_gift_cost" title="Redeem points cost" label_class="required" input_class="digitonly"  />
        <x-input name="redeem_cost" title="Gift Price" label_class="required" input_class="digitonly"  />
        <x-input name="valid_no_of_day" title="No of valid days after redeem" input_class="digitonly"  label_class="required" />
        <x-input name="total_coupon_qty" input_class="digitonly" title="Coupon Qty" label_class="required" />

        <x-input name="gift_image" title="Image" type="file" label_class="required" />
        <x-status />
        </x-form>
    </x-card>
@endsection
@include('redeem-gifts.common')
