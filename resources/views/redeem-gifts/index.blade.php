<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('redeem-gifts.create');

$tableHeader = ['#', 'Gift','Details','Start Date', 'End Date', 'Min Points', 'Point Cost', 'Gift Cost',  'No of days','Total Qty', 'Used Qty', 'Status', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('coupon-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => $title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('redeem-gifts.index') }}">
            <div class="row">

                <x-input name="s" title="Gift name" class="col-md-4 col-lg-4 col-xl-3" form_type="v" />
                <x-status search />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$coupons" pagination="true">
            @forelse ($coupons  as $coupon )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $coupon->gift_name }}</td>
                    <td class="col-md-2">{{ $coupon->short_description }}</td>
                    <td>{{ $coupon->from_date }}</td>
                    <td>{{ $coupon->to_date }}</td>
                    <td>{{ $coupon->min_redeem_point }}</td>
                    <td>{{ $coupon->min_gift_cost }}</td>
                    <td>{{ $coupon->redeem_cost }}</td>
                    <td>{{ $coupon->valid_no_of_day }}</td>
                    <td>{{ $coupon->total_coupon_qty }}</td>
                    <td>{{ $coupon->use_coupon_qty }}</td>
                    <td>
                        <x-status text="{{ $coupon->status }}" />
                    </td>
                    <td>
                        <x-dt :date_time="$coupon->created_at" />

                    </td>
                    <td>
                        @can('coupon-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('redeem-gifts.edit', $coupon->id) }}" />
                        @endcan
                        @can('coupon-delete')
                            <x-delete action="{{ route('redeem-gifts.destroy', $coupon->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="15" />
            @endforelse

        </x-table>
    </x-card>
@endsection
