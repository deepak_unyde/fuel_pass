<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]); 

$tableHeader = ['#', 'Name',   'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
       
        <x-form with_button="search" method="get" action="{{ route('ratings.index') }}">
            <div class="row">
                <x-select name="petrol_pump_id" title="Petrol Pump" :options="$petrolPump" form_type="v"  class="col-md-3" />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$mileages" pagination="true">
            @forelse ($ratings  as $rating )
                <tr>
                    <td>{{ ++$i }}</td>
                     
                      
                    <td>
                        <x-dt :date_time="$rating->created_at" />
                    </td>
                    <td>
                        @can('mileage-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('ratings.edit', $ratings->id) }}" />
                        @endcan
                        @can('mileage-delete')
                            <x-delete action="{{ route('ratings.destroy', $ratings->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="6" />
            @endforelse

        </x-table>
    </x-card>
@endsection
