<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('packages.create');

$tableHeader = ['#','Icon', 'Name','Start Date','End Date', 'Duration','Cost','Discount','Discount (%)','Reward Point','Cashback', 'Status', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('package-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => $title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('packages.index') }}">
            <div class="row">

                <x-input name="s" title="Package name" class="col-md-4 col-lg-4 col-xl-3" form_type="v" />

                <div class="col-md-2 form-group col-md-3 col-lg-3 col-xl-2">
                    <label for="form_date" class="">From Date</label>
                    <input type="text" class="form-control calendar" placeholder="Package Start Date" name="from_date" value="<?= app('request')->input('from_date')?>"  />
                
                </div>

                <div class="col-md-2 form-group col-md-3 col-lg-3 col-xl-2">
                    <label for="to_date" class="">To Date </label>
                    <input type="text" class="form-control calendar" placeholder="Package End Date" name="to_date" value="<?= app('request')->input('to_date')?>"  />
                
                </div>
                <x-status search />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$packages" pagination="true">
            @forelse ($packages  as $package )
                 @php
                 $package_image =   ($package->getFirstMediaUrl('package_image'))?$package->getFirstMediaUrl('package_image'):asset('img/default.png');
                @endphp
                <tr>
                    <td>{{ ++$i }}</td>
                    <td> <img class="avatar-img avatar-img-small"
                        src="{{ $package_image }}"><span class=""></span></td>
                    <td>{{ $package->package_name }}</td>
                    <td>{{ $package->from_date }}</td>
                    <td>{{ $package->to_date }}</td> 
                    <td>{{ $package->duration }}</td>
                    <td>{{ $package->cost }}</td>
                    <td>
                        <?php
                            if($package->discount==0)
                                echo "No";
                            else
                                echo "Yes";
                        ?>
                    </td>
                    <td>{{ $package->discount_percentage }}</td>
                    <td>{{ $package->reward_point }}</td>
                    <td>{{ $package->cashback }}</td>
                    <td>
                        <x-status text="{{ $package->status }}" />
                    </td>
                    <td>
                        <x-dt :date_time="$package->created_at"/>

                    </td>
                    <td>
                        @can('package-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('packages.edit', $package->id) }}" />
                        @endcan
                        @can('package-delete')
                            <x-delete action="{{ route('packages.destroy', $package->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="14" />
            @endforelse

        </x-table>
    </x-card>
@endsection
