<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('packages.index');
$discount = array('0'=>'No','1'=>'Yes');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        <x-form method="post"  enctype="multipart/form-data"  action="{{ route('packages.store') }}">
            <x-input name="package_name" title="Package name" label_class="required" />
            <x-input type="textarea" name="package_description" title="Package description" />

            <x-input name="package_image" title="Package Icon" type="file" label_class="required" />
            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label ">Package Start Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control calendar_1" placeholder="Package Start Date" name="from_date" value="<?= app('request')->input('from_date')?>" required />
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label ">Package End Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control calendar_1" placeholder="Package End Date" name="to_date" value="<?= app('request')->input('to_date')?>" required />
                </div>
            </div>
            <x-input name="duration" title="Duration (No of days)" label_class="required" input_class="digitonly" />
            <x-input name="cost" title="Package Cost" label_class="required" input_class="digitonly" />
            <x-input name="reward_point" title="Reward Point (per litre)" label_class="required" input_class="digitonly" />
            
            <x-input name="cashback" title="Cashback amount (per litre)" label_class="required" input_class="digitonly" />
            <x-select name="discount" title="Package Discount" :options="$discount" type="select"   onchange="getDiscountvalue(this.value)" />
            <x-input name="discount_percentage" title="Discount (%)" value="0" input_class="digitonly" />
            <x-status />
        </x-form>
    </x-card>
@endsection
@include('packages.common')
