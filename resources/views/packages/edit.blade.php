<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('packages.index');

$discount = array('0'=>'No','1'=>'Yes');
$package_image = $package->getFirstMediaUrl('package_image');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($package, ['method' => 'PUT','enctype' => 'multipart/form-data', 'class' => 'form',  'route' => ['packages.update', $package->id]]) !!} 
        <x-input name="package_name" title="Package name" label_class="required" />
            <x-input type="textarea" name="package_description" title="Package description" />
            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label ">Package Start Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control from_date" placeholder="Package Start Date" name="from_date" value="<?=($package->from_date)?date('d-m-Y',strtotime($package->from_date)):''?>" required />
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label ">Package End Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control to_date" placeholder="Package End Date" name="to_date" value="<?=($package->to_date)?date('d-m-Y',strtotime($package->to_date)):''?>" required />
                </div>
            </div>
            <x-input name="duration" title="Duration (No of days)" label_class="required"  input_class="digitonly"  />
            <x-input name="cost" title="Package Cost" label_class="required"  input_class="digitonly" />

            
            <x-input name="package_image" title="Package Icon" type="file" :selected="$package_image" />
            <x-input name="reward_point" title="Reward Point (per litre)" label_class="required"  input_class="digitonly" />
            
            <x-input name="cashback" title="Cashback amount (per litre)" label_class="required"  input_class="digitonly" />
            <x-select name="discount" title="Package Discount" :options="$discount" type="select"   onchange="getDiscountvalue(this.value)" />
            <x-input name="discount_percentage" title="Discount (%)" value="0"  input_class="digitonly" />
            <x-status />
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('packages.common')
