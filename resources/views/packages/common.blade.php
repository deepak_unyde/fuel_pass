@push('after-scripts')
    <script>
/*function getDiscountvalue(val)
{
        if(val=='1')
        {
           $('#discount_percentage').attr("required","required");
        }
        else
        {
            $('#discount_percentage').removeAttr("required");
            $('#discount_percentage').removeClass('is-invalid');
            $('#discount_percentage-error').remove();
        }
}*/
        $(document).ready(function() {
            $(".form").validate({
                rules: {
                    package_name: "required",
                    from_date:"required",
                    to_date:"required",
                    status:"required",
                    discount:"required",
                    duration: {
                        required: true,
                        number: true
                    },
                    cost: {
                        required: true,
                        number: true
                    },
                    reward_point: {
                        required: true,
                        number: true
                    },
                    cashback: {
                        required: true,
                        number: true
                    },
                    discount_percentage: {
                        required: {
                        depends: function(elem) {
                        return $("#discount").val() == 1
                        }
                        },
                        number: true,
                        min: 0
                        }

                    
                }
                 
            });
        });
         
    </script>
@endpush
