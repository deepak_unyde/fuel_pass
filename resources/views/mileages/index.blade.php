<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('mileages.create');

$tableHeader = ['#', 'Name', 'Mobile',  'Meter Reading', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('mileage-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' =>$title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('mileages.index') }}">
            <div class="row">
                <x-select name="user_id" title="User" :options="$users" form_type="v"  class="col-md-3" />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$mileages" pagination="true">
            @forelse ($mileages  as $mileage )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @if ($mileage->user)
                            {{ $mileage->user->name }}
                        @endif
                    </td>
                    <td>
                        @if ($mileage->user)
                            {{ $mileage->user->mobile }}
                        @endif
                    </td>
                    <td>{{ $mileage->meter_reading }}</td>
                      
                    <td>
                        <x-dt :date_time="$mileage->created_at" />
                    </td>
                    <td>
                        @can('mileage-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('mileages.edit', $mileage->id) }}" />
                        @endcan
                        @can('mileage-delete')
                            <x-delete action="{{ route('mileages.destroy', $mileage->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="6" />
            @endforelse

        </x-table>
    </x-card>
@endsection
