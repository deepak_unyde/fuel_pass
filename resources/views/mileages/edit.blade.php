<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('mileages.index');

@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($mileage, ['method' => 'PUT', 'class' => 'form', 'route' => ['mileages.update', $mileage->id]]) !!}
        <x-select name="user_id" title="User" :options="$users" type="select" label_class="required" />
            <x-input name="meter_reading" title="Enter Meter Reading" label_class="required" />
           
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('mileages.common')
