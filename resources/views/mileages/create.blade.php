<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('mileages.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('mileage-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" />
            </x-slot>
        @endcan
        <x-form method="post" action="{{ route('mileages.store') }}">
            <x-select name="user_id" title="User" :options="$users" type="select" label_class="required" />
            <x-input name="meter_reading" title="Enter meter reading" label_class="required"   />
             
        </x-form>
    </x-card>
@endsection
@include('mileages.common')
