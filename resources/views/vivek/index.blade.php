<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]); 

$tableHeader = ['#', 'Lati','Longi','Accuracy', 'Constellation Details','Consolidated Data','Device','Created At'];
$colorArr = [];
$groupArr = [];
 
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
        <x-link href="{{  route('logger.create')  }}" title="Clear All Data"
                    icon="fa fa-trash" class="btn-danger"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <x-link href="{{  route('logger.graph')  }}" title="Graph Data"
                    icon="fa fa-graph" class="btn-warning"/>            
        </x-slot>
        <x-form with_button="search" method="get" action="{{ route('logger.index') }}">
            <div class="row">
                <x-input name="count" title="Common satellite" class="col-md-2 col-lg-2 col-xl-2" form_type="v" input_class="digitonly" />
                <x-input name="s" title="Satellite" class="col-md-4 col-lg-4 col-xl-4" form_type="v" />
                <x-input name="fdate" title="From datetime" class="col-md-3 col-lg-3 col-xl-3" placeholder="YYYY-MM-DD H:i:s" form_type="v" />
                <x-input name="tdate"  placeholder="YYYY-MM-DD HH:ii:ss"  title="To datetime" class="col-md-3 col-lg-3 col-xl-3" form_type="v" />
                <!--div class="col-md-2 form-group col-md-2 col-lg-2 col-xl-2">
                <label for="time" class="">Time</label>
                <select class="form-select" name="time">
                    <option value="1"  >All Data</option>
                    <option value="15">Last 15 Min</option>
                    <option selected="selected"  value="30">Last 30 Min</option>
                    <option value="45">Last 45 Min</option>
                    <option value="60">Last 1 Hour</option>
                    <option value="90">Last 1:30 Hour</option>
                    <option value="120">Last 2 Hour</option>
                </select>
         
                </div-->

                <div class="col-md-4 col-lg-4 col-xl-4">
                    <label for="device_name[]" class="required">Device Name</label>
                    <select class=" form-select  select2"  multiple="" type="select" id="device_name[]" name="device_name[]">
                  <option value="" <?php if(empty($_REQUEST['device_name'])){ echo 'selected="selected"';}?>>All Device</option>
                  <?php if($deviceList){ foreach($deviceList as $v){?>
                  <option value="<?=$v->device_name?>" <?php if(!empty($_REQUEST['device_name'])) { for($c=0;$c<count($_REQUEST['device_name']);$c++){ if($_REQUEST['device_name'][$c]==$v->device_name){ echo 'selected="selected"';}}}?>><?=$v->device_name?></option>
                  <?php }} ?>
                </select>
                
                </div>
                
               
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
                
            </div>
        </x-form>
        
        <x-table :table_header="$tableHeader" :data="$loggers" pagination="true">
            @forelse ($loggers  as $data )
                <?php 
                $constellation_count=0;
                $typeExplodeArr =array();
                $typeExplodeUniqueArr =array();
                $nCount = 0;
                if($data->constellation_type)
                {
                $Array = explode(',',$data->constellation_type);
                $nCount = count($Array)-1;
                $typeExplodeArr = array_unique($Array);
                $constellation_count = count($typeExplodeArr)-1;
                $typeExplodeUniqueArr = array_count_values($Array);
                }
                ?> 
                <tr>
                    <td>{{ ++$i }}</td>
                    <td><?=$data->lati?></td>
                    <td><?=$data->longi?></td>
                    <td><?=$data->accuracy?></td>
                    <td>
                    <table>
                    <tr  style="border: 1px solid black;">
                           <th>Constellation Count :</th>
                           <td style="border: 1px solid black;" colspan="<?=$constellation_count?>"><?=$constellation_count?> (<?=$nCount?>)</td>
                    </tr>
                          <tr  style="border: 1px solid black;">
                           <th>Constellation Group Type :</th>
                           <?php if($typeExplodeArr){$inc=1; foreach($typeExplodeArr as $key=>$value){
                             
                             
                             
                              if($inc<count($typeExplodeArr)){?>
                           <td  style="border: 1px solid black;">
                              <?php  echo $value;  ?>
                           </td>  
                          <?php }$inc++;}} ?>
                          </tr>
                          <tr  style="border: 1px solid black;">
                           <th>Constellation Val Count :</th>
                          
                           <?php if($typeExplodeArr){ $inc=1;foreach($typeExplodeArr as $key=>$value){ if($inc<count($typeExplodeArr)){?>
                            
                            <td  style="border: 1px solid black;">
                              <?php echo $typeExplodeUniqueArr[$value];?>
                            </td>

                             <?php } $inc++;}} ?>
                         
                          </tr>
                    </table>       
                    </td>

                    <td>
                    <?php 
                     $tyArray = explode(',',$data->constellation_type);
                     $vArray = explode(',',$data->vehicle_id);
                    
                     for($a=0;$a<count($vArray)-1;$a++){
                       
                      
                      if (!array_key_exists($tyArray[$a],$colorArr))
                      {
                        $colorArr[$tyArray[$a]]['vehicle'][]=$vArray[$a]; 
                        $colorArr[$tyArray[$a]]['color'][]=sprintf('#%06X', mt_rand(0, 0xFFFFFF)); 
                      }
                      else
                      {
                        if(!empty($colorArr) and !in_array($vArray[$a],$colorArr[$tyArray[$a]]['vehicle']))
                       {
                        $colorArr[$tyArray[$a]]['vehicle'][]=$vArray[$a]; 
                        $colorArr[$tyArray[$a]]['color'][]=sprintf('#%06X', mt_rand(0, 0xFFFFFF)); 
                       }
                      }
                      
                      }
                     //echo "<pre>";print_r($colorArr);
                    ?>   
                         <table>
                           <tr  style="border: 1px solid black;">
                           <th>Vehicle:</th>
                             <?php $vArray = explode(',',$data->vehicle_id);
                             for($a=0;$a<count($vArray)-1;$a++){
                               
                              if(in_array($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']))
                              {  $key = array_search($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']);   $color = $colorArr[$tyArray[$a]]['color'][$key];
                              }
                              else { $color = '';  }
                               ?>
                                
                             <td bgcolor="<?=$color;?>" style="border: 1px solid black;"><?=$vArray[$a]?></td>

                             <?php }?>
                           </tr> 
                           <tr  style="border: 1px solid black;">
                           <th>Azimuth:</th>
                             <?php $aArray = explode(',',$data->azimuth);
                             for($a=0;$a<count($aArray)-1;$a++){
                               
                              if(in_array($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']))
                              {  $key = array_search($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']);   $color = $colorArr[$tyArray[$a]]['color'][$key];
                              }
                              else { $color = '';  }
                               
                               ?>
                             <td  bgcolor="<?=$color;?>"  style="border: 1px solid black;"><?=$aArray[$a]?></td>
                             <?php }?>
                           </tr> 
                           <tr  style="border: 1px solid black;">
                            <th>Elevation:</th>
                            <?php $eArray = explode(',',$data->elevation);
                                  for($a=0;$a<count($eArray)-1;$a++){  if(in_array($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']))
                                    {  $key = array_search($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']);   $color = $colorArr[$tyArray[$a]]['color'][$key];
                                    }
                                    else { $color = '';  }?>
                                  <td  bgcolor="<?=$color;?>"  style="border: 1px solid black;"><?=$eArray[$a]?></td>
                                  <?php }?>
                            </tr> 
                            <tr>
                       <th  style="border: 1px solid black;">CNHZ:</th>          
                            <?php $cnhzArray = explode(',',$data->cnhz);
                             for($a=0;$a<count($cnhzArray)-1;$a++){ 
                              if(in_array($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']))
                              {  $key = array_search($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']);   $color = $colorArr[$tyArray[$a]]['color'][$key];
                              }
                              else { $color = '';  }?>
                             <td  bgcolor="<?=$color;?>"  style="border: 1px solid black;"><?=$cnhzArray[$a]?></td>
                             <?php }?>
                           </tr> 
                           <tr  style="border: 1px solid black;">
                       <th>Constellation type:</th>
                       <?php //$tyArray = explode(',',$data->constellation_type);
                             for($a=0;$a<count($tyArray)-1;$a++){  if(in_array($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']))
                              {  $key = array_search($vArray[$a], $colorArr[$tyArray[$a]]['vehicle']);   $color = $colorArr[$tyArray[$a]]['color'][$key];
                              }
                              else { $color = '';  }?>
                             <td  bgcolor="<?=$color;?>" style="border: 1px solid black;"><?=$tyArray[$a]?></td>
                             <?php }?>
                           </tr>
                          </table>
                          
                    </td>
                    <td><?=!empty($data->device_name)?str_replace(',',' | ',$data->device_name):'NA'?></td>
                   
                    
                    <td><?=$data->created_at?></td>
                </tr>
            @empty
                <x-norecord colspan="11" />
            @endforelse

        </x-table>

         
    </x-card>
@endsection
