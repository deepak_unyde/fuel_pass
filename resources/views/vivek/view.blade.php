<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]); 
/*$deviceList = array(
  'redmi'=>'redmi',
  'samsung'=>'samsung'
);*/
 
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
    
        <x-slot name="header_right">
         <x-link href="{{  route('logger.index')  }}" title="Back"
                    icon="fa fa-graph" class="btn-primary"/>            
        </x-slot>
         
        <x-form with_button="search" method="get" action="{{ route('logger.graph') }}">
            <div class="row">
                <x-input name="s" title="Vehicle Id" class="col-md-2 col-lg-2 col-xl-2" form_type="v" input_class="digitonly" />
                <x-input name="group" title="Group" class="col-md-2 col-lg-2 col-xl-2" form_type="v" />
                <x-input name="fdate" title="From datetime" class="col-md-2 col-lg-2 col-xl-2" placeholder="YYYY-MM-DD H:i:s" form_type="v" />
                <x-input name="tdate"  placeholder="YYYY-MM-DD HH:ii:ss"  title="To datetime" class="col-md-2 col-lg-2 col-xl-2" form_type="v" />
                
                <div class="col-md-2 col-lg-2 col-xl-2">
                    <label for="device_name[]" class="required">Device Name</label>
                    <select class=" form-select  select2"  multiple="" type="select" id="device_name[]" name="device_name[]">
                  <option value="" <?php if(empty($_REQUEST['device_name'])){ echo 'selected="selected"';}?>>All Device</option>
                  <?php if($deviceList){ foreach($deviceList as $v){?>
                  <option value="<?=$v->device_name?>" <?php if(!empty($_REQUEST['device_name'])) { for($c=0;$c<count($_REQUEST['device_name']);$c++){ if($_REQUEST['device_name'][$c]==$v->device_name){ echo 'selected="selected"';}}}?>><?=$v->device_name?></option>
                  <?php }} ?>
                </select>
                
                </div>
                
                
                <!--x-select name="device_name[]" input_class="select2"  label_class="required" multiple
                title="Device Name" :options="$deviceList" type="select" form_type="v"/-->
                

                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>
        </x-form>
        
        <?php 
        $groupNo =  app('request')->input('group');
        $vehicleNo = app('request')->input('s');
        $graphArray = array();
        $childArr = array();
foreach ($loggers  as $data ) 
{
  /*echo "Vehicle : ".$data->vehicle_id;
  echo "<br/>";
  echo "constellation_type: ".$data->constellation_type;
  echo "<br/>";
  echo "azimuth: ".$data->azimuth;
  echo "<br/>";
  echo "<br/>";*/
  
  $constellationTypeArray=explode(',',$data->constellation_type);
  $vehicleArray=explode(',',$data->vehicle_id);
  $azimuthArray=explode(',',$data->azimuth);
  $elevationArray=explode(',',$data->elevation);
  $constellationFilterArray =array();
  //echo $groupNo;exit;
  $constellationFilterArray = array_filter($constellationTypeArray, function($number) use ($groupNo) {  
    if($number==$groupNo)
    {
      return $number;
    }
    
  });
  if($constellationFilterArray)
  {
    foreach($constellationFilterArray as $key=>$val)
    {
      
      if($vehicleArray[$key]==$vehicleNo)
      {
       $graphArray['created_at'][]=$data->created_at; 
       $graphArray['device_name'][]=$data->device_name; 
       $graphArray['vehicleNo'][]=$vehicleNo;
       $graphArray['groupNo'][]=$groupNo;
       $graphArray['constellation_type'][]=$constellationTypeArray[$key];
       $graphArray['vehicle_id'][]=$vehicleArray[$key];
       $graphArray['azimuth'][]=$azimuthArray[$key];
       $graphArray['elevation'][]=$elevationArray[$key];
        

       
      }
     
    }
   
    
    //echo "<pre/>";print_r($graphArray);
     }
 
  //$typeExplodeArr = array_unique($array);
  // echo "<pre/>";print_r($mapArray);
  
}
 //echo "<pre/>";print_r($graphArray);
?>     

  
<div class="row mt-4 row-cols-2">
            <div class="col" style="width:100%">
              <div class="card mb-4">
                <div class="card-header">Azimuth Chart</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="canvas-1"></canvas>
                  </div>
                </div>
              </div>
            </div>
</div>

<div class="row mt-4 row-cols-2">
            <div class="col" style="width:100%">
              <div class="card mb-4">
                <div class="card-header">Elevation</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="canvas-2"></canvas>
                  </div>
                </div>
              </div>
            </div>
</div>

    </x-card>
@endsection
@push('after-scripts')
<script src="{{ asset('js/chart.js/js/chart.min.js') }}"></script>
<script src="{{ asset('js/@coreui/chartjs/js/coreui-chartjs.js') }}"></script>
<!--script src="{{ asset('js/chart/charts.js') }}"></script-->
<script>
  let colorArr=[];
  <?php if($deviceList){ foreach($deviceList as $v){?>
    colorArr.push(getRandomColor());
  <?php  }}?>
     console.log(colorArr);
  const barChart = new Chart(document.getElementById('canvas-1'), {
  type: 'line',
  data: {
    labels: [<?php if($graphArray){ foreach ($graphArray['created_at']  as $key=>$data ) {?>'<?=$data?>', <?php }}?> ],
    datasets: [
      <?php if($deviceList){ $i=0;foreach($deviceList as $v){?>
      {
      label: '<?=$v->device_name?>',          
      backgroundColor: colorArr[<?=$i?>],
      borderColor:colorArr[<?=$i?>],
      pointBackgroundColor: colorArr[<?=$i?>],
      pointBorderColor: '#f00',
      data: [<?php if($graphArray){ foreach ($graphArray['azimuth']  as $key=>$data ) { if($v->device_name==$graphArray['device_name'][$key]) { echo $data; }   ?>, <?php }}?>]
        },
        <?php $i++;}} ?>   
]
  },
  options: {
    responsive: true
  }
}); 

 
const barChart_1 = new Chart(document.getElementById('canvas-2'), {
  type: 'line',
  data: {
    labels: [<?php if($graphArray){ foreach ($graphArray['created_at']  as $key=>$data ) {?>'<?=$data?>', <?php }}?> ],
    datasets: [
      <?php if($deviceList){ $i=0;foreach($deviceList as $v){?>
      {
      label: '<?=$v->device_name?>',          
      backgroundColor: colorArr[<?=$i?>],
      borderColor:colorArr[<?=$i?>],
      pointBackgroundColor: colorArr[<?=$i?>],
      pointBorderColor: '#f00',
      data: [<?php if($graphArray){ foreach ($graphArray['elevation']  as $key=>$data ) { if($v->device_name==$graphArray['device_name'][$key]) { echo $data; }   ?>, <?php }}?>]
        },
        <?php $i++;}} ?>   
]
  },
  options: {
    responsive: true
  }
}); 
  
function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
                }
  </script>

@endpush
