<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('petrol-pumps.create');

$tableHeader = ['#','Company','Code','Beacon Id','State','Petrol Price','Name','Address','Pincode', 'Status', 'Created At', 'AVG Rating', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('petrol-pump-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' =>$title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('petrol-pumps.index') }}">
            <div class="row">
                <x-select name="price_id" title="State" :options="$prices" form_type="v"  class="col-md-2" />    
              
                @if(loginUserRoleId()==1)
                <x-select name="company_id" title="Company" :options="$companies" form_type="v"  class="col-md-2" />
                @endif
                 
                <x-input name="code" title="Petrol Pump code" class="col-md-2 col-lg-2 col-xl-2" form_type="v" />
                <x-input name="s" title="Petrol Pump name" class="col-md-2 col-lg-2 col-xl-2" form_type="v" />
                

                <x-status search />

                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$petrolPumps" pagination="true">
            @forelse ($petrolPumps  as $petrolPump )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @if ($petrolPump->company)
                            {{ $petrolPump->company->company_name }}
                        @endif
                    </td>
                    <td>{{ $petrolPump->code }}</td>
                    <td>
                     {{ $petrolPump->beacon_id }}
                    </td>
                    <td>
                        @if ($petrolPump->price)
                            {{ $petrolPump->price->state_name }}
                        @endif
                    </td>
                    <td>
                        @if ($petrolPump->price)
                            {{ $petrolPump->price->petrol_cost }}
                        @endif
                    </td>
                    
                    <td>{{ $petrolPump->name }}</td>
                    <td>{{ $petrolPump->address }}</td>
                    <td>{{ $petrolPump->pincode }}</td>
                    <td>
                        <x-status text="{{ $petrolPump->status }}" />
                    </td>
                    <td>
                        <x-dt :date_time="$petrolPump->created_at" />
                    </td>
                    <td>
                    <x-link title="{{$petrolPump->rating}}" company_name="{{ $petrolPump->company->company_name }}"  petrol_pump_name="{{$petrolPump->name}}" data-toggle="modal" data-target="#model" class="btn btn-info" icon="" onclick="getUserAvgRating({{ $petrolPump->id }})" id="modal_{{ $petrolPump->id }}" />
                    </td>
                    <td>
                   
                        @can('petrol-pump-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('petrol-pumps.edit', $petrolPump->id) }}" />
                        @endcan
                        @can('petrol-pump-delete')
                            <x-delete action="{{ route('petrol-pumps.destroy', $petrolPump->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="12" />
            @endforelse

        </x-table>
    </x-card>
<x-modal title="Userwise Rating Average" id="model">
    <div class="modal-body">
            <div class="container" id="loaderdata"> </div>
    </div>
</x-modal>
@endsection
@push('after-scripts')
    <script>
        function getUserAvgRating(id, request_from='UserAvgRating')
        {
            $("#loaderdata").html('');
            var petrol_pump_name = $('#modal_'+id).attr('petrol_pump_name');
            var company_name = $('#modal_'+id).attr('company_name');
            $('.modal-title').text(petrol_pump_name+' ('+company_name+')');
            $.ajaxSetup({
                headers: {
                    _token: "{{ csrf_token() }}"
                }
            });
             
            if (id) {
              // addDisabled('.loader');
                $.ajax({
                    url: "{{ route('ajax.request') }}",
                    method: 'GET',
                    data: {
                        petrol_pump_id: id,
                        request_from:request_from
                    },
                    dataType: "html",
                    success: function(result) {  
                        $("#loaderdata").html(result);
                       // removeDisabled('.loader');
                       $('#model').modal('show');
                    }
                });
            } else {
                alert("No Record found");
            }

           
        }
         
    </script>
@endpush
