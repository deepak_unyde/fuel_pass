@php 
$tableHeader = ['#','User','Petrol Pump','Code','AVG Rating'];
@endphp

<div class="table-responsive">
    <table class="table table-striped table-hover mt-4">
        <thead>
            <tr class="table-dark ">
                <th>#</th>
                <th>User</th>
                <th>Petrol Pump</th>
                <th>Code</th>
                <th>User AVG Rating</th>
            </tr>
        </thead>
        <tbody>
        <?php if($userRating->count()){  $i=0;foreach($userRating as $avg){?>
            <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @if ($avg->user)
                            {{ $avg->user->name }} ( {{ $avg->user->mobile }})
                        @endif
                    </td>
                    <td>
                        @if ($avg->petrolPump)
                            {{ $avg->petrolPump->name }}  
                        @endif
                    </td>
                    <td>
                        @if ($avg->petrolPump)
                            {{ $avg->petrolPump->code }} 
                        @endif
                    </td>
                    
                    <td>{{$avg->avg_rating}}</td>
                </tr>
        <?php }} else {?>    
            <tr>
            <td colspan="5" class="text-white text-center bg-secondary">No record found.</td>
            </tr>
         <?php }?>   
        </tbody>
    </table>
</div>

{{--<x-table :table_header="$tableHeader" :data="$userRating" pagination="true">
            @forelse ($userRating  as $avg )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @if ($avg->user)
                            {{ $avg->user->name }} ( {{ $avg->user->mobile }})
                        @endif
                    </td>
                    <td>
                        @if ($avg->petrolPump)
                            {{ $avg->petrolPump->name }}  
                        @endif
                    </td>
                    <td>
                        @if ($avg->petrolPump)
                            {{ $avg->petrolPump->code }} 
                        @endif
                    </td>
                    
                    <td>{{$avg->avg_rating}}
                    <x-link title="{{$avg->avg_rating}}" petrol_pump_id="{{ $avg->petrol_pump_id }}"  user_id="{{$avg->user_id}}" data-toggle="modal" data-target="#model" class="  btn btn-info" icon="" onclick="getUserRating({{ $avg->id }})" id="modal_{{ $avg->id }}" />
                    </td>
                </tr>
            @empty
                <x-norecord colspan="5" />
            @endforelse
</x-table>--}}