@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {
                    company_id: "required",
                    price_id: "required",
                    beacon_id: "required",
                    name: "required",
                    code: "required",
                    address: "required",
                    pincode: "required",
                    latitude: "required",
                    longitude: "required",
                    status: "required",
                    opening_time: "required",
                    closing_time:"required",
                     
                }
            });
        });
    </script>
@endpush
