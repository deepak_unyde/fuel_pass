<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('petrol-pumps.index');

@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($petrolPump, ['method' => 'PUT', 'class' => 'form', 'route' => ['petrol-pumps.update', $petrolPump->id]]) !!}

        @if(loginUserRoleId()==1)
            <x-select name="company_id" title="Company" :options="$companies" type="select" label_class="required" />
            @else
            <input type="hidden" name="company_id" value="{{loginUserCompanyId()}}">
            @endif
        <x-select name="price_id" title="State" :options="$prices" type="select" label_class="required" />
            <x-input name="code" title="Petrol Pump code" label_class="required" />
            <x-input name="beacon_id" title="Petrol Pump Beacon Id" label_class="required" />
            <x-input name="name" title="Petrol Pump name" label_class="required" />
            <x-input type="textarea" name="address" title="Petrol address" />
            <x-input name="pincode" title="Pincode" label_class="required" />
            <x-input name="latitude" title="Latitude" label_class="required" />
            <x-input name="longitude" title="Longitude" label_class="required" />
            <x-input name="opening_time" title="Opening Time (00:00:00)" label_class="required" />
            <x-input name="closing_time" title="Closing Time (00:00:00)" label_class="required" />
            <x-status />
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('petrol-pumps.common')
