<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('promotions.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        <x-form method="post" enctype="multipart/form-data" action="{{ route('promotions.store') }}">
            <x-input name="title" title="Promotion Title" label_class="required" />
            <x-input name="heading" title="Promotion Heading" label_class="required" />

            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label required">Start Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control calendar_1" placeholder="Start Date" name="from_date" value="<?= app('request')->input('from_date')?>" required />
                </div>
        </div>
        <div class="mb-3 row">
            <label class="col-sm-4 col-form-label required">End Date</label>
            <div class="col-sm-8">
                <input type="text" class="form-control calendar_1" placeholder="End Date" name="to_date" value="<?= app('request')->input('to_date')?>" required />
            </div>
        </div>
            <x-input name="url" title="Promotion link" placeholder="eg: http:\\www.google.com" label_class="required" />
            <x-input  name="short_description" title="Promotion Short Description" label_class="required"  />
            <x-input type="textarea" name="description" title="Promotion Description" label_class="required"  />
            <x-input name="promotion_image" title="Promotion Image" type="file" label_class="required" />
            <x-status />
            
            
        </x-form>
    </x-card>
@endsection
@include('promotions.common')
