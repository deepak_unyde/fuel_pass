<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('promotions.create');

$tableHeader = ['#', 'Image', 'Title','Heading','Short Description', 'Start Date', 'End Date','Target Url', 'Status', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('company-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => $title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        @if(loginUserRoleId()==1)
        <x-form with_button="search" method="get" action="{{ route('promotions.index') }}">
            <div class="row">
                <x-input name="s" title="Title / Heading" class="col-md-4 col-lg-4 col-xl-3" form_type="v" />
                <x-status search />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>
        </x-form>
        @endif
        <x-table :table_header="$tableHeader" :data="$promotions" pagination="true">
            @forelse ($promotions  as $company )
                @php
                 
                $promotion_image =   ($company->getFirstMediaUrl('promotion_image'))?$company->getFirstMediaUrl('promotion_image'):asset('img/default.png');
                @endphp
                <tr>
                    <td>{{ ++$i }}</td>
                    <td> <img class="avatar-img avatar-img-small"
                        src="{{ $promotion_image }}"><span class=""></span></td>
                    <td>{{ $company->title }}</td>
                    <td>{{ $company->heading }}</td>
                    <td>{{ $company->short_description }}</td>
                    <td>{{ $company->from_date }}</td>
                    <td>{{ $company->to_date }}</td>
                    <td>{{ $company->url }}</td>
                    <td>
                        <x-status text="{{ $company->status }}" />
                    </td>
                    <td>
                        <x-dt :date_time="$company->created_at" />

                    </td>
                    <td>
                        @can('company-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('promotions.edit', $company->id) }}" />
                        @endcan
                        @can('company-delete')
                            <x-delete action="{{ route('promotions.destroy', $company->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="11" />
            @endforelse

        </x-table>
    </x-card>
@endsection
