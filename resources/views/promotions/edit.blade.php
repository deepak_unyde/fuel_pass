<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('promotions.index');
$promotion_image = $promotion->getFirstMediaUrl('promotion_image');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($promotion, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form', 'route' => ['promotions.update', $promotion->id]]) !!}
        <x-input name="title" title="Promotion Title" label_class="required" />
            <x-input name="heading" title="Promotion Heading" label_class="required" />

            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label ">Promotion Start Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control from_date" placeholder="Package Start Date" name="from_date" value="<?=($promotion->from_date)?date('d-m-Y',strtotime($promotion->from_date)):''?>" required />
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-4 col-form-label ">Promotion End Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control to_date" placeholder="Package End Date" name="to_date" value="<?=($promotion->to_date)?date('d-m-Y',strtotime($promotion->to_date)):''?>" required />
                </div>
            </div>
            <x-input name="url" title="Promotion link" placeholder="eg: http:\\www.google.com" label_class="required" />
            <x-input  name="short_description" title="Promotion Short Description" label_class="required"  />
            <x-input type="textarea" name="description" title="Promotion Description" label_class="required"  />
            <x-input name="promotion_image" title="Promotion Image" type="file"   :selected="$promotion_image"  />
             
            <x-status />
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('promotions.common')
