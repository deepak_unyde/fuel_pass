@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({

                rules: {
                    title: {
                        required: true,
                    },
                    heading: {
                        required: true,
                    },
                    url:{
                        required: true,
                    },
                    short_description: {
                        required: true,
                    },
                    description: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                    from_date: {
                        required: true,
                         
                    },
                    to_date: {
                        required: true,
                        
                    },
                    
                }
                 
            });
        });
    </script>
@endpush
