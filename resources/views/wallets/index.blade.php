<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('wallets.create');

$tableHeader = ['#', 'Name', 'Mobile',  'Wallet Amount', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('wallet-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' =>$title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('wallets.index') }}">
            <div class="row">
                <x-select name="user_id" title="User" :options="$users" form_type="v"  class="col-md-3" />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$wallets" pagination="true">
            @forelse ($wallets  as $wallet )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @if ($wallet->user)
                            {{ $wallet->user->name }}
                        @endif
                    </td>
                    <td>
                        @if ($wallet->user)
                            {{ $wallet->user->mobile }}
                        @endif
                    </td>
                    <td>{{ $wallet->amount }}</td>
                      
                    <td>
                        <x-dt :date_time="$wallet->created_at" />
                    </td>
                    <td>
                        @can('wallet-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('wallets.edit', $wallet->id) }}" />
                        @endcan
                        @can('wallet-delete')
                            <x-delete action="{{ route('wallets.destroy', $wallet->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="6" />
            @endforelse

        </x-table>
    </x-card>
@endsection
