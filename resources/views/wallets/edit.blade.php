<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('wallets.index');

@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($wallet, ['method' => 'PUT', 'class' => 'form', 'route' => ['wallets.update', $wallet->id]]) !!}
        <x-select name="user_id" title="User" :options="$users" type="select" label_class="required" />
            <x-input name="amount" title="Wallet Amount" label_class="required" />
           
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('wallets.common')
