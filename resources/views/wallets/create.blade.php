<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('wallets.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('wallet-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" />
            </x-slot>
        @endcan
        <x-form method="post" action="{{ route('wallets.store') }}">
            <x-select name="user_id" title="User" :options="$users" type="select" label_class="required" />
            <x-input name="amount" title="Wallet Amount" label_class="required"   />
             
        </x-form>
    </x-card>
@endsection
@include('wallets.common')
