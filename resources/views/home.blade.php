@extends('layouts.admin')
@php
$ntype=array('daily'=>'Daily','weekly'=>'Weekly','monthly'=>'Monthly');
@endphp
@section('title')
{{ $title }}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- __('You are logged in!') --}}


                    <!---- Dashlet Start---->    
    <div class="row">
        <div class="col-sm-6 col-lg-3 mb-3">
            <div class="card text-white bg-primary">
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-gas-pump"></i>  {{$data['petrol_pump_count']}}
              </div>
                <div>Total Active Petrol Pump</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('petrol-pumps.index') }}"> More info</a> 
            </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-3">
            <div class="card  text-white bg-info">
           
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-user"></i> {{$data['user_count']}}</div>
                        <div>Total Active Users</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('users.index') }}"> More info</a> 
            </div>
             
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-3">
            <div class="card  text-white bg-secondary">
            
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-gift"></i> {{$data['package_count']}}</div>
                        <div>Total Active Package Plan</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('packages.index') }}"> More info</a> 
            </div>    
             
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-3">
              <div class="card  text-white bg-dark">
                
            <div class="card-body">
            <div class="fs-4 fw-semibold"><i class="fas fa-gift"></i> {{$data['redeemGift_count']}}</div>
              <div>Total Active Redeem Offer</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('redeem-gifts.index') }}"> More info</a> 
            </div>    
             </div>
        </div>

        
<!-- 2nd row-->
        <div class="col-sm-6 col-lg-3 mb-3">
            <div class="card  text-white bg-warning">
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i> {{ ($todayTransactionWalletHistory[0])?$todayTransactionWalletHistory[0]->Wallet:0 }} </div>
                        <div>Today Wallet</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('transactions.index') }}"> More info</a> 
            </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-3">
              <div class="card  text-white bg-danger">
                <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i> {{ ($todayTransactionWalletHistory[0])?$todayTransactionWalletHistory[0]->Transaction:0 }}</div>
                    <div>Today Transaction</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('transactions.index') }}"> More info</a> 
            </div>
              </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-3">
            <div class="card text-white bg-info">
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-award"></i> {{ ($todayTransactionWalletHistory[0])?$todayTransactionWalletHistory[0]->Reward:0 }}</div>
                        <div>Today Reward Point</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('transactions.index') }}"> More info</a> 
            </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-3">
          <div class="card text-white bg-success">
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i> {{ ($todayTransactionWalletHistory[0])?$todayTransactionWalletHistory[0]->Cashback:0 }} 
              </div>
                <div>Today Cashback</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('transactions.index') }}"> More info</a> 
            </div>
          </div>
        </div>

     
<!-- 3nd row-->
<div class="col-sm-6 col-lg-3 mb-3">
            <div class="card   text-white bg-warning">
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i> {{ ($totalTransactionWalletHistory[0])?$totalTransactionWalletHistory[0]->Wallet:0 }} </div>
                        <div>Total Wallet</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('transactions.index') }}"> More info</a> 
            </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-3">
              <div class="card  text-white bg-danger">
                <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i>{{ ($totalTransactionWalletHistory[0])?$totalTransactionWalletHistory[0]->Transaction:0 }}</div>
                    <div>Total Transaction</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('transactions.index') }}"> More info</a> 
            </div>
              </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-3">
            <div class="card text-white bg-info">
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-award"></i> {{ ($totalTransactionWalletHistory[0])?$totalTransactionWalletHistory[0]->Reward:0 }}</div>
                        <div>Total Reward Point</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('transactions.index') }}"> More info</a> 
            </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3 mb-3">
          <div class="card text-white bg-success">
            <div class="card-body">
              <div class="fs-4 fw-semibold"><i class="fas fa-rupee-sign"></i>   {{ ($totalTransactionWalletHistory[0])?$totalTransactionWalletHistory[0]->Cashback:0 }}
              </div>
                <div>Total Cashback</div>
            </div>    
            <div class="card-footer px-3 py-2">
              <a class="text-white text-decoration-none" href="{{ route('transactions.index') }}"> More info</a> 
            </div>
          </div>
        </div>
      
<!---End Rows 3---->             

        

<!---Graph---->
<!-- Chart start --->
           <div class="row row-cols-2">
            <!--div class="col">
              <div class="card mb-4">
                <div class="card-header">Line Chart</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="canvas-1"></canvas>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col">
              <div class="card mb-4">
                <div class="card-header">Doughnut Chart</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="canvas-3"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card mb-4">
                <div class="card-header">Radar Chart</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="canvas-4"></canvas>
                  </div>
                </div>
              </div>
            </div-->

            <div class="col" style="width:100%">
              <div class="card mb-4">
               
            
            <div class="card-header">
              
            <div class="row">
                    <div class="col-md-3">Petrol Pump Transaction</div> 
                    <div class="col-md-2"><input type="text" class="form-control calendar" placeholder="From Date" name="from_date" id="from_date" value="{{date('d-m-Y', strtotime('-7 days'))}}" /></div> 
                    <div class="col-md-2"><input type="text" class="form-control calendar" placeholder="To Date" name="to_date" id="to_date" value="{{date('d-m-Y')}}"  /></div> 
                    <div class="col-md-2"><select class=" form-select" type="select" id="ntype" name="ntype">
              <option value="date">Daily</option>
              <option value="week">Weekly</option>
              <option value="month">Monthly</option>
            </select></div> 

            <div class="col-md-2"><select class=" form-select" type="select" id="company_id" name="company_id">
              <option value="">Select Company</option>
              <?php
              if($companies){ foreach($companies as $k=>$v){?>
                <option value="{{$k}}">{{$v}}</option>
              <?php }} ?>
              
            </select></div> 
                    <div class="col-md-1"><button onclick="getSearch()" class="btn btn-primary transaction_graph_search_btn" id="transaction_graph_search_btn">Search</button></div> 
            </div> 

           
            </div>
                <div class="card-body">
                  <div class="c-chart-wrapper" id="transactionGraph">
                    <canvas id="transaction-graph"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="col" style="width:100%">
              <div class="card mb-4">
                <div class="card-header">Today/Total History (Bar Chart)</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="canvas-2"></canvas>
                  </div>
                </div>
              </div>
            </div>

            
            <div class="col">
              <div class="card mb-4">
                <div class="card-header">Today/Total History (pieChart)</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="graph_1"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <!--div class="col">
              <div class="card mb-4">
                <div class="card-header">Total Transaction/Wallet/Cashback/Reward Points History</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="graph_2"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card mb-4">
                <div class="card-header">Polar Area Chart</div>
                <div class="card-body">
                  <div class="c-chart-wrapper">
                    <canvas id="canvas-6"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
<!----END---->
                </div>
            </div>
        </div>
    </div>
    
@endsection
@push('after-scripts')
<script src="{{ asset('js/chart.js/js/chart.min.js') }}"></script>
<script src="{{ asset('js/@coreui/chartjs/js/coreui-chartjs.js') }}"></script>
<!--script src="{{ asset('js/chart/charts.js') }}"></script-->
<script>
 let colorArr=[];
  <?php  for ($i=0;$i<count((array)$totalTransactionWalletHistory[0]);$i++){?>
    colorArr.push(getRandomColor());
  <?php  }?>

  const pieChart = new Chart(document.getElementById('graph_1'), {
  type: 'pie',
  data: {
    labels: ['<?php  echo implode("','",array_keys(((array)$totalTransactionWalletHistory[0])));?>'],
    datasets: [{
      data: [<?php  echo implode(",",((array)$totalTransactionWalletHistory[0]));?>],
     // backgroundColor: ["'"+colorArr.join("','")+"'"],
     // hoverBackgroundColor: ["'"+colorArr.join("','")+"'"]
     backgroundColor: ['#dc3545', '#0d6efd', '#198754','#fd7e14','#6610f2','#4f5d73'],
      hoverBackgroundColor: ['#dc3545', '#0d6efd', '#198754','#fd7e14','#6610f2','#4f5d73']
    }]
  },
  options: {
    responsive: true
  }
});  
var barChart = new Chart(document.getElementById('canvas-2'), {
  type: 'bar',
  data: {
    labels: ['<?php  echo implode("','",array_keys(((array)$todayTransactionWalletHistory[0])));?>'],
    datasets: [
      {
      label: 'Today History',          
      backgroundColor: colorArr[0],
      borderColor:colorArr[0],
      highlightFill: colorArr[0],
      highlightStroke: colorArr[0],
      data: [<?php  echo implode(",",((array)$todayTransactionWalletHistory[0]));?>]
      },
      {
      label: 'Total History',          
      backgroundColor: colorArr[1],
      borderColor:colorArr[1],
      highlightFill: colorArr[1],
      highlightStroke: colorArr[1],
      data: [<?php  echo implode(",",((array)$totalTransactionWalletHistory[0]));?>]
      },
      ]
  },
  options: {
    responsive: true
  }
}); 


function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
                }

 
function getSearch() 
  {
    $("#from_date").removeClass('is-invalid');
    $("#to_date").removeClass('is-invalid');
    $("#ntype").removeClass('is-invalid');
    var from_date = $('#from_date').val();
    var to_date = $('#to_date').val();
    var ntype = $('#ntype').val();
    var ntypeText = $('#ntype option:selected').text();
    var error = false;
    if(from_date.length < 5)
    {
         error =true;
        $("#from_date").addClass('is-invalid');
    }
    if(to_date.length < 5)
    {
    $("#to_date").addClass('is-invalid');
    error =true;
    }
    if(ntype.length < 4)
    {
        $("#ntype").addClass('is-invalid');
        error =true;
    }
    if (error==false)
     {
    var labelName = ntypeText+" Transaction";
    var labelArrKey = [];
    var labelArrVal = [];
                addDisabled('.transaction_graph_search_btn');
                $('.transaction_graph_search_btn').html('Wait..');
                $.ajax({
                    url: "{{ route('ajax.request') }}",
                    method: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date,
                        ntype:ntype,
                        request_from:"Home"
                    },
                    dataType: "JSON",
                    success: function(result) {
                        for(var i=0; i<result.length; i++)
                        {
                            labelArrKey.push(result[i].Date);
                            labelArrVal.push(result[i].amount);
                           console.log(result[i].Date);
                        }
 
                        generateChart(labelName ,labelArrKey ,labelArrVal);
                        console.log(result);
                        removeDisabled('.transaction_graph_search_btn');
                        $('.transaction_graph_search_btn').html('Search');
                    }
                });
            } 
    }

$(document ).ready(function()
 {
     
    console.log( "ready!" );
    var labelName ="Daily Transaction";
    var labelArrKey = [];
    var labelArrVal = [];
    <?php if(!empty($data['transaction_graph'])){ for($i=0;$i<count($data['transaction_graph']);$i++){ ?>
        labelArrKey.push('<?=$data['transaction_graph'][$i]->Date?>');
        labelArrVal.push('<?=$data['transaction_graph'][$i]->amount?>');
    <?php }} ?>
    generateChart(labelName ,labelArrKey ,labelArrVal );
    
    console.log(labelArrKey);
});   
function generateChart(labelName='Transaction',labelArrKey=[],labelArrVal=[])
 {
    $('#transactionGraph').html('');    
    $('#transactionGraph').html(`<canvas id="transaction-graph"></canvas>`);
var barChart = new Chart(document.getElementById('transaction-graph'), {
  type: 'bar',
  data: {
    labels: labelArrKey,
    datasets: [{
        label:labelName, 
      backgroundColor: 'rgba(220, 220, 220, 0.5)',
      borderColor: 'rgba(220, 220, 220, 0.8)',
      highlightFill: 'rgba(220, 220, 220, 0.75)',
      highlightStroke: 'rgba(220, 220, 220, 1)',
      data: labelArrVal
    }]
  },
  options: {
    responsive: true
  }
    }); 
 }
</script>

@endpush
