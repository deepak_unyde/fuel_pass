@component('mail::message')
# Welcome to {{ config('app.name') }}
    
Your username :<strong>{{ $data->email }}</strong><br>
Your Password :<strong>{{ $data->password }}</strong>

@component('mail::button', ['url' => config('app.url')])
Login
@endcomponent

Thanks,<br>

{{ config('app.name') }}
@endcomponent
