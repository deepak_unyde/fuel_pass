<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('services.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        <x-form method="post" enctype="multipart/form-data" action="{{ route('services.store') }}">
           <x-input name="service_name" title="Service name" label_class="required" />
           <x-input name="service_image" title="Service Image" type="file"  label_class="required" />
           <x-input name="amount" title="Service Cost" label_class="required" input_class="digitonly" />
           <x-input name="short_description" title="Service Short description" label_class="required"   />
           <x-input type="textarea" name="description" title="Service description" label_class="required" />
           <x-status />
        </x-form>
    </x-card>
@endsection
@include('services.common')
