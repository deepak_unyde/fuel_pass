<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('services.index');
$service_image = $service->getFirstMediaUrl('service_image');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($service, ['method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form', 'route' => ['services.update', $service->id]]) !!}
        <x-input name="service_name" title="Service name" label_class="required" />
           <x-input name="service_image" title="Service Image" type="file" :selected="$service_image" />
           <x-input name="amount" title="Service Cost" label_class="required" input_class="digitonly" />
            <x-input name="short_description" title="Service Short description" label_class="required"   />
            <x-input type="textarea" name="description" title="Service description" label_class="required" />
            <x-status />
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('services.common')
