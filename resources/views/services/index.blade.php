<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('services.create');

$tableHeader = ['#', 'Image', 'Service Name','Amount','Short Description','Description', 'Status', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('service-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => $title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        @if(loginUserRoleId()==1)
        <x-form with_button="search" method="get" action="{{ route('services.index') }}">
            <div class="row">
                <x-input name="s" title="Service name" class="col-md-4 col-lg-4 col-xl-3" form_type="v" />
                <x-status search />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>
        </x-form>
        @endif
        <x-table :table_header="$tableHeader" :data="$services" pagination="true">
            @forelse ($services  as $service )
                @php
                   $service_image =   ($service->getFirstMediaUrl('service_image'))?$service->getFirstMediaUrl('service_image'):asset('img/default.png');
                @endphp
                <tr>
                    <td>{{ ++$i }}</td>
                    <td> <img class="avatar-img avatar-img-small"
                        src="{{ $service_image }}"><span class=""></span></td>
                    <td>{{ $service->service_name }}</td>
                    <td>{{ $service->amount }}</td>
                    <td class="col-md-2">{{ $service->short_description }}</td>
                    <td class="col-md-2">{{ $service->description }}</td>
                    <td>
                        <x-status text="{{ $service->status }}" />
                    </td>
                    <td>
                        <x-dt :date_time="$service->created_at" />

                    </td>
                    <td>
                    <x-link title="View Slot" service_name="{{$service->service_name }}"  data-toggle="modal" data-target="#model" class="btn btn-info" icon="" onclick="serviceSlot({{ $service->id }},'Service')" id="modal_{{ $service->id }}" />

                        @can('service-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('services.edit', $service->id) }}" />
                        @endcan
                        @can('service-delete')
                            <x-delete action="{{ route('services.destroy', $service->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="8" />
            @endforelse

        </x-table>
    </x-card>
<x-modal title="Service Time Slot" id="model">
    <div class="modal-body">
        <div class="container">
        <form method="post"  action="{{ route('services.add') }}">
            <input type="hidden" value="0" id="set_service_id" name="service_id" />
       @csrf 
        <div class="row">
        <div class="form-group col-md-4">
			<label for="slot_start_time" class="">Slot Start Time</label>
			<input placeholder="00:00" id="slot_start_times" class="form-control "  name="slot_start_time" type="text" required />
		</div>
		
		<div class="form-group col-md-4">
			<label for="slot_end_time" class="">Slot End Time</label>
			<input placeholder="00:00" id="slot_end_time" class="form-control "  name="slot_end_time" type="text" required />
		</div>
		
		<div class="form-group col-md-2">
			<label for="slot_limit" class="">Slot Limit</label>
			<input placeholder="Slot limit" id="slot_limit" class="form-control digitonly"  name="slot_limit" type="text" required />
		</div>
		
        <div class="form-group col-md-2 col-lg-3 col-xl-2">
			<label for=""> &nbsp; </label>
			<button class="btn btn-primary form-control" type="submit" icon="fa-search"> <i class="fa fa-plus" aria-hidden="true"></i> Add </button>
		</div>
        </div>   
        </form>
        <div id="loaderdata" class="mt-4"></div>
        </div>
    </div>
    <div class="modal-footer">&nbsp;
    </div>        
</x-modal>    
@endsection
@push('after-scripts')
    <script>
        function serviceSlot(id, request_from)
        {
            $("#loaderdata").html('');
            var service_name = $('#modal_'+id).attr('service_name');
            $('.modal-title').text(service_name);
            $('#set_service_id').val(id);
            $.ajaxSetup({
                headers: {
                    _token: "{{ csrf_token() }}"
                }
            });
             
            if (id) {
           //    addDisabled('.loader');
                $.ajax({
                    url: "{{ route('ajax.request') }}",
                    method: 'GET',
                    data: {
                        service_id: id,
                        request_from:request_from
                    },
                    dataType: "html",
                    success: function(result) { 
                        $("#loaderdata").html(result);
                     //   removeDisabled('.loader');
                       $('#model').modal('show');
                    }
                });
            } else {
                alert("No Record found");
            }

           
        }
         
    </script>
@endpush
