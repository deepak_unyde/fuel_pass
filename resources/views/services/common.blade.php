@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({

                rules: {
                    service_name: {
                        required: true
                    },
                    amount: {
                        required: true,
                        number: true
                    },
                    description: {
                        required: true
                    },
                    
                    short_description: {
                        required: true
                    },

                    status: {
                        required: true,
                    },
                    
                }
                 
            });
        });
         
    </script>
@endpush
