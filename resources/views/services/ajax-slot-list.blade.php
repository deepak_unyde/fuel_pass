<div class="table-responsive">
    <table class="table table-striped table-hover mt-4">
        <thead>
            <tr class="table-dark ">
				<th>#</th>
				<th>Start Time</th>
				<th>End Time</th>
                <th>Limit</th>
				<th>Action</th>
            </tr>
        </thead>
		<tbody>
            <?php if($serviceDetails){ $i=0;foreach($serviceDetails as $detail){?>
			<tr>
				<td>{{++$i;}}</td>
				<td>{{$detail->slot_start_time}}</td>
				<td>{{$detail->slot_end_time}}</td>
                <td>{{$detail->slot_limit}}</td>
				<td> <x-delete action="{{ route('services.delete', $detail->id) }}" /></td>
			</tr>
            <?php }}?>
		</tbody>
    </table>
</div>