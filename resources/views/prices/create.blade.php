<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('prices.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        <x-form method="post" action="{{ route('prices.store') }}">
            <x-input name="state_name" title="State name" label_class="required" />
            <x-input name="state_code" title="State code" label_class="required" />
            <x-input name="petrol_cost" title="Petrol cost" label_class="required"   input_class="digitonly"  />
            <!--<x-input name="diesel_cost" title="Diesel cost" label_class="required" input_class="digitonly"    />
            <x-input name="cng_cost" title="CNG cost" label_class="required"  input_class="digitonly"   />-->
        </x-form>
    </x-card>
@endsection
@include('prices.common')
