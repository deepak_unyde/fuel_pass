<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('prices.create');

$tableHeader = ['#', 'State Name','State Code','Today Petrol Cost','Updated At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('price-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' => $title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('prices.index') }}">
            <div class="row">

                <x-input name="s" title="State Code" class="col-md-4 col-lg-4 col-xl-3" form_type="v" />

                <x-input name="state_name" title="State name" class="col-md-4 col-lg-4 col-xl-3" form_type="v" />
                
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$prices" pagination="true">
            @forelse ($prices  as $price )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $price->state_name }}</td>
                    <td>{{ $price->state_code }}</td>
                    <td><i class="fa fa-rupee-sign"></i>  {{ $price->petrol_cost }}</td>
                     
                    <td>
                        <x-dt :date_time="$price->updated_at" />

                    </td>
                    <td>
                        @can('price-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('prices.edit', $price->id) }}" />
                        @endcan
                        @can('price-delete')
                            <x-delete action="{{ route('prices.destroy', $price->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="8" />
            @endforelse

        </x-table>
    </x-card>
@endsection
