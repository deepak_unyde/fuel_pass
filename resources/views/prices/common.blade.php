@push('after-scripts')
    <script>
 
        $(document).ready(function() {
            $(".form").validate({
                rules: {
                    state_name:"required",
                    state_code:"required",
                    petrol_cost: {
                        required: true,
                        number: true,
                        min: 0
                        }
                        /*,
                    diesel_cost: {
                        required: true,
                        number: true,
                        min: 0
                        },
                    cng_cost: {
                        required: true,
                        number: true,
                        min: 0
                        }*/
                }
                 
            });
        });
    </script>
@endpush
