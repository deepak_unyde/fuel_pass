<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
 
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
        </x-slot>
        {!! Form::model($price, ['method' => 'PUT', 'class' => 'form', 'route' => ['prices.update', $price->id]]) !!} 
        <x-input name="state_name" title="State name" label_class="required" />
        <x-input name="state_code" title="State code" label_class="required" />
        <x-input name="petrol_cost" title="Petrol Price (per Litre)" label_class="required" input_class="digitonly"  />
       <!-- <x-input name="diesel_cost" title="Diesel Price (per Litre)" label_class="required" input_class="digitonly"  />
        <x-input name="cng_cost" title="CNG Price (per litre)" label_class="required" input_class="digitonly"  />-->
            
             
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('prices.common')
