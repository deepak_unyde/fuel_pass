<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.edit', ['title' => $title]);
$href = route('vehicles.index');

@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        <x-slot name="header_right">
            <x-link href="{{ $href }}" />
        </x-slot>
        {!! Form::model($vehicle, ['method' => 'PUT', 'class' => 'form', 'route' => ['vehicles.update', $vehicle->id]]) !!}
        <x-select name="user_id" title="User" :options="$users" type="select" label_class="required" />
            <x-select name="vehicle_type" title="Vehicle type" :options="$vehicleType" type="select" label_class="required" />
            <x-select name="fuel_type" title="Fuel type" :options="$fuelType" type="select" label_class="required" />
            <x-input name="registration_number" title="Enter Vehicle Registration No" label_class="required"   />
            <x-status />
           
        <div class="mb-3 row">
            <div class="col-sm-8 offset-sm-4">
                <x-button />
            </div>
        </div>
        {!! Form::close() !!}
    </x-card>
@endsection
@include('vehicles.common')
