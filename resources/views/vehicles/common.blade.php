@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".form").validate({
                rules: {
                    user_id: "required",
                    vehicle_type:"required",
                    fuel_type:"required",
                    registration_number:"required",
                    status:"required"
                    
                }
            });
        });

$(function() {
  $('.col-sm-8').on('keydown', '.digitvalidate', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
});    
    </script>
@endpush
