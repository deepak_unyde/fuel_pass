<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.create', ['title' => $title]);
$href = route('mileages.index');
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('mileage-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" />
            </x-slot>
        @endcan
        <x-form method="post" action="{{ route('vehicles.store') }}">
            <x-select name="user_id" title="User" :options="$users" type="select" label_class="required" />
            <x-select name="vehicle_type" title="Vehicle type" :options="$vehicleType" type="select" label_class="required" />
            <x-select name="fuel_type" title="Fuel type" :options="$fuelType" type="select" label_class="required" />
            
            <x-input name="registration_number" title="Enter Vehicle Registration No" label_class="required"   />
            <x-status />
             
        </x-form>
    </x-card>
@endsection
@include('mileages.common')
