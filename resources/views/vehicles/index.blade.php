<!-- Layout  -->
@extends('layouts.admin')
@php
$title = __('messages.list', ['title' => $title]);
$href = route('vehicles.create');

$tableHeader = ['#', 'Name', 'Mobile', 'Registration No', 'Fuel Type', 'Vehicle Type', 'Created At', 'Action'];
@endphp
@section('title')
    {{ $title }}
@endsection
@section('content')
    <x-card :title="$title">
        @can('vehicle-create')
            <x-slot name="header_right">
                <x-link href="{{ $href }}" title="{{ __('messages.create', ['title' =>$title]) }}"
                    icon="fa-plus-circle" />
            </x-slot>
        @endcan
        <x-form with_button="search" method="get" action="{{ route('vehicles.index') }}">
            <div class="row">
                <x-select name="user_id" title="User" :options="$users" form_type="v"  class="col-md-2" />
                <x-select name="fuel_type" title="Fuel type" :options="$fuelType" form_type="v"  class="col-md-2" />
                <x-select name="vehicle_type" title="Vehicle type" :options="$vehicleType" form_type="v"  class="col-md-2" />
                <x-input name="s" title="Registration No" class="col-md-4 col-lg-4 col-xl-3" form_type="v" />
                <x-button type="primary form-control" text="Search" icon="fa-search" button_type="search" />
            </div>

        </x-form>
        <x-table :table_header="$tableHeader" :data="$vehicles" pagination="true">
            @forelse ($vehicles  as $vehicle )
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>
                        @if ($vehicle->user)
                            {{ $vehicle->user->name }}
                        @endif
                    </td>
                    <td>
                        @if ($vehicle->user)
                            {{ $vehicle->user->mobile }}
                        @endif
                    </td>
                    <td>{{ $vehicle->registration_number }}</td>
                    <td>{{ $vehicle->fuel_type }}</td>
                    <td>{{ $vehicle->vehicle_type }}</td>
                      
                    <td>
                        <x-dt :date_time="$vehicle->created_at" />
                    </td>
                    <td>
                        @can('vehicle-edit')
                            <x-link title="Edit" icon="fa-edit" href="{{ route('vehicles.edit', $vehicle->id) }}" />
                        @endcan
                        @can('vehicle-delete')
                            <x-delete action="{{ route('vehicles.destroy', $vehicle->id) }}" />
                        @endcan
                    </td>
                </tr>
            @empty
                <x-norecord colspan="8" />
            @endforelse

        </x-table>
    </x-card>
@endsection
