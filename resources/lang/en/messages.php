<?php
return [
    'list' => ':title List',
    'create' => 'Add New :title',
    'edit' => 'Edit :title',
    'detail' => ':title Detail',
    'back' => 'Back',
    'insert' => ' The :title has been added successfully.',
    'update' => ' The :title has been updated successfully.',
    'delete' => ' The :title has been deleted successfully.',
    'try_again' => 'Please try again!',
    'no_record' => 'No record found.',
    'assign' => ':Title to :name successfully'
];
