<?php

namespace App\Imports;

use App\Models\ExistingAsset;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
//use Session;
use Illuminate\Support\Facades\Session;

class ImportData implements ToModel, WithStartRow
{
    private $rows = 0;
    public $existing_asset_import_id, $category, $duplicateRecord = array();
    public function __construct($existing_asset_import_id, $category)
    {
        $this->existing_asset_import_id = $existing_asset_import_id;
        $this->category = $category;
        Session::put('duplicateRecord', array());
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function startRow(): int
    {
        return 2;
    }


    public function model(array $row)
    {
        $asset_data = [];
        $existing_latitude = "";
        $existing_longitude = "";
        $unique_field = "";
        $category = $this->category;

        $import_field = importFieldName($category->import_field);
        if (is_array($import_field)) {
            foreach ($import_field as $key => $value) {
                if ($value == "unique_field") {
                    $unique_field = @$row[$key];
                }
                if ($value == "existing_latitude") {
                    $existing_latitude = @$row[$key];
                }
                if ($value == "existing_longitude") {
                    $existing_longitude = @$row[$key];
                }
                $field_value = @$row[$key];
                $asset_data[] = ['field_name' => $value, 'field_value' => $field_value];
            }
        }
        ++$this->rows;


        $existAssetRecord = ExistingAsset::where([['category_id', $category->id], ['unique_field', $unique_field]]);

        if ($existAssetRecord->count() > 0) {

            $this->duplicateRecord[] = array(
                'asset_data' => $asset_data,
                'existing_latitude' => $existing_latitude,
                'existing_longitude' => $existing_longitude,
                'category_id' => $category->id,
                'unique_field' => $unique_field
            );


            Session::put('duplicateRecord', $this->duplicateRecord);
        } else {
            return new ExistingAsset([
                'existing_asset_import_id'     => $this->existing_asset_import_id,
                'asset_data' => $asset_data,
                'existing_latitude' => $existing_latitude,
                'existing_longitude' => $existing_longitude,
                'category_id' => $category->id,
                'unique_field' => $unique_field
            ]);
        }
    }

    public function getRowCount(): int
    {

        return $this->rows;
    }

    public function getDuplicateRecord()
    {
        if ($this->duplicateRecord) {
            return true;
        } else {
            return false;
        }
    }
}
