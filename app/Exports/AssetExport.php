<?php

namespace App\Exports;

use App\Models\AssetImportData;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
class AssetExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public  $exportData,$fieldNameAndLabel;
    public function __construct($fieldNameAndLabel, $exportData)
    {
        $this->exportData = $exportData;
        $this->fieldNameAndLabel = $fieldNameAndLabel;
    }
    public function collection()
    {
        return $this->exportData;
    }

    public function headings(): array
    {
        $fieldLabel = array();
        $fieldLabel = $this->fieldNameAndLabel;
        $fieldLabel['createdAt'] ='createdAt';
        $fieldLabel['UpdatedAt'] ='UpdatedAt';
        return $fieldLabel;
    }

    public function map($asset): array
    {
        $asset_data = $asset->asset_data;
        $assetDataArray =array();
        if($asset_data)
        {
            foreach($this->fieldNameAndLabel as $key=>$val)
            {
                $assetDataArray[$key]='';
                if (array_key_exists($key, $asset_data)) 
                {
                $assetDataArray[$key] = $asset_data[$key];
                }
                
            }
           $assetDataArray['createdAt'] =$asset->created_at;
           $assetDataArray['UpdatedAt'] =$asset->updated_at;
          
        }
      if($assetDataArray)
        {
           return $assetDataArray;
        }
    }
}
