<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\{Scene, ExistingAsset};
use Grimzy\LaravelMysqlSpatial\Types\Point;

class ExistingAssetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'existingasset:sceneupdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scene id update in existing asset';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $assets = ExistingAsset::select('id', 'existing_latitude', 'existing_longitude')
            ->whereNull('scene_id')
            ->whereNotNull('existing_latitude')
            ->whereNotNull('existing_longitude')
            ->get();
        if ($assets->count()) {
            foreach ($assets as $asset) {
                $point = new Point($asset->existing_latitude, $asset->existing_longitude);
                $scene = new Scene();
                $sceneRow = $scene->contains('scene_area', $point)->first();
                if ($sceneRow) {
                    $asset->office_id = $sceneRow->office_id;
                    $asset->division_id = $sceneRow->division_id;
                    $asset->zone_id = $sceneRow->zone_id;
                    $asset->grid_id = $sceneRow->grid_id;
                    $asset->scene_id = $sceneRow->id;
                    $asset->save();
                }
            }
        }
    }
}
