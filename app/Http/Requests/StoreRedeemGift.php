<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRedeemGift extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gift_name'=> 'required|min:4',
            'short_description'=> 'required|min:6',
           // 'from_date' => 'required|date_format:d-m-Y|after_or_equal:'.date('d-m-Y'),
            'from_date' => 'required|date_format:d-m-Y',
            'to_date' => 'required|date_format:d-m-Y|after_or_equal:from_date',
            'min_gift_cost'=>'required|numeric',
            'min_redeem_point'=>'required|numeric',
            'redeem_cost'=>'required|numeric',
            'valid_no_of_day'=>'required|numeric',
            'total_coupon_qty'=>'required|numeric',
            'status'=>'required' 
        ];
    }
}
