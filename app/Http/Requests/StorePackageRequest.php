<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_name' => 'required|min:4',
           // 'package_image' => 'required|image|mimes:jpeg,png,jpg|max:300|dimensions:width=100,height=100',
           //'package_image' => 'required',
            'duration' => 'required|numeric',
            'cost' => 'required|numeric',
            'reward_point'=>'required|numeric',
            'cashback'=>'required|numeric',
            'from_date' => 'required|date_format:d-m-Y',
            'to_date' => 'required|date_format:d-m-Y|after_or_equal:from_date',
            'discount'=>'required',
            'status'=>'required'
        ];
    }
}
