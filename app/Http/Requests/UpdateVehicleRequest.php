<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       // echo $this->vehicle->id;
        return [
            'registration_number'=>'required|unique:App\Models\Vehicle,registration_number,'.$this->vehicle->id,
            'vehicle_type'=>'required',
            'fuel_type'=>'required',
            'status'=>'required'
         ];
    }
}
