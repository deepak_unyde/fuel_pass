<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:4|max:255',
            'heading' => 'required|min:4|max:255',
            'url'=>'required',
            'short_description'=>'required',
            'description'=>'required',
            'status'=>'required',
            //'from_date' => 'required|date_format:d-m-Y|after_or_equal:'.date('d-m-Y'),
            'from_date' => 'required|date_format:d-m-Y',
            'to_date' => 'required|date_format:d-m-Y|after_or_equal:from_date',
            #'promotion_image' =>'required|mimes:jpeg,png,jpg,gif'
           # 'promotion_image' =>'required|image|size:1024||dimensions:min_width=100,min_height=100,max_width=1000,max_height=1000'
        ];
    }
}
