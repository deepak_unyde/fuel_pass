<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|min:4|max:50|unique:App\Models\Company,company_name',
            'company_mobile' => 'nullable|digits:10|unique:App\Models\Company,company_mobile',
            'company_email' => 'nullable|email|unique:App\Models\Company,company_email',
            'password' => 'required|min:8|max:30|confirmed',
            'company_address'=>'required'
        ];
    }
}
