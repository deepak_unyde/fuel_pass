<?php

namespace App\Http\Requests;
    
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePetrolPumpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->request->all();
        return [
            'name' => 'required',
            'company_id' => 'required',
            'price_id' => 'required',
            'address' => 'required',
            'pincode' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'opening_time' => 'required|date_format:H:i',
            'closing_time' => 'required|date_format:H:i|after:time_start',
            'beacon_id' => 'required|unique:App\Models\PetrolPump,beacon_id',
            'code' => [
                'required',
                Rule::unique('petrol_pumps')->where(function ($query) use ($request) {
                    return $query->where('code', $request['code'])
                        ->where('company_id', $request['company_id']);
                })
            ],
            'status' => 'required',
        ];
    }
}
