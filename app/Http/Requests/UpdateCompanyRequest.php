<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|min:4|max:50|unique:App\Models\Company,company_name,'. $this->company->id,
            'company_mobile' => 'nullable|digits:10|unique:App\Models\Company,company_mobile,'. $this->company->id,
            'company_email' => 'nullable|email|unique:App\Models\Company,company_email,'. $this->company->id,
            'company_address'=>'required'
        ];

         
    }
}
