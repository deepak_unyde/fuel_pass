<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Company,PetrolPump,Price};
use Validator;
use Illuminate\Validation\Rule;
Use DB;

class PetrolPumpController extends Controller
{
    /**
     * Display a Petrol Pump listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $status = false;
        $message = __('api.no_record');
        $petrolPump = PetrolPump::select('petrol_pumps.id', 'company_id','beacon_id','price_id', 'code', 'name', 'address','pincode','latitude','longitude', 'opening_time', 'closing_time', 'status', 'petrol_pumps.created_at', DB::raw('ROUND(IFNULL(avg(rating),0),2) as rating'))
        ->with(['company' => function ($query) {
            $query->select('id', 'company_name')->with(['media']);
             }])
        ->with(['price' => function ($query) {
        $query->select('id','state_code','state_name','petrol_cost');
            }])
            ->leftJoin('ratings', [['ratings.petrol_pump_id','=','petrol_pumps.id']])
            ->where('status', 1)
            ->groupBy('petrol_pumps.id')
            ->orderBy('id','desc')
            ->get();
        if ($petrolPump->count()) {
              foreach ($petrolPump as $pump) {
                    $pump->company_logo = ($pump->company->getFirstMediaUrl('company_logo'))?$pump->company->getFirstMediaUrl('company_logo'):asset('img/default.png');

                    $pump->company_logo_thumb = ($pump->company->getFirstMediaUrl('company_logo','thumb'))?$pump->company->getFirstMediaUrl('company_logo','thumb'):asset('img/default.png');
                   }
            $status = true;                    
            $response['data']['petrolPump'] = $petrolPump;
            $message = __('api.list', ['title' => 'Petrol Pump']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }
    /**
     * Display a Fuel price listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function price(Request $request)
    {
        $status = false;
        $message = __('api.no_record');

        $price = Price::select('id', 'state_name', 'state_code', 'petrol_cost', 'diesel_cost','cng_cost', 'updated_at')
         ->orderBy('state_name','ASC')
        ->get();
        if ($price->count()) {
            $status = true;
            $response['data']['price'] = $price;
            $message = __('api.list', ['title' => 'Price']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

}
