<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\{Payment,User,Package,Profile,PetrolUser,Price,PetrolPump,Rating};
use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
class PaymentController extends Controller
{


    /***
     * Transaction History (Transaction, Saving, Rewards)
     * 
     */
    public function index(Request $request)
    {
        $status = false;
        $message = __('api.no_record');
        
        $transaction= PetrolUser::select('petrol_users.id','petrol_users.petrol_pump_id','transaction_no','fuel_cost','fuel_quantity','amount', 'reward_point','cashback','status','payment_no','petrol_users.created_at','ratings.rating')
        ->leftJoin('ratings',[['ratings.petrol_user_id','=','petrol_users.id'],['ratings.user_id','=','petrol_users.user_id']])
        ->with(['petrolPump' => function ($query) {
            $query->select('id','company_id', 'code', 'name','address','pincode')
            ->with(['company' => function ($query) {
                $query->select('id', 'company_name')->with('media');
            }]);
             }])
            ->where(function ($query)  use ($request) {
                $query->where('petrol_users.user_id',$request->user()->id);
                $s = $request->s;
                $status = $request->status;
                if ($status) {
                    $query->where('status',$status);
                }
                if ($s==1) {
                    $query->whereBetween(DB::raw("date(fu_petrol_users.created_at)"), [
                        date('Y-m-d', strtotime('-7 days')), date('Y-m-d')
                      ]);
                }
                if ($s==2) {
                    $query->where(DB::raw("date_format(fu_petrol_users.created_at,'%Y-%m')"), '=',
                        date('Y-m', strtotime('-1 month')));
                }
                
            })
             
            // ->where('user_id',$request->user()->id)
            ->orderBy('id','desc')
            ->get();
            if ($transaction->count()) {
                $status = true;
                $response['data']['transaction']=$transaction;
                $message = __('api.list', ['title' => 'Transaction']);
            }
            $response['status'] = $status;
            $response['message'] = $message;
            
            return $this->sendResponse($response);
 
    }

    /**
    * Grenerate Order/Transaction No for initialize purchase package
    **/


    public function initialize(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'package_id' => 'required|numeric',
            'amount' => 'required|numeric',
        ]);
        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }    
        $user_id = $request->user()->id;
        //$transaction_no = 'PACK-TRANS'.date('Ymd').time().rand(0000,9999);
        $transaction_no = generateNumber('PACKAGEORDERONLINE');
        $input['package_id']=$request->package_id;
        $input['amount']=$request->amount;
        $input['user_id']=$user_id;
        $input['transaction_no']=$transaction_no;
        $input['status']='pending';
        
        $payment = Payment::create($input);
        $data = [];
        if($payment) {
            $data['transaction_no'] = $payment->transaction_no;
            $data['id'] = $payment->id;
            $status = true;
            $message = __('api.purchase_package', ['title' => 'Order Id Generated successfully']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        $response['data'] = $data;
        return $this->sendResponse($response);
    }

    /**
     * Purchase Package payment process
     *
     */
    public function purchasePackage(Request $request)
    {
       $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            //'package_id' => 'required|numeric',
            //'amount' => 'required|numeric',
            'status'=>'required',
           // 'response'=>'required',
            //'transaction_no'=>'required',
            'payment_no'=>'required',
            //'pay_source'=>'required'
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }    
        $payment = Payment::find($request->id);
        $payment->payment_no=$request->payment_no;
        $payment->payment_datetime=date('Y-m-d H:i:s');
        $payment->response=$request->response;
        $payment->status=$request->status;
        
        $user_id = $request->user()->id;
        $data = [];
        if($payment->update()) {

            if($request->status=='success')
            {
                $package = Package::find($payment->package_id);
                $createUserPackage = array(
                    'package_id'=>$payment->package_id,
                    'user_id'=>$user_id,
                    'transaction_id'=>$payment->id,
                    'from_date'=>date('Y-m-d'),
                    'to_date'=>date('Y-m-d', strtotime("$package->duration days")),
                    'cost'=>$package->cost,
                    'discount'=>$package->discount,
                    'reward_point'=>$package->reward_point,
                    'discount_percentage'=>$package->discount_percentage,
                    'cashback'=>$package->cashback,
                    'status'=>1
                );
                
                $package->users()->attach($user_id,$createUserPackage);
                $insert_id = $package->users()->select('package_user.id')->where("user_id", "=",$user_id)->withPivot("id")->orderBy('id', 'desc')->first()->pivot->id;
               
               $user =  User::find($user_id);
               $user->profile()->update(['package_id'=>$payment->package_id,'package_user_id'=>$insert_id]);

                $status = true;
                $message = __('api.purchase_package', ['title' => 'Package Payment successfully completed']);
             }
             else
             {
                $status = false;
                $message = __('api.purchase_package', ['title' => 'Package Payment failed.']);
             }
        }
        $response['message'] = $message;
        $response['status'] = $status;
        
        return $this->sendResponse($response);
    }


     /**
     * Purchase Package from wallet payment process
     *
     */
    public function purchasePackageFromWallet(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'package_id' => 'required|numeric',
            'amount' => 'required|numeric',
            //'status'=>'required',
           //'response'=>'required',
        ]);
        if($validator->fails()) 
        {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }    
        
        $user_id = $request->user()->id;
        $user =  User::find($user_id)
        ->profile
        ->select('package_user_id','package_user.to_date','profiles.wallet_amount')
        ->leftJoin('package_user',[['package_user.id','=','package_user_id'],['package_user.user_id','=','profiles.user_id']])
        ->where(['profiles.user_id'=>$user_id])
        ->get();
        
        $wallet_amount = ($user[0]->wallet_amount)?$user[0]->wallet_amount:0;
        $package_user_id = ($user[0]->package_user_id)?$user[0]->package_user_id:0;
        $to_date = ($user[0]->to_date)?$user[0]->to_date:date('Y-m-d',strtotime('-1 days'));
        
        if($wallet_amount <  $request->amount) 
           {
            $response['status'] = false;
            $response['message'] =  __('api.purchase_package', ['title' =>'Insufficient balance please Rechange your wallet']);
            return $this->sendResponse($response);
           }    
        if( strtotime($to_date) > strtotime(date('Y-m-d')))
            {
                $response['message'] =  __('api.purchase_package', ['title' =>"Your plan already active please try after $to_date "]);
                $response['status'] = false;
                return $this->sendResponse($response);
            }   

        $transaction_no = "TANSAC".date('Ymd').time(); //generateNumber('PACKAGEORDERWALLET');
        $payment_no = "WALLET".date('Ymd').time();//generateNumber('PACKAGEPAYMENTWALLET');
         
        $input['package_id']=$request->package_id;
        $input['amount']=$request->amount;
        $input['user_id']=$user_id;
        $input['transaction_no']=$transaction_no;
        $input['status']='pending';
        $input['pay_source']='wallet';
        $input['payment_no']=$payment_no;
        $input['payment_datetime'] =date('Y-m-d H:i:s');
        $input['response'] =$request->response;
        $payment = Payment::create($input);
        $data = [];
        if($payment) 
        {
            $data['transaction_no'] = $payment->transaction_no;
            $data['payment_no'] = $payment->payment_no;
            $data['id'] = $payment->id;
            $data['status']='pending' ;

            
                $package = Package::find($request->package_id);
                $createUserPackage = array(
                    'package_id'=>$request->package_id,
                    'user_id'=>$user_id,
                    'transaction_id'=>$payment->id,
                    'from_date'=>date('Y-m-d'),
                    'to_date'=>date('Y-m-d', strtotime("$package->duration days")),
                    'cost'=>$package->cost,
                    'discount'=>$package->discount,
                    'reward_point'=>$package->reward_point,
                    'discount_percentage'=>$package->discount_percentage,
                    'cashback'=>$package->cashback,
                    'status'=>1
                );
                
                $package->users()->attach($user_id,$createUserPackage);
                $insert_id = $package->users()->select('package_user.id')
                ->where("user_id", "=",$user_id)
                ->whereDate('package_user.created_at', '=', Carbon::today()->toDateString())
                ->withPivot("id")->orderBy('id', 'desc')->first()->pivot->id;
                
                if($insert_id)
                {
                    $payment->status = 'success';
                    $payment->save(); 
                    $new_wallet_amount = $wallet_amount - $request->amount;
                    $user =  User::find($user_id);
                    $user->profile()->update(['package_id'=>$request->package_id,'package_user_id'=>$insert_id,'wallet_amount'=>$new_wallet_amount]);

                    $status = true;
                    $message = __('api.purchase_package', ['title' => 'Package Payment form wallet successfully completed']);
                }
                else
                {
                   $payment->update(['status'=>'failed']); 
                   $status = false;
                   $message = __('api.purchase_package', ['title' => 'Package Payment failed.']);
                }
        }
        $response['message'] = $message;
        $response['status'] = $status;
        $response['data'] = $data;
        return $this->sendResponse($response);
    }
    /*
    User Purchase fuel from Petrol Pump
    */
    public function purchasePetrol(Request $request)
    {
        $data =[];
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
             'package_id' => 'required|numeric',
             'petrol_pump_id'=>'required|numeric',
             'amount' => 'required|numeric|min:10',
             //'fuel_quantity'=>'required|numeric',
             //'fuel_type'=>'required',
             //'status'=>'required',
              //'response'=>'required',
            //'transaction_no'=>'required',
            //'payment_no'=>'required'
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }    
        
        $user_id = $request->user()->id;
        $user =  User::find($user_id)
        ->profile
        ->select('package_user_id','package_user.reward_point','package_user.cashback','package_user.to_date','profiles.wallet_amount','profiles.cashback  as user_cashback','profiles.reward_point  as user_reward_point')
        ->leftJoin('package_user',[['package_user.id','=','package_user_id'],['package_user.user_id','=','profiles.user_id']])
        ->where(['profiles.user_id'=>$user_id])
        ->get();
         
            $reward_point = ($user[0]->reward_point)?$user[0]->reward_point:0;
            $cashback = ($user[0]->cashback)?$user[0]->cashback:0;
            $user_cashback = ($user[0]->user_cashback)?$user[0]->user_cashback:0;
            $user_reward_point = ($user[0]->user_reward_point)?$user[0]->user_reward_point:0;
            $wallet_amount = ($user[0]->wallet_amount)?$user[0]->wallet_amount:0;
            $package_user_id = ($user[0]->package_user_id)?$user[0]->package_user_id:0;
            $to_date = ($user[0]->to_date)?$user[0]->to_date:date('Y-m-d',strtotime('-1 days'));
            
            //if( $package_user_id < 1){
            if( strtotime($to_date) < strtotime(date('Y-m-d'))){
                $response['message'] =  __('api.petrol_pump_payment', ['title' =>'Sorry any plan not active yet. Please purchase plan first']);
                $response['status'] = false;
                return $this->sendResponse($response);
            }   
          
        $input['package_id']=$request->package_id;
        $input['petrol_pump_id']=$request->petrol_pump_id;
        $input['amount']=$request->amount;
        $input['user_id']=$user_id;
        
        $payment_no = generateNumber('FUELPAYMENTWALLET');
        $input['payment_no']=$payment_no;
        $input['transaction_no']=$payment_no;
        $input['status']='success';
         
        $petrolPump =  PetrolPump::find($request->petrol_pump_id)
        ->with(
            ['price'=> function($query){
                $query->select('id','state_code','state_name','petrol_cost');
            }]
        )->get();

        $fuel_cost = ($petrolPump[0]->price->petrol_cost)?$petrolPump[0]->price->petrol_cost:0;
        $input['reward_point']=ROUND(($reward_point*(($request->amount/$fuel_cost))),2);
        $input['cashback']=ROUND(($cashback*(($request->amount/$fuel_cost))),2);
        $input['fuel_cost']=$fuel_cost;
        $input['fuel_quantity']=ROUND(($request->amount/$fuel_cost) ,2);
        $data = [];
         
        if($wallet_amount >= $request->amount)
        {
        
            $petrolUser = PetrolUser::create($input);
       
            if($petrolUser) 
            {
                $data[] = $petrolUser->only(['id', 'payment_no','amount','status','created_at']);
                $msg =  "Your Payment ".$request->amount." Rs. ".$request->status;
                if($input['status']=='success')
                {
                     /* Update User Wallet*/
                     $new_wallet_amount = ($wallet_amount - $request->amount)+$input['cashback'];
                     $user_cashback = $user_cashback+$input['cashback'];
                     $user_reward_point = $user_reward_point+$input['reward_point'];
                     User::find($user_id)->profile()->update(['wallet_amount'=>$new_wallet_amount,'cashback'=>$user_cashback,'reward_point'=>$user_reward_point]);

                    $status = true;
                    $message = __('api.petrol_pump_payment', ['title' =>$msg]);
                    $response['data']['transaction'] = $data;
                }
                else
                {
                    $status = false;
                    $message = __('api.petrol_pump_payment', ['title' =>$msg]);
                }
            }
            else
            {
                $status = false;
                $message = __('api.petrol_pump_payment', ['title' =>'Payment failed due to some technical issue please contact to Administration']);
            }
        }
        else
        {
            $status = false;
            $message = __('api.petrol_pump_payment', ['title' =>'Insufficient balance please Rechange your wallet']);
        }
       # $response['data'] = $data;
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }
/*** Service Payment process */
public function servicePayment(Request $request)
    {
         
        $status = false;
        $message = __('api.try_again');
        //$now = Carbon::today()->toDateTimeString();
        $now = Carbon::now();
        $validator = Validator::make($request->all(), [
             'id' => 'required|numeric',
             'amount' => 'required|numeric|min:10',
             ]);
        if ($validator->fails()) 
        {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }    
        $user_id = $request->user()->id;
        $serviceUser = DB::table('service_user')
       ->select('*')
       ->where('id',$request->id)->first();
       if($serviceUser)
       {
            DB::table('service_user')
            ->where('id',$request->id)
            ->update(['payment_no'=>$request->payment_no,'payment_date'=>$now,'payment_amount'=>$request->amount,'payment_status'=>$request->payment_status,'updated_at'=>$now]);
             

            DB::table('service_transaction_user')
            ->insert(['request_payload' =>json_encode($request->all()),'created_at'=>$now,'updated_at'=>$now, 'user_id'=>$user_id, 'service_id'=>$serviceUser->service_id,'amount'=>$request->amount,'status'=>$request->payment_status,'payment_no'=>$request->payment_no,'order_no'=>$serviceUser->order_no,'response'=>$request->response,'service_user_id'=>$request->id,'payment_datetime'=>$now]);

            $status = true;
            $response['data'] = ['order_no'=>$serviceUser->order_no,'payment_no'=>$request->payment_no,'payment_status'=>$request->payment_status];
            $message = __('api.petrol_pump_payment', ['title' =>'Your Service Payment Successfully Completed']);
            
       }
       else
        {
            $status = false;
            $message = __('api.petrol_pump_payment', ['title' =>'Your Service Payment Failed.Please Try Again']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    
}
