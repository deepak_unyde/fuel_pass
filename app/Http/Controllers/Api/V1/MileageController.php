<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mileage;
use Validator;
use Illuminate\Validation\Rule;

class MileageController extends Controller
{
    /**
     * Display a Mileage listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $status = false;
        $message = __('api.no_record');

        $mileages = Mileage::select('id','user_id','meter_reading','created_at')
            ->where('user_id', $request->user()->id)
            ->orderBy('id','desc')
            ->get();
        if ($mileages->count()) {
            $status = true;
            $response['data']['mileages'] = $mileages;
            $message = __('api.list', ['title' => 'Mileage']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    /**
     * Store mileage a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'meter_reading' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $user_id = $request->user()->id;
        $input['user_id'] = $user_id;
         $input['meter_reading'] = $request->meter_reading;
        $mileage = Mileage::create($input);
        if ($mileage) {
            $status = true;
            $response['data']['mileage'] = $mileage->only('id','user_id','meter_reading','created_at');
            $message = __('api.insert', ['title' => 'Mileage']);
        }

        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

     
}
