<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\{RedeemGift,User};
use Illuminate\Http\Request;
use Validator;

class RedeemGiftController extends Controller
{
    /**
     * Display a Redeem Offer listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* Redeem Offer List ***/
    public function index(Request $request)
    {
        $status = false;
        $message = __('api.no_record');
         
        $date = Date('Y-m-d');
        $redeemGift= RedeemGift::with('media')
        ->select('id','gift_name','short_description','from_date','to_date','min_gift_cost','min_redeem_point','redeem_cost','valid_no_of_day')
            ->where('status',1)
            ->where('balance_coupon_qty','>',0)
            ->Where('from_date', '<=',$date)
            ->Where('to_date', '>=',$date)
            ->orderBy('min_redeem_point')
            ->active()->get();
            if ($redeemGift) {

                foreach ($redeemGift as $gift) {
                    $gift->gift_image = ($gift->getFirstMediaUrl('gift_image'))?$gift->getFirstMediaUrl('gift_image'):asset('img/default.png');

                    $gift->gift_image_thumb = ($gift->getFirstMediaUrl('gift_image','thumb'))?$gift->getFirstMediaUrl('gift_image','thumb'):asset('img/default.png');
                   }
           
                $status = true;
                $response['data']['redeemGift']=$redeemGift;
                $message = __('api.list', ['title' => 'Redeem Offer']);
            }
            $response['status'] = $status;
            $response['message'] = $message;
             
            return $this->sendResponse($response);
 
    }

    /* Redeem Point by Users ***/
    public function redeemPoint(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'redeem_gift_id'=>'required|numeric|gt:0',
            'redeem_point'=>'required|numeric|gt:0',
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $user_id = $request->user()->id;
        $redeemGift = RedeemGift::find($request->redeem_gift_id);
        if($redeemGift->balance_coupon_qty <= 0)
        {
            $status = false;
            $message = __('api.dynamic_msg', ['title' => 'This coupon is out of stock please choose another coupon.']);
        }
        else if($redeemGift->min_redeem_point > $request->redeem_point) 
        {
            $status = false;
            $message = __('api.dynamic_msg', ['title' => "Need minimum redeem point $redeemGift->min_redeem_point"]);
        }
        else
        {
           $profile =  User::find($user_id)->profile;
           if($profile->reward_point > 0 && $profile->reward_point >= $request->redeem_point)
           {
                $profile->updateOrCreate(['user_id'=>$user_id],['reward_point'=>$profile->reward_point-$request->redeem_point]);

                $redeemGiftArray['use_coupon_qty'] =  $redeemGift->use_coupon_qty+1;
                $redeemGiftArray['balance_coupon_qty'] =  $redeemGift->balance_coupon_qty-1;
                $redeemGift->update($redeemGiftArray);

                $redeemUser = array(
                    'redeem_gift_id'=>$request->redeem_gift_id,
                    'user_id'=>$user_id,
                    'redeem_code'=>substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,10).time(),
                    'redeem_point'=>$request->redeem_point,
                    'status'=>0
                );
                $redeemGift->users()->attach($user_id,$redeemUser);
                $status = true;
                $message = __('api.dynamic_msg', ['title' => 'Your reward points redeem successfully completed.']);
          }
           else
           {
                $status = false;   
                $message = __('api.dynamic_msg', ['title' => 'Reward point is less than redeem points.']);   
           }

        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    /* User Redeem List ***/
    public function userRedeemList(Request $request)
    {
        $status = false;
        $message = __('api.no_record');
        
        $user_id = $request->user()->id;
        $redeemGift=   User::
       with(['redeem' => function ($query) {
            $query->with(['media']);
             }])
        ->where('id', $user_id)
        ->orderBy('id', 'desc')
        ->get();
            if ($redeemGift) {

                $status = true;
                $response['data']['redeemList']=$redeemGift;
                $message = __('api.list', ['title' => 'User Redeem']);
            }
            $response['status'] = $status;
            $response['message'] = $message;
             
            return $this->sendResponse($response);
    }
}
