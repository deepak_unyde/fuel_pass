<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\{Package,Promotion};
use Illuminate\Http\Request;
use Validator;

class PackageController extends Controller
{
    /**
     * Display a package listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = false;
        $message = __('api.no_record');

        $to_date = Date('Y-m-d');
        $data = [];
        $packageList= Package::select('id', 'package_name', 'package_description', 'from_date', 'to_date', 'duration', 'cost', 'discount', 'discount_percentage', 'reward_point', 'cashback', 'status')
            ->with(['media']) 
            ->where('to_date', '>=',$to_date)
            ->orderBy('package_name')
            ->active()->get();
            if ($packageList) {
                    foreach ($packageList as $package) {
                        $package->package_image = ($package->getFirstMediaUrl('package_image'))?$package->getFirstMediaUrl('package_image'):asset('img/default.png');
                       }
                $status = true;
                $data['packageList']=$packageList;
                #$response['data']['packageList']=$packageList;
                $message = __('api.list', ['title' => 'Package']);
            }
            $response['status'] = $status;
            $response['message'] = $message;
            $response['data'] = $data;
            return $this->sendResponse($response);
 
    }

    /**
     * Our Feature List with icon & function name.
     */
    function ourFeature(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $data = [];
         
        $features = array(
            ['title'=>'Pay','short_description'=>'Pay the petrol pump','icon_path'=>asset('img/payment.png'),'function_name'=>'fuel-pay'],
            ['title'=>'Transactions','short_description'=>'All your past transactions','icon_path'=>asset('img/transaction.png'),'function_name'=>'transaction?s=0'],
            ['title'=>'Savings','short_description'=>'Your monthly & weekly savings','icon_path'=>asset('img/saving.png'),'function_name'=>'transaction?s=0'],
            ['title'=>'Wallet','short_description'=>'Check balance & add money','icon_path'=>asset('img/wallet.png'),'function_name'=>'wallet'],
            ['title'=>'Redeem','short_description'=>'Redeem Points','icon_path'=>asset('img/coupon.png'),'function_name'=>'redeem-offer'],
        );
        
        if ($features) {
            $data['features'] = $features;
            $status = true;
            $message = __('api.list', ['title' => 'Our features']);
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $data;
        return $this->sendResponse($response);
    }

    /************** Promotion List********/
    public function promotion(Request $request)
    {
        $status = false;
        $message = __('api.no_record');

        $to_date = Date('Y-m-d');
        $data = [];
        $promotionList= Promotion::select(
        'id', 
        'title', 
        'heading',
        'url',
        'from_date',
        'to_date',
        'short_description',
        'description',
        'status')
            ->with(['media']) 
            ->where('to_date', '>=',$to_date)
            ->orderBy('id','desc')
            ->active()->get();
            if ($promotionList) {
                    foreach ($promotionList as $promotion) {
                        $promotion->promotion_image = ($promotion->getFirstMediaUrl('promotion_image'))?$promotion->getFirstMediaUrl('promotion_image'):asset('img/default.png');
                      
                        $promotion->promotion_image_thumb = ($promotion->getFirstMediaUrl('promotion_image','thumb'))?$promotion->getFirstMediaUrl('promotion_image','thumb'):asset('img/default.png');
                       }
                $status = true;
                $data['promotionList']=$promotionList;
                $message = __('api.list', ['title' => 'Promotion']);
            }
            $response['status'] = $status;
            $response['message'] = $message;
            $response['data'] = $data;
            return $this->sendResponse($response);
 
    }
}
