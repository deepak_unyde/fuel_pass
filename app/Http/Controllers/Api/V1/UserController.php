<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\{User,Profile,Rating,Address,Otp};
use Illuminate\Support\Str;
use Auth,Hash, DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SignupMail;
 
class UserController extends Controller
{
    /**
     * User Profile 
     */
    function updateProfile(Request $request)
    {
        $status = false;
        $message = __('api.try_again');    
        $user_id = $request->user()->id;
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|digits:10|unique:App\Models\User,mobile,'. $user_id,
            'name' => 'required',
            'email' => 'required|email|unique:App\Models\User,email,'. $user_id,
            'package_id' => 'required|numeric',
            'term_accepted'=>'required|numeric|gt:0'
        ]);
        
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
       
        $user = User::find($user_id);
        $user['mobile'] = $request->mobile;
        $user['email'] = $request->email;
        $user['name'] = $request->name;
      
        if ( $user->update()) {
            $insertProfile['user_id']=$user_id;
            $insertProfile['package_id']=$request->package_id;
            $insertProfile['term_accepted']=$request->term_accepted;
            $user->profile()->updateOrCreate(['user_id'=>$user_id],$insertProfile);
            $response['status'] = true;
            $message = __('api.update',['title'=>'user profile']);
        } else {
            $status = false;
            $message = __('api.try_again');    
        }
        $response['message'] = $message;
        return $this->sendResponse($response);
    }
    /**
     * User login using mobile
     */
    function login(Request $request)
    {
        $response = [];
        $message = null;
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|digits:10|numeric',
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        
        $user = User::with(['roles'])
       ->where('mobile', $request->post('mobile'))->first();
        $otp = Otp::generateNumericOTP();
        if ($user) 
        {
            if($user->status==1 && $user->roles[0]->id==2)
            {
                $user_id = $user->id;
                Otp::generateOtp($user_id, $request->mobile, $otp);
                if ($otp) {
                // sendSMS($request->mobile, $otp, '');
                }
                $response['status'] = true;    
                $message = __('api.login_success');
            }
            else
            {
            $response['status'] = false;    
            $message = __('api.login_fail');    
            }
        } 
        else 
        {
            Otp::generateOtp(0, $request->mobile, $otp);
            if ($otp) {
               // sendSMS($request->mobile, $otp, '');
            }
            $response['status'] = true;
            $message = __('api.login_success');
        }
        $response['message'] = $message;
        return $this->sendResponse($response);
    }


    /**
     * Verify OTP
     */
    function verify(Request $request)
    {
        $response = [];
        $message = null;
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|digits:10',
            'otp' => 'required|digits:4',
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $otp = $request->post('otp');
        $user = User::select('users.id', 'name', 'users.mobile')->where('users.mobile', $request->post('mobile'))
            ->whereHas('roles', function ($query) {
                $query->where('role_id', 2);
            })
            ->with('profile')
            ->where('users.status', 1)
            ->first();
           
            if ($user) {
            if (Otp::verify($request->mobile, $otp) || $otp == 1234) {
           // if ($otp == 1234) {
                # store user history for tracking
                User::loginLog($request, $user->id);
                # creating user token
                $user->tokens()->delete();
                $token = $user->createToken('mytoken')->plainTextToken;
                if($user->profile->package_id==0)
                    {
                        $profile_status = false;
                    }
                    else
                    {
                        $profile_status = true;
                    }
                $response['data'] = ['user' => $user->only(['id','name', 'mobile']), 'token' => $token,'profile_status'=>$profile_status];
                $message = __('api.verify_success');
            } else {
                $response['status'] = false;
                $message = __('api.verify_fail');
            }
        } else {
            if (Otp::verify($request->mobile, $otp) || $otp == 1234)  
            //if ($otp == 1234) 
             {
                # create new user
                $input['mobile'] = $request->post('mobile');
                $input['status'] = 1;
                $input['company_id'] = 0;
                $newuser = User::create($input);
                # create profile
                $newuser->profile()->updateOrCreate(['user_id'=>$newuser->id],['user_id'=>$newuser->id]);

                if($newuser)
                {
                    # Assign user role
                    $newuser->assignRole(2);
                }
                # store user history for tracking
                User::loginLog($request, $newuser->id);
                # creating user token
                $token = $newuser->createToken('mytoken')->plainTextToken;
                $response['data'] =  ['user' => $newuser->only(['id','name', 'mobile']),'token' => $token,'profile_status'=>false];
                $message = __('api.verify_success');
             } 
            else {
                $response['status'] = false;
                $message = __('api.verify_fail');
            }
        }
        $response['message'] = $message;
        return $this->sendResponse($response);
    }

    /**
     * Get User status
     */
    function user(Request $request)
    {
       $data =[];

       $userStatus = $request->user();
       $data =  $userStatus->only(['status']);
       $user_id = $request->user()->id;
       $user =  User::find($user_id) 
        ->profile
        ->select('profiles.package_id','package_user_id','package_user.to_date')
        ->leftJoin('package_user',[['package_user.id','=','package_user_id'],['package_user.user_id','=','profiles.user_id']])
        ->where(['profiles.user_id'=>$user_id])
        ->first();
        
        if($user->package_id==0)
        {
            $data['profile_status'] = false;
            $data['package_status'] = 'User not purchase any package yet';
        }
        elseif($user->package_id>0 && strtotime($user->to_date) < strtotime(date('Y-m-d')))
        {
            $data['profile_status'] = false;
            $data['package_status'] = "Package Expired form ".$user->to_date;
        }
        else
        {
            $data['profile_status'] = true;
            $data['package_status'] = "Package Active";
        }
        $response['data'] = ['user' => $data];
        
       // $user = $request->user();
        //$response['data'] = ['user' => $user->only(['status'])];
        return $this->sendResponse($response);
    }


    /**
     * After successfully login
     */
    function home()
    {
        $response['data'] = ['home' => 'Welcome Dashboard'];
        return $this->sendResponse($response);
    }

    /**
     * User logout 
     */
    function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        $response['message'] = __('api.logout');
        $response['data'] = [];

        return $this->sendResponse($response);
    }

    /**
     * User profile
     */
    function profile(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        
        $user = User::select('users.id', 'users.name','users.email', 'users.mobile',  'packages.package_name', 'profiles.wallet_amount','profiles.reward_point','profiles.cashback','package_user.from_date', 'package_user.to_date', 'package_user.cost', DB::raw("CASE WHEN fu_package_user.to_date >= Current_date() THEN 'Active' Else 'Expired' END as planStatus, IFNULL(DATEDIFF(fu_package_user.to_date,current_date()),0) as remainingDays "))
        ->leftjoin('profiles','profiles.user_id', '=', 'users.id')
        ->leftJoin('package_user', [['package_user.id', '=', 'profiles.package_user_id'],['package_user.package_id', '=', 'profiles.package_id'],['package_user.user_id', '=', 'profiles.user_id']])
        ->leftJoin('packages', 'packages.id', '=', 'profiles.package_id')
        ->where('users.id', $request->user()->id)->get();
        $response['data']['user'] = $user;
        if ($user) {
            $status = true;
            $message = __('api.detail', ['title' => 'User']);
        }
        $response['status'] = $status;
        $response['message'] = $message;
        
        return $this->sendResponse($response);
    }

    /** Add/Update Petrol Pump Rating from User  */

    function rating(Request $request)
    {
        $status = false;
        $message = __('api.try_again');    
        $validator = Validator::make($request->all(), [
            'petrol_pump_id' => 'required|numeric',
            'transaction_id' => 'required|numeric',
            'rating' => 'required'
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $user_id = $request->user()->id;
        /*$rating = Rating::where('user_id', $user_id)
        ->where('petrol_pump_id', $request->petrol_pump_id)
        ->first();*/

        $updateOrCreate = Rating::updateOrCreate(['user_id'=>$user_id,'petrol_pump_id'=>$request->petrol_pump_id,'petrol_user_id'=>$request->transaction_id],['user_id'=>$user_id,'petrol_pump_id'=>$request->petrol_pump_id,'petrol_user_id'=>$request->transaction_id,'rating'=>$request->rating]);
        if($updateOrCreate)
        {
            $message = __('api.dynamic_msg', ['title' => 'Thank you for rating.']);
            $status = true;
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $this->sendResponse($response);
       
    }
    /** Add Delete List for User Address Start */
    function addAddress(Request $request)
    {
        $status = false;
        $message = __('api.try_again');    
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'pincode' => 'required|numeric',
             
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
       $user_id = $request->user()->id;
       $addressArray = array(
        'user_id'=>$user_id,
        'latitude'=>$request->latitude,
        'longitude'=>$request->longitude,
        'address'=>$request->address,
        'city'=>$request->city,
        'state'=>$request->state,
        'country'=>$request->country,
        'pincode'=>$request->pincode
       );
       $address = Address::create($addressArray);
       if ($address) {
        $status = true;
        $message = __('api.insert', ['title' => 'User Address']);
         }
    $response['status'] = $status;
    $response['message'] = $message;
    return $this->sendResponse($response);
        
    }

    public function getAddress(Request $request)
    {
        $status = false;
        $message = __('api.no_record');
        $address = Address::
            where('user_id', $request->user()->id)
            ->orderBy('id','desc')
            ->get();
        if ($address->count()) {
            $status = true;
            $response['data']['address'] = $address;
            $message = __('api.list', ['title' => 'User address']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    public function deleteAddress(Request $request)
    {

        $status = false;
        $message = __('api.no_record');
        $user_id =  $request->user()->id;
        if (Address::find($request->id)->delete()) 
        {
            $status = true;
            $message = __('api.delete', ['title' => 'Address']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }
    /** Add Delete List for User Address End */

    /** Service App Related User Login & Update profile */

    function sendOtp(Request $request)
    {
        $response = [];
        $message = null;
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|digits:10|numeric',
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $user = User::with(['roles'])
       ->where('mobile', $request->post('mobile'))->first();
        $otp = Otp::generateNumericOTP();
        if ($otp) {
            // sendSMS($request->mobile, $otp, '');
         }
        $user =  User::updateOrCreate(['mobile'=>$request->mobile],['mobile'=>$request->mobile,'company_id'=>0]); 
        $user->profile()->updateOrCreate(['user_id'=>$user->id],['user_id'=>$user->id]);
        $user_id = 0;
        $user_id = $user->id;
        $user->syncRoles(2);
        Otp::generateOtp($user_id, $request->mobile, $otp);
        $response['status'] = true;    
        $message = __('api.login_success');

        $response['message'] = $message;
        return $this->sendResponse($response);
    }


    /**
     * Verify OTP
     */
    function otpVerify(Request $request)
    {
        $response = [];
        $message = null;
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|digits:10',
            'otp' => 'required|digits:4',
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $otp = $request->post('otp');
        if (Otp::verify($request->mobile, $otp) || $otp == 1234) 
        {
            $user =  User::updateOrCreate(['mobile'=>$request->mobile],['mobile'=>$request->mobile,'company_id'=>0]);
            $user->profile()->updateOrCreate(['user_id'=>$user->id],['user_id'=>$user->id]);
            $user_id = $user->id;
            $user->syncRoles(2);
                User::loginLog($request, $user->id);
                $user->tokens()->delete();
                $token = $user->createToken('mytoken')->plainTextToken;
                $response['data'] = ['user' => $user->only(['id','name', 'mobile']), 'token' => $token];
                $message = __('api.verify_success');
        } 
        else 
        {
                $response['status'] = false;
                $message = __('api.verify_fail');
        }
        $response['message'] = $message;
        return $this->sendResponse($response);
    }

    

    /**
     * User Profile 
     */
    function editProfile(Request $request)
    {
        $status = false;
        $message = __('api.try_again');    
        $user_id = $request->user()->id;
        $validator = Validator::make($request->all(), [
            //'mobile' => 'required|digits:10|unique:App\Models\User,mobile,'. $user_id,
            'name' => 'required',
            'email' => 'required|email|unique:App\Models\User,email,'. $user_id 
        ]);
        
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
       
        $user = User::find($user_id);
        //$user['mobile'] = $request->mobile;
        $user['email'] = $request->email;
        $user['name'] = $request->name;
      
        if ( $user->update()) {
            $insertProfile['user_id']=$user_id;
            $insertProfile['package_id']=0;
            $insertProfile['term_accepted']=1;
            $user->profile()->updateOrCreate(['user_id'=>$user_id],$insertProfile);
            $response['status'] = true;
            $message = __('api.update',['title'=>'user profile']);
        } else {
            $status = false;
            $message = __('api.try_again');    
        }
        $response['message'] = $message;
        return $this->sendResponse($response);
    }


    /** Service related End process */
}
