<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vehicle;
use Validator;
use Illuminate\Validation\Rule;
use DB;
use Carbon\Carbon;
class VehicleController extends Controller
{
    /**
     * Display a Vehicle listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $status = false;
        $message = __('api.no_record');
         
        $vehicles =  Vehicle::select(
            'id',
            'user_id', 
            'registration_number',
            'vehicle_type',
            'fuel_type',
            'status',
            'created_at')
            ->where('user_id', $request->user()->id)
            ->orderBy('id','desc')
            ->get();
        if ($vehicles->count()) {
            $status = true;
            foreach ($vehicles as $vehicle) {
                $vehicle->icon_path = vehicleIcon($vehicle->vehicle_type);
               }
            $response['data']['vehicles'] = $vehicles;
            $message = __('api.list', ['title' => 'Vehicle']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    /**
     * Store a newly vehicle created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'registration_number'=>'required|unique:vehicles,registration_number',
            'vehicle_type'=>'required',
            'fuel_type'=>'required',
            'term_accepted'=>'required',
             
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $user_id = $request->user()->id;
        $input['user_id'] = $user_id;
        $input['registration_number'] = $request->registration_number;
        $input['vehicle_type'] = $request->vehicle_type;
        $input['fuel_type'] = $request->fuel_type;
        $input['term_accepted'] = $request->term_accepted;
        $input['status'] =1;
        $vehicle = Vehicle::create($input);
        if ($vehicle) {
            $status = true;
            $response['data']['vehicle'] = $vehicle->only('id',
            'user_id', 
            'registration_number',
            'vehicle_type',
            'fuel_type',
            'status',
            'created_at');
            $message = __('api.insert', ['title' => 'Vehicle']);
        }

        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

     /*
     Get Vehicle Type listing
     */
    public function vehicleType()
    {
        $status = false;
        $message = __('api.no_record');
        $response['data']=[];
        $vehicles = vehicleTypeList();
        if ($vehicles) {
            $status = true;
            $response['data']['vehicleType'] = $vehicles;
            $message = __('api.list', ['title' => 'Vehicle Type']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }
    /*
     Get Fuel Type listing
    */
    public function fuelType()
    {
        $status = false;
        $message = __('api.no_record');
        $response['data']=[];
        $fuelType = fuelType();
        if ($fuelType) {
            $status = true;
            $response['data']['fuelType'] = $fuelType;
            $message = __('api.list', ['title' => 'Fuel Type']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    /*******************Anuj & Vivek Sir Need Api ************/
    public function metro(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'packet_data'=>'required'
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $now = Carbon::now();
        $metro = 
        DB::insert('insert into fu_metro_packets (packet_data,created_at,updated_at) values (?,?,?)', [$request->packet_data,$now,$now]);
 ;
        if ($metro) {
            $status = true;
            $message = __('api.insert', ['title' => 'Metro Data']);
        }

        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }
    public function logger(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
             
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $now = Carbon::now();
        $constellation_type = $request->constellation_type;
        $constellation_count = 0;
        if(!empty($constellation_type))
        {
          $count = array_unique(explode(',',$constellation_type)) ;
          $constellation_count = count($count)-1;
        }
        $logger = 
        DB::insert('insert into fu_base_logger (lati, longi,accuracy, vehicle_id, azimuth, elevation, cnhz, constellation_type,constellation_count,device_name, created_at, updated_at) values (?,?,?,?,?,?,?,?,?,?,?,?)', [$request->lati,$request->longi,$request->accuracy,$request->vehicle_id,$request->azimuth,$request->elevation,$request->cnhz,$request->constellation_type,$constellation_count,$request->device_name,$now,$now]);
        if ($logger) {
            $status = true;
            $message = __('api.insert', ['title' => ' Data ']);
        }

        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }
}
