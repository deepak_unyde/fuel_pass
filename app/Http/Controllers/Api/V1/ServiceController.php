<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Service,User,ServiceDetail};
use Validator,DB;
use Illuminate\Validation\Rule;

class ServiceController extends Controller
{
    /**
     * Display a wallet listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = false;
        $message = __('api.no_record');
        $date = date('Y-m-d',strtotime('1 days'));
        $services =  Service::with('media')
        ->where('status', 1)
            ->get();
        if ($services->count()) 
        {
            foreach($services as $service)
            {
               # $service->service_details =  ServiceDetail::select('id', 'service_id', 'slot_start_time', 'slot_end_time', 'slot_limit')->where([['status', 1],['service_id',$service->id]])->get();

                $service->service_details = DB::select("select fu_service_details.id, fu_service_details.service_id, slot_start_time, slot_end_time, slot_limit, (slot_limit-count(fu_service_user.slot_id)) as available_slot from fu_service_details left join fu_service_user on service_date='$date' and fu_service_user.slot_id=fu_service_details.id where status=1 and fu_service_details.service_id=$service->id group by fu_service_details.id order by fu_service_details.id ASC");
            }
            $status = true;
            $response['data']['services'] = $services;
            $message = __('api.list', ['title' => 'Service with service details']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    public function serviceDetail(Request $request)
    {
        $status = false;
        $message = __('api.no_record');
        $date = ($request->date)?date('Y-m-d',strtotime($request->date)):date('Y-m-d',strtotime('1 days'));
       /* $ServiceDetail = ServiceDetail::select('id', 'service_id', 'slot_start_time', 'slot_end_time', 'slot_limit')
        ->where([['status', 1],['service_id',$request->id]])->get();*/

       $services =  Service::with('media')->where('id', $request->id)->get();
       $sql = "select fu_service_details.id, fu_service_details.service_id, slot_start_time, slot_end_time, slot_limit, (ifnull(slot_limit,0)-count(fu_service_user.slot_id)) as available_slot from fu_service_details left join fu_service_user on service_date='$date' and fu_service_user.slot_id=fu_service_details.id where status=1 and fu_service_details.service_id=$request->id group by fu_service_details.id order by fu_service_details.id ASC";
        $ServiceDetail = DB::select($sql);
          
        
        
        if ($ServiceDetail) 
        {
            $status = true;
           
            $response['data']['service_details'] = $ServiceDetail;
            $response['data']['services'] = $services;
            $message = __('api.list', ['title' => 'Service details']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    /**
     * Store a newly booking.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'service_id'=>'required|numeric',
            'slot_id'=>'required|numeric',
            'vehicle_id'=>'required|numeric',
            'latitude'=>'required',
            'longitude'=>'required',
            'pickup_address'=>'required',
            'service_date'=>'required',
            'service_end_time'=>'required',
            'service_start_time'=>'required',
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $user_id = $request->user()->id;

        /*$sql= "select * from fu_service_user where user_id=$user_id and vehicle_id='".$request->vehicle_id."' and service_date='".date('Y-m-d', strtotime("$request->service_date"))."'";
        $resultObj = DB::selectOne("select exists($sql) as `exists`");

        if($resultObj->exists)
        {
            $response['status'] = false;
            $response['message'] =__('api.dynamic_msg', ['title' => 'Same date slot already booked for this vehicle']);
            return $this->sendResponse($response);
        }*/
           
 
        $services = Service::find($request->service_id);
        $order_no = generateNumber('SERVICES');
        $createUserService = array(
            'service_id'=>$request->service_id,
            'user_id'=>$user_id,
            'slot_id'=>$request->slot_id,
            'vehicle_id'=>$request->vehicle_id,
            'service_end_time'=>$request->service_end_time ,
            'service_start_time'=>$request->service_start_time,
            'service_date'=>date('Y-m-d', strtotime("$request->service_date")),
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude,
            'pickup_address'=>$request->pickup_address,
            'order_no'=>$order_no,
       );
        $services->users()->attach($user_id,$createUserService);
        $insert_id = $services->users()->select('service_user.id')->where("user_id", "=",$user_id)->withPivot("id")->orderBy('id', 'desc')->first()->pivot->id;
        if ($insert_id) {
            $response['data']=['id'=>$insert_id,'order_no'=>$order_no];
            $status = true;
            $message = __('api.insert', ['title' => 'Service Request']);
        }

        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }
    function userServiceList(Request $request)
    {
        $status = false;
        $message = __('api.try_again');
        $user_id = $request->user()->id;
        $sql = "select fu_service_user.id,vehicle_id, registration_number,vehicle_type,fuel_type, service_id, fu_service_user.user_id, latitude, longitude, pickup_address, service_date, service_start_time,service_end_time, service_status, case when service_status=1 then 'Completed' else 'Pending' end as status  ,payment_no,order_no, payment_status, fu_service_user.created_at, service_name, fu_services.amount as service_amount, fu_services.convenience as service_convenience,payment_amount from fu_service_user left join fu_services on fu_services.id=fu_service_user.service_id left join fu_vehicles on fu_vehicles.id=fu_service_user.vehicle_id where fu_service_user.user_id=$user_id order by fu_service_user.id desc ";
        $ServiceDetail = DB::select(DB::raw($sql)); 
        if ($ServiceDetail) 
        {

                foreach ($ServiceDetail as $vehicle) {
                    $vehicle->icon_path = vehicleIcon($vehicle->vehicle_type);
                   }
            $status = true;
            $response['data']['service_details'] = $ServiceDetail;
            $message = __('api.list', ['title' => 'Service details']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    
}
