<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Wallet,User,Profile};
use Validator;
use Illuminate\Validation\Rule;

class WalletController extends Controller
{
    /**
     * Display a wallet listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = false;
        $message = __('api.no_record');
         
        $wallets =  Wallet::select('id', 'user_id', 'amount', 'created_at')
            ->where('user_id', $request->user()->id)
            ->orderBy('id','desc')
            ->get();
        if ($wallets->count()) {
            $status = true;
            $response['data']['wallets'] = $wallets;
            $message = __('api.list', ['title' => 'Wallet']);
        }
        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    /**
     * Store a newly wallet created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $status = false;
        $message = __('api.try_again');
        $validator = Validator::make($request->all(), [
            'amount'=>'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $this->sendResponse($response);
        }
        $user_id = $request->user()->id;
        $input['user_id'] = $user_id;
        $input['amount'] = $request->amount;
        $wallet = Wallet::create($input);
        if ($wallet) {
           
            $user = User::find($user_id);
            $wallet_amount = $user->profile->wallet_amount+$request->amount;
            $user->profile()->update(['wallet_amount'=>$wallet_amount]);
         
            $status = true;
            $response['data']['wallet'] = $wallet->only('id', 'user_id', 'amount', 'created_at');
            $message = __('api.insert', ['title' => 'Wallet']);
        }

        $response['message'] = $message;
        $response['status'] = $status;
        return $this->sendResponse($response);
    }

    
}
