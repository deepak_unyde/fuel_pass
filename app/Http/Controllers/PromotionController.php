<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use Illuminate\Http\Request;
use App\Http\Requests\{StorePromotionRequest};
class PromotionController extends Controller
{
    private $title;

    public function __construct()
    {
        $this->title = "Promotion";
        $this->middleware('permission:promotion-list', ['only' => ['index']]);
        $this->middleware('permission:promotion-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:promotion-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:promotion-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $promotions = Promotion::select('id', 'title', 'heading','short_description','description', 'url', 'from_date', 'to_date', 'status', 'created_at')
            ->with(['media']) 
            ->orderBy('id', 'desc')
            ->condition($request)
            ->paginate();
            #return $promotions;
        return view('promotions.index', compact('promotions'))->with(['title' => $this->title]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promotions.create')->with(['title' => $this->title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StorePromotionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePromotionRequest $request)
    {
        $request['to_date']=getDateFromDate($request->to_date);
        $request['from_date']=getDateFromDate($request->from_date);
        $promotion = Promotion::create($request->all());
        if ($promotion) {

            if ($request->hasFile('promotion_image') && $request->file('promotion_image')->isValid()) {
                $promotion->addMediaFromRequest('promotion_image')->toMediaCollection('promotion_image');
            }
            return redirect()->route('promotions.index')
                ->with('success', __('messages.insert', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        return view('promotions.edit', compact('promotion'))->with(['title' => $this->title]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\StorePromotionRequest  $request
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(StorePromotionRequest $request, Promotion $promotion)
    {
        $request['to_date']=getDateFromDate($request->to_date);
        $request['from_date']=getDateFromDate($request->from_date);
        if ($promotion->update($request->all())) {
           
            if ($request->hasFile('promotion_image') && $request->file('promotion_image')->isValid()) {
                $promotion->media()->delete();
                $promotion->addMediaFromRequest('promotion_image')->toMediaCollection('promotion_image');
            }
            
            return redirect()->route('promotions.index')->with('success', __('messages.update', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotion)
    {
        if($promotion->delete())
        {
            return redirect()->route('promotions.index')->with('success', __('messages.delete', ['title' => $this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));  
        }
    }
}
