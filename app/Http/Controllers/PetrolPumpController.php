<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Company,PetrolPump,Price,Rating}; 
use App\Http\Requests\{StorePetrolPumpRequest, UpdatePetrolPumpRequest};
use DB; 
class PetrolPumpController extends Controller
{
    private $title;
    private $companies;
    private $prices;

    public function __construct()
    {
        $this->title = "Petrol Pump";
        $this->companies = Company::getAll();
        $this->prices = Price::getAll();
        $this->middleware('permission:petrol-pump-list', ['only' => ['index']]);
        $this->middleware('permission:petrol-pump-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:petrol-pump-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:petrol-pump-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $petrolPumps = PetrolPump::select('petrol_pumps.id','price_id', 'company_id', 'code', 'name', 'beacon_id','address','pincode','latitude','longitude', 'status', 'opening_time', 'closing_time', 'petrol_pumps.created_at'
        , DB::raw('ROUND(IFNULL(avg(rating),0),2) as rating'))
            ->with(['company' => function ($query) {
                $query->select('id', 'company_name');
            }])
            ->with(['price' => function ($query) {
                $query->select('id', 'state_name','petrol_cost');
            }])
            ->leftJoin('ratings', [['ratings.petrol_pump_id','=','petrol_pumps.id']])
            ->condition($request)
            ->groupBy('petrol_pumps.id')
            ->orderBy('id', 'desc')
            ->paginate();

            
        return view('petrol-pumps.index', compact('petrolPumps'))->with(['title' => $this->title,'companies'=> $this->companies,'prices'=>$this->prices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('petrol-pumps.create')->with(['companies' => $this->companies, 'title' => $this->title,'prices'=>$this->prices]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StorePetrolPumpRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePetrolPumpRequest $request)
    {
        $petrolPump = PetrolPump::create($request->all());
        if ($petrolPump) {
            return redirect()->route('petrol-pumps.index')
                ->with('success', __('messages.insert', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PetrolPump $petrolPump)
    {
        $loginUserCompanyId = loginUserCompanyId();
        $loginUserRoleId = loginUserRoleId();
        if($loginUserCompanyId > 0)
        {
            if($petrolPump->company_id==$loginUserCompanyId)
            return view('petrol-pumps.edit', compact('petrolPump'))->with(['title' => $this->title, 'companies' => $this->companies,'prices'=>$this->prices]);
            else
            abort(403);
        }
        else
        {
            return view('petrol-pumps.edit', compact('petrolPump'))->with(['title' => $this->title, 'companies' => $this->companies,'prices'=>$this->prices]);
        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UpdatePetrolPumpRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePetrolPumpRequest $request, PetrolPump $petrolPump)
    {
        if ($petrolPump->update($request->all())) {
            return redirect()->route('petrol-pumps.index')->with('success', __('messages.update', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PetrolPump $petrolPump)
    {
        if ($petrolPump->delete()) {
            return redirect()->route('petrol-pumps.index')->with('success', __('messages.delete', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
}
