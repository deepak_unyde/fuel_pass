<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Price; 
use App\Http\Requests\{UpdatePriceRequest,StorePriceRequest};

class PriceController extends Controller
{
    private $title;

    public function __construct()
    {
        $this->title = "Fuel Price";
        $this->middleware('permission:price-list', ['only' => ['index']]);
        $this->middleware('permission:price-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:price-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:price-delete', ['only' => ['destroy']]);
        
    }
    public function index(Request $request)
    {
       $prices = Price::select(
        'id',
        'state_name',
       'state_code', 
       'petrol_cost', 
       'diesel_cost',
       'cng_cost',
       'created_at','updated_at')
       ->condition($request)
       ->orderBy('state_name', 'ASC')
       ->paginate();
       return view('prices.index',compact('prices'))->with(['title'=>$this->title]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('prices.create')->with(['title' => $this->title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePriceRequest $request)
    {
        $price = Price::create($request->all());
        if ($price) {
            return redirect()->route('prices.index')
                ->with('success', __('messages.insert', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Price  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Price $price)
    {
        return view('prices.edit', compact('price'))->with(['title' => $this->title]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Price  $company
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePriceRequest $request, Price $price)
    {
        if ($price->update($request->all())) {
            return  redirect()->route('prices.index')->with('success', __('messages.update', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function destroy(Price $price)
    {
        if ($price->delete()) {
            return redirect()->route('prices.index')->with('success', __('messages.delete', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
}
