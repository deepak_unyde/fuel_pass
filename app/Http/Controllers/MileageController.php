<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\{User,Mileage}; 
use App\Http\Requests\StoreMileageRequest;

class MileageController extends Controller
{
    private $title,$users;

    public function __construct()
    {
        $this->title = "Mileage";
        $this->users = User::getAllAppUser();
        $this->middleware('permission:mileage-list', ['only' => ['index']]);
        $this->middleware('permission:mileage-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:mileage-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:mileage-delete', ['only' => ['destroy']]);
    }
     
     
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
       $mileages = Mileage::select('id','user_id','meter_reading','created_at')
       ->with(['user'=> function ($query){
              $query->select('id','name','mobile'); 
           }])
       ->orderBy('id', 'desc')           
       ->condition($request)
       ->paginate();
       
       return view('mileages.index',compact('mileages'))->with(['title'=>$this->title,'users'=>$this->users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mileages.create')->with(['title'=>$this->title,'users'=>$this->users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMileageRequest $request)
    {
        $mileage = Mileage::create($request->all());
        if($mileage)
        {
            return redirect()->route('mileages.index')->with('success', __('messages.insert',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mileage $mileage)
    {
       return view('mileages.edit',compact('mileage'))->with(['title'=>$this->title,'users'=>$this->users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mileage $mileage)
    {
        if($mileage->update($request->all()))
        {
            return redirect()->route('mileages.index')->with('success', __('messages.update',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mileage $mileage)
    {
        if($mileage->delete()){
            return redirect()->route('mileages.index')->with('success', __('messages.delete',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
}
