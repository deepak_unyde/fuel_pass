<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\{User,PetrolPump,PetrolUser};  

class RewardController extends Controller
{
    private $title,$users,$petrolPumps;

    public function __construct()
    {
        $this->title = "Transaction";
        $this->users = User::getAllAppUser();
        $this->petrolPumps = PetrolPump::getAll();
        #$this->middleware('permission:reward-list', ['only' => ['index']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $rewards = PetrolUser::select('id','user_id','package_id','petrol_pump_id','amount','reward_point','cashback','status','payment_no','response', 'created_at')
       ->with(['user'=> function ($query){
              $query->select('id','name','mobile'); 
           }])
       ->with(['package'=> function ($query){
            $query->select('id','package_name'); 
         }])
       ->with(['petrolPump'=> function ($query){
            $query->select('id','company_id','name')
            ->with(['company'=> function ($queryCompany){
                $queryCompany->select('id','company_name'); 
             }]); 
         }])     
         ->orderBy('id', 'desc')  
       ->condition($request)
       ->paginate();
       
       return view('rewards.index',compact('rewards'))->with(['title'=>$this->title,'users'=>$this->users,'petrolPumps'=>$this->petrolPumps]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
