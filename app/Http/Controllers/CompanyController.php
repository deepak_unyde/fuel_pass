<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\{Company,User,PetrolPump,Rating,PetrolUser,ServiceDetail}; 
use App\Http\Requests\{StoreCompanyRequest, UpdateCompanyRequest};
use Hash,Response;
use Illuminate\Support\Facades\Mail;
use App\Mail\SignupMail;
use Illuminate\Support\Str;
use DB;
class CompanyController extends Controller
{
    private $title,$role_id;

    public function __construct()
    {
        #$this->role_id = Auth::user()->roles()->first()->id;
        $this->title = "Company";
        $this->middleware('permission:company-list', ['only' => ['index']]);
        $this->middleware('permission:company-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:company-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:company-delete', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {
        
        $companies = Company::select('id', 'company_name', 'company_mobile','company_email','company_address', 'status', 'created_at')
            ->with(['media']) 
            ->orderBy('id', 'desc')
            ->condition($request)
            ->paginate();
            #return $companies;
        return view('companies.index', compact('companies'))->with(['title' => $this->title]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create')->with(['title' => $this->title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreCompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanyRequest $request)
    {
        if (User::where('email', $request->company_email)->exists()) {
            return redirect()->back()->with('error', 'Email already exists.Please choose another email id.');
        }
        elseif (User::where('mobile', $request->company_mobile)->exists()) {
            return redirect()->back()->with('error', 'Mobile already exists.Please choose another mobile no.');
        } else {
            $company = Company::create($request->all());
            if ($company) {
                $userData = [];
                $userData['name'] = $request->company_name;
                $userData['email'] = $request->company_email;
                $userData['mobile'] = $request->company_mobile;
                $userData['company_id'] = $company->id;
                $userData['password'] = Hash::make($request->post('password'));
                $user = User::create($userData);
                if ($user) {
                    $user->assignRole(3);
                    if ($request->login_detail) {
                        $request['email']  = $request->company_email;
                        Mail::to($user)->send(new SignupMail($request));
                    }
                }
            if ($request->hasFile('company_logo') && $request->file('company_logo')->isValid()) {
                    $company->addMediaFromRequest('company_logo')->toMediaCollection('company_logo');
            } 
                return redirect()->route('companies.index')->with('success', __('messages.insert', ['title' => $this->title]));
            } else {
                return redirect()->back()->with('error', __('messages.try_again'));
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        $loginUserCompanyId = loginUserCompanyId();
        $loginUserRoleId = loginUserRoleId();
        if($loginUserCompanyId > 0)
        {
            if($company->id==$loginUserCompanyId)
            return view('companies.edit', compact('company'))->with(['title' => $this->title]);
            else
            abort(403);
        }
        else
        {
            return view('companies.edit', compact('company'))->with(['title' => $this->title]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UpdateCompanyRequest  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        $company_email = $request->company_email;
        unset($request['company_email']);
        if ($company->update($request->all())) {
            if ($company->status == 0) {
                $company->userToken($company->id);
            }
            if ($request->login_detail) {
                $user = User::where('email', $company_email)->first();
                if ($user) {
                    $random = Str::random(8);
                    $user['password'] = Hash::make($random);
                    $user->save();
                    $request['password'] = $random;
                    Mail::to($user)->send(new SignupMail($request));
                }
            }
            if ($request->hasFile('company_logo') && $request->file('company_logo')->isValid()) {
                //$company->clearMediaCollection('company_logo');
                $company->media()->delete();
                $company->addMediaFromRequest('company_logo')->toMediaCollection('company_logo');
            }
            
            return redirect()->route('companies.index')->with('success', __('messages.update', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        if ($company->delete()) {
            return redirect()->route('companies.index')->with('success', __('messages.delete', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    ###################START ALL AJAX CALL CODE ##################
    public function ajaxRequest(Request $request)
    {
        $request_from = null;
        if ($request->request_from) {
           $request_from = $request->request_from;
        }
        unset($request['request_from']);
     
        if ($request->has('create')) {
            if ($request->create != "false") {
                $request['status'] = 1;
            }
            unset($request['create']);
        }
      
        if ($request_from == 'petrolPump') {
            $petrolPump = PetrolPump::getAll($request->all());
            return response::json($petrolPump);
        } elseif ($request_from == 'UserAvgRating') {
            $userRating = Rating::select('id','petrol_pump_id','user_id','petrol_user_id', DB::raw('ROUND(IFNULL(avg(rating),0),2) as avg_rating, "UserAverage" as nType'))
                ->with(['user' => function ($query) {
                    $query->select('id', 'name','mobile');
                }])
                ->with(['petrolPump' => function ($query) {
                    $query->select('id', 'code','name','address','beacon_id');
                }])
                ->where('petrol_pump_id',$request->petrol_pump_id)
                ->groupBy('user_id')
                ->orderBy('user_id')
                ->get();
            return view('petrol-pumps.ajax_data', compact('userRating'));
        } 
        elseif ($request_from == 'Home') {
          
         $ntype = ($request->ntype)?$request->ntype:'date';  
          $from_date = ($request->from_date)?date('Y-m-d',strtotime($request->from_date)):date('Y-m-d',strtotime('-7 days'));
          $to_date = ($request->to_date)?date('Y-m-d',strtotime($request->to_date)):date('Y-m-d');
           if(loginUserRoleId()==3)
           {
            return PetrolUser::select(DB::raw("case when '$ntype'='month' THEN date_format(fu_petrol_users.created_at,'%b-%Y') else date_format(fu_petrol_users.created_at,'%d-%b-%Y')  end as Date, sum(amount) as amount "))
            ->with(['petrolPump'])
            ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')   
            ->where('petrol_pumps.company_id','=',loginUserCompanyId())
            ->whereRaw(DB::raw("date(fu_petrol_users.created_at) between '".$from_date."' and '".$to_date."'"))
            ->where('petrol_users.status','success')
            ->groupBy(DB::raw("$ntype(fu_petrol_users.created_at)"))
            ->get();
            #'".$from_date."'
           }
           else
           {
            return PetrolUser::select(DB::raw("case when '$ntype'='month' THEN date_format(fu_petrol_users.created_at,'%b-%Y') else date_format(fu_petrol_users.created_at,'%d-%b-%Y')  end as Date, sum(amount) as amount "))
            ->whereRaw(DB::raw("date(fu_petrol_users.created_at) between '".$from_date."' and '".$to_date."'"))
            ->where('petrol_users.status','success')
            ->groupBy(DB::raw("$ntype(fu_petrol_users.created_at)"))
            ->get();
           }
        }
        elseif($request_from == 'Service') {
            $serviceDetails = ServiceDetail::
                where('service_id',$request->service_id)
                ->orderBy('id','desc')
                ->get();
              //  return $serviceDetails;
            return view('services.ajax-slot-list', compact('serviceDetails'));
        } 

        else {
        }
    }
        ###################END ALL AJAX CALL CODE ##################
    
}
