<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{User,PetrolPump,PetrolUser};   
class TransactionController extends Controller
{
    private $title,$users,$petrolPumps;

    public function __construct()
    {
        $this->title = "Transaction";
        $this->users = User::getAllAppUser();
        $this->petrolPumps = PetrolPump::getAll();
        $this->middleware('permission:transaction-list', ['only' => ['index']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        if(loginUserCompanyId() && loginUserRoleId()==3)
            {
                $rewards = PetrolUser::select('petrol_users.id','user_id','package_id','petrol_pump_id','amount','reward_point','cashback','petrol_users.status','payment_no','response', 'petrol_users.created_at')
                ->with(['user'=> function ($query){
                        $query->select('id','name','mobile'); 
                    }])
                ->with(['package'=> function ($query){
                        $query->select('id','package_name'); 
                    }])
                ->with(['petrolPump'=> function ($query){
                        $query->select('id','company_id','name')
                        ->with(['company'=> function ($queryCompany){
                            $queryCompany->select('id','company_name'); 
                        }]); 
                    }])
                    ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')     
                    ->orderBy('id', 'desc')  
                ->condition($request)
                ->paginate();
                $this->petrolPumps = PetrolPump::where([['status',1],['company_id',loginUserCompanyId()]])->orderBy('name')->pluck('name', 'id');
               
            }
            else
            {
                $rewards = PetrolUser::select('id','user_id','package_id','petrol_pump_id','amount','reward_point','cashback','status','payment_no','response', 'created_at')
                ->with(['user'=> function ($query){
                        $query->select('id','name','mobile'); 
                    }])
                ->with(['package'=> function ($query){
                        $query->select('id','package_name'); 
                    }])
                ->with(['petrolPump'=> function ($query){
                        $query->select('id','company_id','name')
                        ->with(['company'=> function ($queryCompany){
                            $queryCompany->select('id','company_name'); 
                        }]); 
                    }])
                ->orderBy('id', 'desc')  
                ->condition($request)
                ->paginate();
            }
       return view('rewards.index',compact('rewards'))->with(['title'=>$this->title,'users'=>$this->users,'petrolPumps'=>$this->petrolPumps]);
    }
}
