<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\{RedeemGift,Package,PetrolPump,User,PetrolUser,Company}; 
use DB,Auth;
class HomeController extends Controller
{
    private $title;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { 
        $this->title = "Dashboard";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $folderName = "";
        $fileName = "home";
        $data = [];
        $date = date('Y-m-d');
        $loginUserCompanyId = loginUserCompanyId();
        if($loginUserCompanyId && loginUserRoleId()==3)
            {
              
                $fileName = 'home-company';
                $data['petrol_pump_count'] = PetrolPump::where('company_id',$loginUserCompanyId)->count();

                $data['today_petrol_transaction'] = PetrolUser::with(['petrolPump'])
                ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')
                ->where('petrol_pumps.company_id','=',$loginUserCompanyId)
                ->whereDate('petrol_users.created_at',$date)
                ->where('petrol_users.status','success')
                ->sum('amount');

                $data['today_petrol_reward'] = PetrolUser::with(['petrolPump'])
                ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')
                ->where('petrol_pumps.company_id','=',$loginUserCompanyId)
                ->whereDate('petrol_users.created_at',$date)
                ->where('petrol_users.status','success')
                ->sum('reward_point');

                $data['today_petrol_cashback'] = PetrolUser::with(['petrolPump'])
                ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')
                ->where('petrol_pumps.company_id','=',$loginUserCompanyId)
                ->whereDate('petrol_users.created_at',$date)
                ->where('petrol_users.status','success')
                ->sum('cashback');

                #Total Sum
                $data['total_petrol_transaction'] = PetrolUser::with(['petrolPump'])
                ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')
                ->where('petrol_pumps.company_id','=',$loginUserCompanyId)
                ->where('petrol_users.status','success')
                ->sum('amount');

                $data['total_petrol_reward'] = PetrolUser::with(['petrolPump'])
                ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')
                ->where('petrol_pumps.company_id','=',$loginUserCompanyId)
                ->where('petrol_users.status','success')
                ->sum('reward_point');

                $data['total_petrol_cashback'] = PetrolUser::with(['petrolPump'])
                ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')
                ->where('petrol_pumps.company_id','=',$loginUserCompanyId)
                ->where('petrol_users.status','success')
                ->sum('cashback');


                $data['transaction_graph'] = PetrolUser::select(DB::raw("date_format(fu_petrol_users.created_at,'%d-%b-%Y') as Date, sum(amount) as amount "))
                ->with(['petrolPump'])
                ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')
                ->where('petrol_pumps.company_id','=',$loginUserCompanyId)
                ->whereRaw(DB::raw("date(fu_petrol_users.created_at) between '".date('Y-m-d',strtotime('-7days'))."' and '".date('Y-m-d')."'"))
                ->where('petrol_users.status','success')
                ->groupBy(DB::raw("date(fu_petrol_users.created_at)"))
                ->get();

                # return $data;
                return view('home-company',compact('data'))->with(['title' =>$this->title]);
            }
        else
        {    
            $companies = Company::getAll();
            $fileName = 'home';
           
            $todayTransactionWalletHistory = DB::select(DB::raw("SELECT * from (
            (select IFNULL(Sum(amount),0) As Transaction,IFNULL(sum(reward_point),0) As Reward,IFNULL(sum(cashback),0) as Cashback from fu_petrol_users where status='success' and date(created_at)=current_date()) as A, 
            (select IFNULL(sum(amount),0) as Wallet from fu_wallets where date(created_at)=current_date()) as B, (
select count(id) as 'Petrol Pump' from fu_petrol_pumps where date(created_at)=current_date()) as C ,(select count(id) as 'User' from fu_users where date(created_at)=current_date() and status='1') as D)"));

            $totalTransactionWalletHistory = DB::select(DB::raw("SELECT * from (
            (select IFNULL(Sum(amount),0) As Transaction,IFNULL(sum(reward_point),0) As Reward,IFNULL(sum(cashback),0) as Cashback  from fu_petrol_users where status='success') as A, 
            (select IFNULL(sum(amount),0) as Wallet from fu_wallets where 1=1 ) as B, (
select count(id) as 'Petrol Pump' from fu_petrol_pumps where 1=1) as C, (select count(id) as 'User' from fu_users where  status='1') as D)"));

        $data['petrol_pump_count'] = PetrolPump::where('status',1)->count();
        $data['user_count'] = User::where('status',1)->count();
        $data['redeemGift_count'] = RedeemGift::where('status',1)->count();
        $data['package_count'] = Package::where([['to_date','>=',date('Y-m-d')],['status',1]])->count();


        $data['transaction_graph'] = PetrolUser::select(DB::raw("date_format(fu_petrol_users.created_at,'%d-%b-%Y') as Date, sum(amount) as amount "))
        ->with(['petrolPump'])
        ->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id')
        ->whereRaw(DB::raw("date(fu_petrol_users.created_at) between '".date('Y-m-d',strtotime('-7days'))."' and '".date('Y-m-d')."'"))
        ->where('petrol_users.status','success')
        ->groupBy(DB::raw("date(fu_petrol_users.created_at)"))
        ->get();

       


        return view('home', compact('todayTransactionWalletHistory','totalTransactionWalletHistory','data'))->with(['title' =>$this->title,'companies'=>$companies]);
            }
    
     #return $data;
            /*if ($fileName) {
                $fileName = $folderName . $fileName;
            }
            return view($fileName, compact('data'))->with(['title' =>$this->title]);*/
    }
}
