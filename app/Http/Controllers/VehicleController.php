<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\{User,Vehicle}; 
use App\Http\Requests\{StoreVehicleRequest,UpdateVehicleRequest};

class VehicleController extends Controller
{
    private $title,$users,$vehicleType,$fuelType;

    public function __construct()
    {
        $this->title = "Vehicle";
        $this->users = User::getAllAppUser();
        $this->vehicleType = vehicleType();
       $this->fuelType = fuelType();
        $this->middleware('permission:vehicle-list', ['only' => ['index']]);
        $this->middleware('permission:vehicle-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:vehicle-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:vehicle-delete', ['only' => ['destroy']]);
    }
     
     
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
       $vehicles = Vehicle::select(
       'id',
       'user_id', 
       'registration_number',
       'vehicle_type',
       'fuel_type',
       'term_accepted',
       'status',
       'created_at')
       ->with(['user'=> function ($query){
              $query->select('id','name','mobile'); 
           }])
           ->orderBy('id', 'desc')
       ->condition($request)
       ->paginate();
       
       //return $vehicles;
       return view('vehicles.index',compact('vehicles'))->with(['title'=>$this->title,'users'=>$this->users,'fuelType'=>$this->fuelType,'vehicleType'=>$this->vehicleType]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
        return view('vehicles.create')->with(['title'=>$this->title, 'users'=>$this->users,'fuelType'=>$this->fuelType,'vehicleType'=>$this->vehicleType]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVehicleRequest $request)
    {
        $vehicle =  Vehicle::create($request->all());
        if($vehicle){
            return redirect()->route('vehicles.index')->with('success', __('messages.insert',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again')); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        return view('vehicles.edit', compact('vehicle'))->with(['title'=>$this->title, 'users'=>$this->users,'fuelType'=>$this->fuelType,'vehicleType'=>$this->vehicleType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVehicleRequest $request, Vehicle $vehicle)
    {
        if($vehicle->update($request->all())){
            return redirect()->route('vehicles.index')->with('success', __('messages.update',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again')); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function destroy(Vehicle $vehicle)
    {
        if($vehicle->delete())
        {
            return redirect()->route('vehicles.index')->with('success',__('messages.delete',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
}
