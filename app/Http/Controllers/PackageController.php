<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Package; 
use App\Http\Requests\{StorePackageRequest};

class PackageController extends Controller
{

    private $title;
     
    public function __construct()
    {
        $this->title = "Package";
        $this->middleware('permission:package-list', ['only' => ['index']]);
        $this->middleware('permission:package-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:package-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:package-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $packages = Package::select(
        'id',
        'package_name',
        'package_description',
        'from_date',
        'to_date',
        'duration',
        'cost',
        'discount',
        'discount_percentage',
        'reward_point',
        'cashback',
        'status',
        'created_at'
        )
        ->with(['media']) 
        ->orderBy('id', 'desc')
        ->condition($request)
        ->paginate();

     
 return view('packages.index',compact('packages'))->with(['title'=>$this->title]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('packages.create')->with(['title'=>$this->title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackageRequest $request)
    {
        $insertPackageArray = array(
            'package_name'=>$request->package_name,
            'package_description'=>$request->package_description,
            'duration'=>$request->duration,
            'cost'=>$request->cost,
            'discount'=>$request->discount,
            'discount_percentage'=>($request->discount_percentage)?$request->discount_percentage:0,
            'reward_point'=>$request->reward_point,
            'cashback'=>$request->cashback,
            'status'=>$request->status,
            'from_date'=>getDateFromDate($request->from_date),
            'to_date'=>getDateFromDate($request->to_date),
        );
        $package = Package::create($insertPackageArray);
        if ($package) {

            if ($request->hasFile('package_image') && $request->file('package_image')->isValid()) {
                $package->addMediaFromRequest('package_image')->toMediaCollection('package_image');
            }
            return redirect()->route('packages.index')
                ->with('success', __('messages.insert', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        return view('packages.edit', compact('package'))->with(['title' => $this->title]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePackageRequest $request, Package $package)
    {
        $updatePackageArray = array(
            'package_name'=>$request->package_name,
            'package_description'=>$request->package_description,
            'duration'=>$request->duration,
            'cost'=>$request->cost,
            'discount'=>$request->discount,
            'discount_percentage'=>($request->discount_percentage)?$request->discount_percentage:0,
            'reward_point'=>$request->reward_point,
            'cashback'=>$request->cashback,
             
            'status'=>$request->status,
            'from_date'=>getDateFromDate($request->from_date),
            'to_date'=>getDateFromDate($request->to_date),
        );
        if($package->update($updatePackageArray))
        {
            if ($request->hasFile('package_image') && $request->file('package_image')->isValid()) {
              $package->media()->delete();
             // $package->clearMediaCollection('package_image');
              $package->addMediaFromRequest('package_image')->toMediaCollection('package_image');
            }
            return redirect()->route('packages.index')->with('success', __('messages.update',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        if($package->delete()){
            return redirect()->route('packages.index')->with('success', __('messages.delete',['title'=>$this->title]));
        }
        else{
            redirect()->back()->with('error',__('messages.try_again'));
        }
    }
}
