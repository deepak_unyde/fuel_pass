<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Company,PetrolPump,Package};   
use DB;
class TransactionController extends Controller
{
    public function __construct()
    {
        $this->title = "Transaction History";
        $this->companies = Company::getAll();
    }
    public function index(Request $request)
    {
        $petrolPump = PetrolPump::getPetrolPump();
        $companies = Company::getCompany();
        $package = Package::getAll();
        $LoginId = loginUserId();
        $RoleId = loginUserRoleId();
        
        $CompanyId = ($request->company_id)?$request->company_id:loginUserCompanyId();

        $PetrolPumpId = ($request->petrol_pump_id)?$request->petrol_pump_id:0;
        $PackageId = ($request->package_id)?$request->package_id:0;
        $PaymentNo = ($request->s)?$request->s:'';
        $FromDate = ($request->from_date)?date('Y-m-d',strtotime($request->from_date)):date('Y-m-d',strtotime('-7 days'));
        $ToDate = ($request->to_date)?date('Y-m-d',strtotime($request->to_date)):date('Y-m-d');
        $sStatus = ($request->status)?$request->status:'success';
        $UserId = ($request->user_id)?$request->user_id:0;        

       $statement="usp_ReportTransaction($LoginId,$RoleId,$CompanyId,$PetrolPumpId,$PackageId,'$PaymentNo','$FromDate', '$ToDate', '$sStatus',$UserId)";
       $transactions =  getProcedureData($statement);
       $tableHeader = ($transactions)?array_keys((array)$transactions[0]):array('#');
       
       
       return view('transactions.index', compact('transactions','tableHeader'))->with(['title' => $this->title,'companies'=> $companies,'petrolPump'=>$petrolPump,'package'=>$package]);
    }
}
