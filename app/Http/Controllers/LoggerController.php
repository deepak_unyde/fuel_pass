<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\{Logger};   
use DB;
class LoggerController extends Controller
{
    private $title ;

    public function __construct()
    {
        $this->title = "Vivek Data";
       # $this->middleware('permission:vivek-list', ['only' => ['index']]);
    }

    public function index(Request $request)
    {
        $deviceList = Logger::SELECT(DB::raw("distinct(device_name)"))->get();
        $loggers = Logger::select('id',   'lati','longi','accuracy','vehicle_id','azimuth','elevation','cnhz','constellation_type','constellation_count','device_name','created_at','updated_at')
        ->orderBy('id', 'desc')
        ->condition($request)
        ->paginate(100);
        #return $loggers;
        return view('vivek.index', compact('loggers','deviceList'))->with(['title' => $this->title]);
    }
     
    public function graph(Request $request)
    { 
       //echo $device_name = join("','",$_REQUEST['device_name']);exit;
       // return ($_REQUEST['device_name']);
      $deviceList = Logger::SELECT(DB::raw("distinct(device_name)"))
       ->get();
     #  return $deviceName;
        $loggers = Logger::select('id',   'lati','longi','accuracy','vehicle_id','azimuth','elevation','cnhz','constellation_type','constellation_count','device_name','created_at','updated_at')
        ->orderBy('id', 'desc')
        ->condition($request)
        ->get();
        //->paginate(100);
       # return $loggers;
        return view('vivek.view', compact('loggers', 'deviceList'))->with(['title' =>$this->title]);
         
    }
    public function create(Request $request)
    {
       if (Logger::truncate()) {
            return redirect()->route('logger.index')->with('success', __('messages.delete', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
        
    }
}
