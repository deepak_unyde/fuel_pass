<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    private $title;
    private $group_names;
    public function __construct()
    {
        $this->title = "Permission";
        $this->group_names = Permission::groupBy('group_name')
            ->pluck('group_name', 'group_name');
        $this->middleware('permission:permission-list', ['only' => ['index']]);
        $this->middleware('permission:permission-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:permission-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:permission-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = Permission::where(function ($query) use ($request) {
            $group_name = $request->get('group_name');
            $s = $request->s;
            if ($group_name) {
                $query->where('group_name', $group_name);
            }
            if ($s) {
                $query->where('name', 'LIKE', "%$s%");
            }
        })
            ->orderBy('group_name', 'asc')
            ->paginate();
        return view('permissions.index', compact('permissions'))->with(['group_names' => $this->group_names]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group_names = $this->group_names;
        if ($group_names) {
            $group_names->put('add_new', 'Add New');
        }

        return view('permissions.create', compact('group_names'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:permissions,name'
        ]);
        $group_name =  $request->post('group_name');
        if ($group_name == "add_new") {
            $group_name = $request->post('group_name_new');
        }

        $permission = Permission::create([
            'group_name' => $group_name,
            'name' => $request->post('name')
        ]);
        if ($permission) {
            return redirect()->route('permissions.index')
                ->with('success', __('messages.insert', ['title' => 'Permission']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        $group_names = $this->group_names;
        if ($group_names) {
            $group_names->put('add_new', 'Add New');
        }
        return view('permissions.edit', compact('permission', 'group_names'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $request->validate([
            'name' => 'required|unique:permissions,name,' . $permission->id
        ]);
        $group_name =  $request->post('group_name');
        if ($group_name == "add_new") {
            $permission->group_name = $request->post('group_name_new');
        }

        $permission->name = $request->name;
        if ($permission->save()) {
            return redirect()->route('permissions.index')->with('success', __('messages.update', ['title' => 'Permission']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        if ($permission->delete()) {
            return redirect()->route('permissions.index')->with('success', __('messages.delete', ['title' => 'Permission']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
}
