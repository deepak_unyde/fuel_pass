<?php

namespace App\Http\Controllers;

use App\Models\{Service,ServiceDetail};
use Illuminate\Http\Request;
use App\Http\Requests\{UpdateServiceRequest,StoreServiceRequest};

class ServiceController extends Controller
{
    private $title;

    public function __construct()
    {
        $this->title = "Our Services";
        $this->middleware('permission:service-list', ['only' => ['index']]);
        $this->middleware('permission:service-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:service-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:service-delete', ['only' => ['destroy']]);
        #$this->middleware('permission:service-slot', ['only' => ['edit', 'update']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $services = Service::with(['media']) 
        ->orderBy('id', 'desc')  
       ->condition($request)
       ->paginate();
      # return $services;
       return view('services.index',compact('services'))->with(['title'=>$this->title]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create')->with(['title' => $this->title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServiceRequest $request)
    {
        $service = Service::create($request->all());
        if ($service) {
        if ($request->hasFile('service_image') && $request->file('service_image')->isValid()) {
                $service->addMediaFromRequest('service_image')->toMediaCollection('service_image');
            } 
            return redirect()->route('services.index')->with('success', __('messages.insert', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('services.edit', compact('service'))->with(['title' => $this->title]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UpdateServiceRequest  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceRequest $request, Service $service)
    {
        if ($service->update($request->all())) {
            if ($request->hasFile('service_image') && $request->file('service_image')->isValid()) {
                $service->media()->delete();
                $service->addMediaFromRequest('service_image')->toMediaCollection('service_image');
            }
            return redirect()->route('services.index')->with('success', __('messages.update', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if ($service->delete()) {
            $service->media()->delete();
            return redirect()->route('services.index')->with('success', __('messages.delete', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    public function delete(ServiceDetail $serviceDetail)
    {
        if ($serviceDetail->delete()) {
            return redirect()->route('services.index')->with('success', __('messages.delete', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
    function addServiceDetail(Service $service, Request $request)
    {
        $validate = $this->validate($request, [
            'service_id' => 'required',
            'slot_start_time' => 'required',
            'slot_end_time' => 'required',
            'slot_limit' => 'required|numeric'
        ]);
        $servicveDetail = ServiceDetail::create($request->all());
        if($servicveDetail)
        {
            return redirect()->route('services.index')->with('success', __('messages.insert', ['title' => 'Service slot']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
}
