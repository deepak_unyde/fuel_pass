<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\RedeemGift; 

use App\Http\Requests\{StoreRedeemGift};

class RedeemGiftController extends Controller
{
    private $title;

    public function __construct()
    {
        $this->title = "Redeem Gift";
        $this->middleware('permission:coupon-list', ['only' => ['index']]);
        $this->middleware('permission:coupon-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:coupon-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:coupon-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $coupons = RedeemGift::select(
        'id',
        'gift_name',
        'short_description',
        'from_date',
        'to_date',
        'min_gift_cost',
        'min_redeem_point',
        'redeem_cost',
        'valid_no_of_day',
        'total_coupon_qty',
        'use_coupon_qty',
        'balance_coupon_qty',
        'status',
        'created_at' )
        ->with(['media']) 
       ->condition($request)
       ->orderBy('id', 'desc')
       ->paginate();
       //return $coupons;
       return view('redeem-gifts.index',compact('coupons'))->with(['title'=>$this->title]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('redeem-gifts.create')->with(['title'=>$this->title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRedeemGift $request)
    {
        $request['to_date']=getDateFromDate($request->to_date);
        $request['from_date']=getDateFromDate($request->from_date);
        $request['balance_coupon_qty'] = $request->total_coupon_qty;
         $redeemGift = RedeemGift::create($request->all());
        if ($redeemGift) {

            if ($request->hasFile('gift_image') && $request->file('gift_image')->isValid()) 
            {
                $redeemGift->addMediaFromRequest('gift_image')->toMediaCollection('gift_image');
            }
            return redirect()->route('redeem-gifts.index')
                ->with('success', __('messages.insert', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RedeemGift  $redeemGift
     * @return \Illuminate\Http\Response
     */
    public function show(RedeemGift $redeemGift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RedeemGift  $redeemGift
     * @return \Illuminate\Http\Response
     */
    public function edit(RedeemGift $redeemGift)
    {
        return view('redeem-gifts.edit',compact('redeemGift'))->with(['title'=>$this->title]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RedeemGift  $redeemGift
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRedeemGift $request, RedeemGift $redeemGift)
    {
        $request['to_date']=getDateFromDate($request->to_date);
        $request['from_date']=getDateFromDate($request->from_date);
        
        if($redeemGift->use_coupon_qty == 0)
        {
            $request['balance_coupon_qty'] = $request->total_coupon_qty;
        }

        if ($redeemGift->update($request->all())) {

            if ($request->hasFile('gift_image') && $request->file('gift_image')->isValid()) 
            {
                $redeemGift->media()->delete();
                $redeemGift->addMediaFromRequest('gift_image')->toMediaCollection('gift_image');
            }
            return redirect()->route('redeem-gifts.index')
                ->with('success', __('messages.update', ['title' => $this->title]));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RedeemGift  $redeemGift
     * @return \Illuminate\Http\Response
     */
    public function destroy(RedeemGift $redeemGift)
    {
        if($redeemGift->delete()){
            return redirect()->route('redeem-gifts.index')->with('success', __('messages.delete', ['title' => $this->title]));
        }else{
            return redirect()->back()->with('error',__('messages.try_again'));
        }
    }
}
