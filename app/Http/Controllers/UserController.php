<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\{User,Wallet,Vehicle,Mileage,PetrolUser,PetrolPump,Package,Service,ServiceDetail};
use Illuminate\Http\Request;
use Hash;
use Spatie\Permission\Models\Role;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SignupMail;


class UserController extends Controller
{
    private $roles;
    public function __construct()
    {
        $this->roles = Role::whereNotIn('id', [2,3])->orderBy('name')->pluck('name', 'id');
        $this->middleware('permission:user-list', ['only' => ['index']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->middleware('permission:user-change-password', ['only' => ['changePassword', 'updateChangePassword']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::whereNotIn('id', [3])->orderBy('name')->pluck('name', 'id');
        $users = User::with(['media', 'roles'])
            ->whereExists(function ($query) use ($request) {
                $query->select(DB::raw(1))
                    ->from('model_has_roles')
                    ->whereColumn('model_id', 'id');
                    if ($request->role) 
                    $query->where('role_id', $request->role);
                    else
                        $query->whereIn('role_id', [1,2]);
               })
            ->where(function ($query)  use ($request) {
                $s = $request->s;
                $mobile = $request->mobile;
                if ($s) {
                    $query->where('name', 'LIKE', "%$s%");
                    $query->orWhere('email', 'LIKE', "%$s%");
                }
                if ($mobile) {
                    $query->where('mobile', 'LIKE', "%$mobile%");
                }
                if ($request->has('status')) {
                    $status = $request->status;

                    if ($status) {
                        $query->where('status', $status);
                    } else if ($status === '0') {
                        $query->where('status', $status);
                    }
                }
            })
            ->orderBy('id', 'desc')
            ->paginate();
        return view('users.index', compact('users'))->with(['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create')->with(['roles' => $this->roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|max:50',
            'mobile' => 'nullable|digits:10|unique:users,mobile',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|max:30|confirmed',
            'avatar' => 'nullable|image',
            'about_me' => 'nullable|max:500',
            'role' => 'required|numeric',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($request->password);
        $input['company_id']= 0;
        $user = User::create($input);

        if ($user) {
            $user->assignRole($request->input('role'));
            if ($request->login_detail) {
                Mail::to($user)->send(new SignupMail($request));
            }


            if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
                $user->addMediaFromRequest('avatar')->toMediaCollection('avatar');
            }
            return redirect()->route('users.index')->with('success', __('messages.create', ['title' => 'User']));
        } else {
            return redirect()->back()->with('success', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user,Request $request)
    { 
        $request->user_id=$user->id;
        ########### For User Details List ##############
        $userDetails = User::with(['media', 'roles'])
        ->select('users.id', 'users.name','users.email', 'users.mobile',  'packages.package_name', 'profiles.wallet_amount','profiles.reward_point','profiles.cashback','package_user.from_date', 'package_user.to_date', 'package_user.cost', DB::raw("CASE WHEN fu_package_user.to_date >= Current_date() THEN 'Active' Else 'Expired' END as planStatus, IFNULL(DATEDIFF(fu_package_user.to_date,current_date()),0) as remainingDays, fu_package_user.cashback as package_cashback, fu_package_user.reward_point as package_reward "))
        ->leftjoin('profiles','profiles.user_id', '=', 'users.id')
        ->leftJoin('package_user', [['package_user.id', '=', 'profiles.package_user_id'],['package_user.package_id', '=', 'profiles.package_id'],['package_user.user_id', '=', 'profiles.user_id']])
        ->leftJoin('packages', 'packages.id', '=', 'profiles.package_id')
        ->where('users.id', $user->id)->first();

        ########### For User Wallet List ##############
        $wallets = Wallet::select('id', 'user_id', 'amount', 'created_at')
        ->condition($request)
        ->orderBy('id', 'desc')
        ->paginate(10,['*'],'walletPage');
        ############## For User Vehicle list ############
        $vehicles = Vehicle::select(
            'id','user_id','registration_number','vehicle_type','fuel_type','term_accepted','status','created_at')
            //->where('user_id',$user->id)
            ->condition($request)
            ->orderBy('id', 'desc')
            ->paginate(10,['*'],'vehiclePage');
        ################### For User Mileage List ##############
         $mileages = Mileage::select('id','user_id','meter_reading','created_at')
         ->condition($request)
         ->orderBy('id', 'desc')           
         ->paginate(10,['*'],'mileagePage');   
        ##################### For user reward & cashback history ###########
        $rewards = PetrolUser::select('user_id','package_id','petrol_pump_id','amount','reward_point','cashback','status','payment_no','response', 'created_at')
        ->with(['package'=> function ($query){
            $query->select('id','package_name'); 
         }])
        ->with(['petrolPump'=> function ($query){
            $query->select('id','company_id','name')
            ->with(['company'=> function ($queryCompany){
                $queryCompany->select('id','company_name'); 
             }]); 
        }]) 
        ->condition($request)   
        ->orderBy('id', 'desc')  
        ->paginate(10,['*'],'rewardPage'); 
        $petrolPumps = PetrolPump::getAll();
        $package = Package::getAll();
        ##############User Purchase Package List####################
        $userPackage =  User::with(['packages' => function ($query) {
            $query->with('media')
            ->select('packages.id','packages.package_name','payments.transaction_no','payments.payment_no','package_user.package_id','package_user.user_id','package_user.reward_point','package_user.cashback','package_user.from_date','package_user.to_date','package_user.cost','package_user.status','package_user.created_at')
            ->leftJoin('payments', 'payments.id', '=', 'transaction_id');
             }])
        ->where('id', $user->id)
        ->paginate(false);


        ##############User Redeem Request List####################
        $userRedeem =  User::with('redeem')
        ->where('id', $user->id)
        ->orderBy('id', 'desc')
        ->paginate();
         
      #  return $userRedeem;


         ##############Service  Request List####################
       

        return view('users.view', compact('user','userDetails','wallets','vehicles','mileages','rewards','petrolPumps','package','userPackage'))->with(['title' =>'']);
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::whereNotIn('id', [3])->orderBy('name')->pluck('name', 'id');
        $user_has_role = null;
        if ($user->roles->first()) {
            $user_has_role = $user->roles->first()->id;
        }
        return view('users.edit', compact('user', 'user_has_role'))->with(['roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @parxam  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
       
        $request->validate([
            'name' => 'required|min:2|max:50',
            'mobile' => 'nullable|digits:10|unique:App\Models\User,mobile,' . $user->id,
            'email' => 'required|email|unique:App\Models\User,email,' . $user->id,
            'avatar' => 'nullable|image',
            'about_me' => 'nullable|max:500',
           // 'role' => 'required|numeric',
        ]);
        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->about_me = $request->about_me;
        $user->status = $request->status;
        
        if ($user->save()) {
            if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
                $user->addMediaFromRequest('avatar')->toMediaCollection('avatar');
            }
             
            return redirect()->route('users.index')->with('success', __('messages.update', ['title' => 'User']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->delete()) {
            return redirect()->route('users.index')->with('success', __('messages.delete', ['title' => 'User']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = \Auth::user();
        return view('backend.user.profile', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request, User $user)
    {
        $user = Auth::user();
        $request->validate([
            'name' => 'required|min:2|max:50',
            'email' => 'required|email|unique:App\Models\User,email,' . $user->id,
            'avatar' => 'image'
        ]);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->about_me = $request->about_me;
        if ($user->save()) {
            return redirect()->back()->with('success', __('messages.update', ['title' => 'Profile']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
    /**
     * Show the form for update a resource password.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword()
    {
        return view('users.change-password');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateChangePassword(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'password' => 'required|min:8|max:30|confirmed',
        ]);
        $user->password = Hash::make($request->password);
        if ($user->save()) {
            return redirect()->back()->with('success', __('messages.update', ['title' => 'Change Password']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
}
