<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:role-list', ['only' => ['index']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $group_names = array();
        $roles = Role::where(function ($query) use ($request) {
            $s = $request->s;
            if ($s) {
                $query->where('name', 'LIKE', "%$s%");
            }
        })
            ->paginate();
        return view('roles.index', compact('roles', 'group_names'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create')->with(['permission_lists' => $this->permission()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'present|array'
        ]);
        $role = Role::create(['name' => $request->post('name')]);
        if ($role) {

            $role->syncPermissions($request->post('permission'));
            return redirect()->route('roles.index')->with('success', __('messages.insert', ['title' => 'Role']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $role_permissions = $role->getAllPermissions();
        $role_has_permissions = [];
        foreach ($role_permissions as $role_permission) {
            $role_has_permissions[$role_permission->id] = $role_permission->id;
        }

        return view('roles.edit', compact('role', 'role_has_permissions'))->with(['permission_lists' => $this->permission()]);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name,' . $role->id
        ]);
        $role->name = $request->post('name');
        if ($role->save()) {
            $role->syncPermissions($request->post('permission'));
            return redirect()->route('roles.index')->with('success', __('messages.update', ['title' => 'Role']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        if ($role->delete()) {
            return redirect()->route('roles.index')->with('success', __('messages.delete', ['title' => 'Role']));
        } else {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Permission List
     */
    private function permission()
    {
        $permission_lists = [];
        $permissions = Permission::select('id', 'group_name', 'name')
            ->orderBy('group_name')
            ->orderBy('name')
            ->get();
        if ($permissions) {
            foreach ($permissions as $permission) {
                $group_name = $permission->group_name;
                $permission_lists[$group_name][] = $permission;
            }
        }
        return $permission_lists;
    }
}
