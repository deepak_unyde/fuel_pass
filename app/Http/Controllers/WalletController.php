<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{User,Wallet}; 
use App\Http\Requests\{StoreWalletRequest, UpdateWalletRequest};


class WalletController extends Controller
{
    private $title,$users;

    public function __construct()
    {
        $this->title = "Wallet";
        $this->users = User::getAllAppUser();
        $this->middleware('permission:wallet-list', ['only' => ['index']]);
        $this->middleware('permission:wallet-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:wallet-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:wallet-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $wallets = Wallet::select('id', 'user_id', 'amount', 'created_at')
            ->with(['user' => function ($query) {
                $query->select('id', 'name','mobile');
            }])
            ->orderBy('id', 'desc')
            ->condition($request)
            ->paginate();
        return view('wallets.index', compact('wallets'))->with(['title' => $this->title,'users'=> $this->users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('wallets.create')->with(['title'=>$this->title, 'users'=>$this->users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreWalletRequest  $StoreWalletRequest
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWalletRequest $request)
    {
        $wallet = Wallet::create($request->all());
        if($wallet)
        {
            return redirect()->route('wallets.index')->with('success', __('messages.insert',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));                
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Wallet $wallet)
    {
        return view('wallets.edit',compact('wallet'))->with(['title'=>$this->title,'users'=>$this->users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWalletRequest $request, Wallet $wallet)
    {
        if($wallet->update($request->all())){
            return redirect()->route('wallets.index')->with('success', __('messages.update',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wallet $wallet)
    {
        if($wallet->delete())
        {
            return redirect()->route('wallets.index')->with('success',__('messages.delete',['title'=>$this->title]));
        }
        else
        {
            return redirect()->back()->with('error', __('messages.try_again'));
        }
    }
}
