<?php

namespace App\Http\Controllers;

use App\Models\{Rating,PetrolPump};
use Illuminate\Http\Request;
use DB;
class RatingController extends Controller
{
    private $title,$petrolPump;
    public function __construct()
    {
        $this->title = "User Rating";
        $this->petrolPump = PetrolPump::getAll();
       
      #  $this->middleware('permission:rating-list', ['only' => ['index']]); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $petrolPump = PetrolPump::select('petrol_pumps.id', 'company_id','beacon_id','price_id', 'code', 'name', 'address','pincode','latitude','longitude', 'status', 'petrol_pumps.created_at', DB::raw('ROUND(IFNULL(avg(rating),0),2) as rating'))
        ->with(['company' => function ($query) {
            $query->select('id', 'company_name');
             }])
            ->leftJoin('ratings', [['ratings.petrol_pump_id','=','petrol_pumps.id']])
            ->where('status', 1)
            ->groupBy('petrol_pumps.id')
            ->orderBy('id','desc')
            ->paginate();
       
       return view('ratings.index',compact('ratings'))->with(['title'=>$this->title]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function show(Rating $rating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function edit(Rating $rating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rating $rating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rating $rating)
    {
        //
    }
}
