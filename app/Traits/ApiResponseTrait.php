<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Response;

trait ApiResponseTrait
{

    protected function sendResponse($response)
    {
        $status = true;
        $errors = null;
        $message = null;
        $data = (object) [];
        $code = 200;

        if (is_array($response)) {
            if (array_key_exists('status', $response)) {
                $status = $response['status'];
            }
            if (array_key_exists('message', $response)) {
                $message = $response['message'];
            }
            if (array_key_exists('data', $response)) {
                $data = $response['data'];
            }
            if (array_key_exists('code', $response)) {
                $code = $response['code'];
            }
            if (array_key_exists('errors', $response)) {
                $errors = $response['errors'];
                if (!array_key_exists('status', $response)) {
                    $status = false;
                }
                if (!array_key_exists('message', $response)) {
                    $message = "Invalid input data.";
                }
            }
        }

        return response()->json([
            'status' => $status,
            'errors' => $errors,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    protected function errorResponse($errors = null, $message = null, $code = null)
    {
        return response()->json([
            'status' => 'Success',
            'error' => $errors,
            'message' => $message,
            'data' => null
        ], $code);
    }
}
