<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

//Scribe 
use Knuckles\Camel\Extraction\ExtractedEndpointData;
use Knuckles\Scribe\Scribe;
use App\Models\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        Paginator::useBootstrap();
        $limit = config('app.limit');
        $i = ($request->input('page', 1) - 1) * $limit;
        View::share(['i' => $i]);


        //DB query log into logs/laravel.log file
        if (app()->environment('local')) {
            DB::listen(function ($query) {
                $bindings = $query->bindings;
                $sql = $query->sql;
                $replaced = Str::replaceArray('?', $bindings, $sql);
                #Log::info($replaced);
            });
        }

        //prevent N+1 query
        Model::preventLazyLoading(!app()->isProduction());

        //Scribe Api Documentation
        // Be sure to wrap in a `class_exists()`, 
        // so production doesn't break if you installed Scribe as dev-only

        Scribe::beforeResponseCall(function (Request $request, ExtractedEndpointData $endpointData) {
            $token = User::first()->api_token;
            echo "es";
            exit;
            $request->headers->add(["Authorization" => "Bearer $token"]);
        });
    }
}
