<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    /**
     * Permission name
     */
    function permissionName($group_name)
    {
        return Permission::where(['group_name' => $group_name])->pluck('name', 'id');
    }
}
