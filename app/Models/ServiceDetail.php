<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'service_id',
        'slot_start_time', 
        'slot_end_time', 
        'slot_limit',
        'status'        
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
