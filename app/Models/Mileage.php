<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Mileage extends Model
{
    use HasFactory, SoftDeletes,LogsActivity;
    protected $fillable = [
        'user_id', 
        'meter_reading'
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y h:i a',
    ];
    function user()
    {
        return $this->belongsTo(User::class);
    }
    
    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $user_id = $request->user_id;
            $s = $request->s;
            if ($user_id) {
                $query->where('user_id', $user_id);
            }
        });
    }
}
