<?php

namespace App\Models;
 
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
class Service extends Model implements HasMedia
{
    use HasFactory,LogsActivity,InteractsWithMedia;
 
    protected $fillable = [
        'service_name',
        'amount', 
        'status', 
        'description',
        'short_description'        
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(128)
            ->height(128);
    }

    public function users()
    {
        
         return $this->belongsToMany(User::class)->withPivot(['id','service_id','user_id','pickup_address','service_date','slot_id','service_start_time','service_end_time','service_status','payment_no','payment_no','order_no','payment_date','payment_status'])->withTimestamps();
    }   
    /**
     * Get All
     */
    static function getAll($request = null)
    {
        return self::orderBy('service_name')->pluck('service_name','id');
    }
    static function scopeActive($query)
    {
       return $query->where('status', 1);
    }

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            
            $s = $request->s;
            if ($s) {
                $query->where('service_name', 'LIKE', "%$s%");
            }
            if ($request->has('status')) {
                $status = $request->status;
                if ($status) {
                    $query->where('status', $status);
                } else if ($status === '0') {
                    $query->where('status', $status);
                }
            }
            
        });
    }
}
