<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use DB;
class Logger extends Model
{
    protected $table = 'base_logger';
    protected $fillable = [
        'lati','longi','accuracy','vehicle_id','azimuth','elevation','cnhz','constellation_type','constellation_count','device_name'
    ];

    static function getAll($request = null)
    {
        return self::orderBy('id')->pluck('company_name', 'id');
    }

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $s = $request->s;
            $count = $request->count;
            $time = $request->time;
            $from_date = $request->fdate;
            $to_date =$request->tdate;
            $device_name = !empty($request->device_name)?join("','",$request->device_name):'';
           if(!empty($time) and $time != '1')
           {
              $datetime = date('Y-m-d H:i:s',strtotime("- $time min"));  
           }
           else
           {
              $datetime = date('Y-m-d H:i:s',strtotime('-30 min'));
           }
         // exit;
            if ($count) {
                $query->where(DB::raw("LENGTH(vehicle_id) - LENGTH(REPLACE(vehicle_id, ',', ''))"), ">=", $count);
            }
            if ($s) {
                $query->where('vehicle_id', 'LIKE', "%$s%");
            }
            if ($device_name) {
                $query->whereIn('device_name', $request->device_name);
            }
            if($from_date && $to_date)
            {
                $query->whereBetween('created_at', [
                    $from_date, $to_date
                  ]);    
            } 
           /* if(!empty($time) and $time != '1')
           {
            $query->where('created_at','>=', $datetime); 
           }
           if(empty($time))
           {
            $query->where('created_at','>=', $datetime); 
           }*/
            
        });
    }
}
