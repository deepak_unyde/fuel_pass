<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Carbon\Carbon;
class PetrolUser extends Model
{
    use HasFactory,SoftDeletes,LogsActivity;
    
    protected $fillable = ['user_id','package_id','petrol_pump_id','transaction_no','fuel_type','fuel_cost','fuel_quantity','amount', 'reward_point','cashback','status','payment_no','response'];
    protected $casts = [
        'created_at'=>'datetime:d-m-Y h:i a',
    ];
      #One to One Relation with Petrolpump
      public function petrolPump()
      {
          return $this->belongsTo(PetrolPump::class);
      }

      public function user()
      {
          return $this->belongsTo(User::class);
      }
      public function package()
      {
          return $this->belongsTo(Package::class);
      }

      

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $user_id = $request->user_id;
            $package_id = $request->get('package_id');
            $petrol_pump_id = $request->get('petrol_pump_id');

            $from_date = ($request->from_date)?Carbon::parse($request->from_date)
            ->toDateTimeString():'';
             $to_date = ($request->to_date)?Carbon::parse($request->to_date)
            ->toDateTimeString():'';
           
            if ($user_id) {
                $query->where('user_id', $user_id);
            }
            if ($package_id) {
                $query->where('package_id', $package_id);
            }
            if ($petrol_pump_id) {
                $query->where('petrol_pump_id', $petrol_pump_id);
            }
            if($from_date && $to_date)
            {
                $query->whereBetween('petrol_users.created_at', [
                    $from_date, $to_date
                  ]);    
            } 
            if ($request->status) {
                $query->where('petrol_users.status', $request->status);
            }

            if(loginUserCompanyId() && loginUserRoleId()==3)
            {
              //  $query->join('petrol_pumps','petrol_pumps.id','=','petrol_pump_id');
                $query->where('petrol_pumps.company_id','=',loginUserCompanyId());
                
            }
        });
    }

}
