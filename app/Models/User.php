<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\App;
use Laravel\Sanctum\HasApiTokens;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

use Spatie\Permission\Traits\HasRoles;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Yadahan\AuthenticationLog\AuthenticationLogable;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, InteractsWithMedia, HasRoles, SoftDeletes, AuthenticationLogable;
    use LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'company_id',
        'emp_code',
        'name',
        'email',
        'mobile',
        'password',
        'about_me',
        'address',
        'status'
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
     
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(128)
            ->height(128);
    }
    
    public function registerMediaCollections(): void
    {
        // echo config('app.avatar');
        // exit;
        $this
            ->addMediaCollection('avatar')
            ->useFallbackUrl(config('app.asset_url') . config('app.avatar'))
            ->singleFile();
    }

    // Cache permissions and roles
    public function getPermissionsAttribute()
    {
        $permissions = Cache::rememberForever('permissions_cache', function () {
            return Permission::select('permissions.*', 'model_has_permissions.*')
                ->join('model_has_permissions', 'permissions.id', '=', 'model_has_permissions.permission_id')
                ->get();
        });

        return $permissions->where('model_id', $this->id);
    }
 
    static function loginLog($request, $user_id)
    {
        $user_log = [];
        $user_log['authenticatable_type'] = "App\Models\User";
        $user_log['authenticatable_id'] = $user_id;
        $user_log['ip_address'] = $request->ip();
        $user_log['user_agent'] =  $request->userAgent();
        $user_log['login_from'] = "api";
        $user_log['extra_info'] = json_encode($request->extra_info);
        $user_log['login_at'] = Carbon::now();
        DB::table('authentication_log')->insert($user_log);
    }

     
    /**
     * Get All supervisor
     */
    static function getAllAppUser()
    {
        return self::where(['status' => 1])
            ->whereHas('roles', function ($query) {
                $query->where('role_id', 2);
            })
            ->orderBy('name')
            ->pluck('name', 'id');
    }

    /**
     * Scope a query to only include login user vendor id.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    
 
    static function userTokenDelete($user)
    {
        if ($user->status == 0) {
            $user->tokens()->delete();
        }
    }

    
    /* Define many to many relation */
     

    public function packages()
    {
         return $this->belongsToMany(Package::class)->withPivot(['package_id','user_id','transaction_id','from_date','to_date','cost','discount','discount_percentage', 'reward_point','cashback','status'])->withTimestamps();
    }  

     
    /**
     * @return HasOne
     * @description get the detail associated with the post
     */
    public function profile() 
    {
        return $this->hasOne(Profile::class);
    }

    public function redeem()
    {
         return $this->belongsToMany(RedeemGift::class)->withPivot(['redeem_gift_id','user_id','redeem_code','redeem_point','use_date','status'])->withTimestamps();
    }

    public function address()
    {
        return $this->hasMany(Address::class);
    }
    
    public function service()
    {
        
         return $this->belongsToMany(Service::class)->withPivot(['id','service_id','user_id','pickup_address','service_date','slot_id','service_start_time','service_end_time','service_status','payment_no','payment_no','order_no','payment_date','payment_status'])->withTimestamps();
    } 
}
