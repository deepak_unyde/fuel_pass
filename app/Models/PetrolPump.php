<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use DB;
class PetrolPump extends Model
{
    use HasFactory,SoftDeletes, LogsActivity;

    protected $fillable = [
        'company_id',
        'price_id',
        'code', 
        'beacon_id',
        'name',
        'address',
        'pincode',
        'latitude',
        'longitude',
        'opening_time',
        'closing_time',
        'status'
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y h:i a',
        
    ];
    // One to One Relation with office
    public function price()
    {
        return $this->belongsTo(Price::class);
    }

    
    /* Relation one-one with company */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get All
     */

    static function getAll($request = null)
    {
           
            if (createAction()) {
                return self::active()->orderBy('name')->pluck('name', 'id');
            }
            return self::where($request)->orderBy('name')->pluck('name', 'id');
       
    }



    static function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $company_id = $request->get('company_id');
            $s = $request->s;
            if ($company_id) {
                $query->where('company_id', $company_id);
            }
            if(loginUserCompanyId() && loginUserRoleId()==3)
            {
                $query->where('company_id',loginUserCompanyId());
            }
            if($request->price_id){
                $query->where('price_id', $request->price_id);
            }
            if ($s) {
                $query->where('name', 'LIKE', "%$s%");
            }
            if($request->code){
                $query->where('code','LIKE',"%$request->code%");
            }
            if ($request->has('status')) {
                $status = $request->status;
                if ($status) {
                    $query->where('status', $status);
                } else if ($status === '0') {
                    $query->where('status', $status);
                }
            }
        });
    }

    public function petrolPumpUserRating()
    {
        return $this->belongsToMany(PetrolPump::class,'ratings')->withPivot(['petrol_pump_id','user_id','rating'])->withTimestamps();
    }  

    static function getPetrolPump()
    {
        $whereArr = [];
        if(createAction()==false) 
        {
         $whereArr['status'] = 1; 
        }
         if(loginUserCompanyId())
        $whereArr['company_id'] = loginUserCompanyId();

        $petrolPump = self::select(DB::raw("CONCAT(name,' (',code,')') AS name"),'id')
       ->where($whereArr)
       ->orderBy('name')->pluck('name', 'id');
       return   $petrolPump;
    }
}
