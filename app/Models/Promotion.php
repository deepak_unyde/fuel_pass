<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
class Promotion extends Model implements HasMedia
{
    use HasFactory,SoftDeletes,InteractsWithMedia   ;
    
    protected $fillable = [
        'title', 
        'heading',
        'url',
        'from_date',
        'to_date',
        'short_description',
        'description',
        'status'
    ];
    protected $casts = [
        'from_date' => 'date:d-m-Y',
        'to_date' => 'datetime:d-m-Y',
        'created_at'=>'datetime:d-m-Y h:i a',
    ];
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(128)
            ->height(128);
    }
    static function getAll($request = null)
    {
          
        if (createAction()) {
            return self::active()->orderBy('title')->pluck('title', 'id');
        } 
         else {
            return self::orderBy('title')->pluck('title', 'id');
            }
    }

    static function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $s = $request->s;
            if ($s) {
                $query->where('title', 'LIKE', "%$s%");
            }
            if ($s) {
                $query->where('heading', 'LIKE', "%$s%");
            }
            if ($request->has('status')) {
                $status = $request->status;
                if ($status) {
                    $query->where('status', $status);
                } else if ($status === '0') {
                    $query->where('status', $status);
                }
            }
             
        });
    }
  
}
