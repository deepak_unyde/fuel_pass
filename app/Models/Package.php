<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Carbon\Carbon;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
class Package extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, LogsActivity,InteractsWithMedia;
    protected $fillable = [
        'package_name',
        'package_description',
        'from_date',
        'to_date',
        'duration',
        'cost',
        'discount',
        'discount_percentage',
        'reward_point',
        'cashback',
        'status'
    ];
    protected $casts = [
        'from_date' => 'date:d-m-Y',
        'to_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y h:i a',
    ];
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(128)
            ->height(128);
    }

   /* public function registerMediaCollections(): void
    {
        // echo config('app.avatar');
        // exit;
        $this
            ->addMediaCollection('package_image')
            ->useFallbackUrl(config('app.asset_url') . config('app.package_image'))
            ->singleFile();
    }*/
    static function getAll($request = null)
    {
        if (createAction()) {
            return self::active()->orderBy('package_name')->pluck('package_name', 'id');
        }
        return self::where($request)->orderBy('package_name')->pluck('package_name', 'id');
    }

    static function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $s = $request->s;
             $from_date = ($request->from_date)?Carbon::parse($request->from_date)
            ->toDateString():'';
             $to_date = ($request->to_date)?Carbon::parse($request->to_date)
            ->toDateString():'';
            if ($s) {
                $query->where('package_name', 'LIKE', "%$s%");
            }
            /*if($from_date && $to_date)
            {
                $query->whereBetween('created_at', [
                    $from_date, $to_date
                  ]);    
            }*/
            if($from_date)
            {
                $query->where('from_date', $from_date);    
            }
            if($to_date)
            {
                $query->where('to_date', $to_date);    
            }
            if ($request->has('status')) {
                $status = $request->status;
                if ($status) {
                    $query->where('status', $status);
                } else if ($status === '0') {
                    $query->where('status', $status);
                }
            }
        });
    }

    public function users()
    {
         return $this->belongsToMany(User::class)->withPivot(['id','package_id','user_id','transaction_id','from_date','to_date','cost','discount','discount_percentage', 'reward_point','cashback','status'])->withTimestamps();
    }  
}
