<?php

namespace App\Models; 
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
class Company extends Model  implements HasMedia
{
    use HasFactory,SoftDeletes,InteractsWithMedia;
    
    protected $fillable = [
        'company_name', 
        'company_mobile',
        'company_email',
        'company_address',
        'status'
    ];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y h:i a',
    ];
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(128)
            ->height(128);
    }
    static function getAll($request = null)
    {
          
        if (createAction()) {
            return self::active()->orderBy('company_name')->pluck('company_name', 'id');
        } 
         else {
            return self::orderBy('company_name')->pluck('company_name', 'id');
            }
    }

    static function scopeActive($query)
    {
       
        return $query->where('status', 1);
    }

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $s = $request->s;
            if ($s) {
                $query->where('company_name', 'LIKE', "%$s%");
            }
            if ($request->has('status')) {
                $status = $request->status;
                if ($status) {
                    $query->where('status', $status);
                } else if ($status === '0') {
                    $query->where('status', $status);
                }
            }
            if(loginUserCompanyId() && loginUserRoleId()==3)
            {
                $query->where('id',loginUserCompanyId());
            }
        });
    }

    static function getCompany()
    {
        $whereArr = [];
        if(createAction()==false) 
        {
        $whereArr['status'] = 1; 
        }
        if(loginUserCompanyId())
        {
        $whereArr['id'] = loginUserCompanyId();
        }
        $company = self::select('*')
       ->where($whereArr)
       ->orderBy('company_name')
       ->orderBy('company_name')->pluck('company_name', 'id');
       return   $company;
    }
}
