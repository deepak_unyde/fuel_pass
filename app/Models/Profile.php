<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model; 
use Spatie\Activitylog\Traits\LogsActivity;
use Carbon\Carbon;

class Profile extends Model
{
    use HasFactory,LogsActivity;
    protected $fillable = ['user_id','package_id','wallet_amount','reward_point','cashback','term_accepted'];

    public function user() 
    {
        return $this->belongTo(User::class);
    }
}
