<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Carbon\Carbon;

class Wallet extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;
    protected $fillable = [
        'user_id', 
        'amount'
    ];
    protected $casts = [
        'created_at'=>'datetime:d-M-Y h:i A',
    ];
     /* Relation one-one with company */
     public function user()
     {
         return $this->belongsTo(User::class);
     }

      
    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $user_id = $request->user_id;
            $s = $request->s;
            if ($user_id) {
                $query->where('user_id', $user_id);
            }
            
             
            
        });
    }
}
