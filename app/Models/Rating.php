<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Rating extends Model
{
    use HasFactory;
    protected $fillable = [
        'petrol_pump_id',
        'petrol_user_id',
        'user_id', 
        'rating'       
    ];
    /**
     * Get the user that owns the rating.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the petrolPump that made the review.
     */
    public function petrolPump()
    {
        return $this->belongsTo(PetrolPump::class);
    }

    public function petrolUser()
      {
          return $this->belongsTo(PetrolUser::class);
      }

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $user_id = $request->user_id;
            $petrol_pump_id = $request->petrol_pump_id;
            $petrol_user_id = $request->petrol_user_id;
           
            $from_date = ($request->from_date)?Carbon::parse($request->from_date)
            ->toDateTimeString():'';
             $to_date = ($request->to_date)?Carbon::parse($request->to_date)
            ->toDateTimeString():'';

            if ($user_id) {
                $query->where('user_id', $user_id);
            }
            if ($petrol_user_id) {
                $query->where('petrol_user_id', $petrol_user_id);
            }
            if ($petrol_pump_id) {
                $query->where('petrol_pump_id', $petrol_pump_id);
            }
            if($from_date && $to_date)
            {
                $query->whereBetween('created_at', [
                    $from_date, $to_date
                  ]);    
            }
            
        });
    }
}
