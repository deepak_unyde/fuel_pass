<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Carbon\Carbon;
class Payment extends Model
{
    use HasFactory,SoftDeletes,LogsActivity;
    protected $fillable = [
        'user_id','package_id','transaction_no','amount',' status','payment_no','response','payment_datetime'
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y h:i a',
        'payment_datetime'=>'datetime:d-m-Y h:i a',
    ];

    
     
}
