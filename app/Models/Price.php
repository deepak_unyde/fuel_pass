<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Price extends Model
{
    use HasFactory,LogsActivity;
    protected $fillable = [
        'state_name',
        'state_code', 
        'petrol_cost', 
        'diesel_cost',
        'cng_cost'        
    ];
    /**
     * Get All
     */
    static function getAll($request = null)
    {
        return self::orderBy('state_name')->pluck('state_name','id');
    }

    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $state_name = $request->get('state_name');
            $s = $request->s;
            
            if ($state_name) {
                $query->where('state_name', 'LIKE', "%$state_name%");
            }
            if ($s) {
                $query->where('state_code', 'LIKE', "%$s%");
            }
            
        });
    }
}
