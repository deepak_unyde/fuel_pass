<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
class RedeemGift extends Model implements HasMedia
{
    use HasFactory,SoftDeletes,InteractsWithMedia;
    protected $fillable = [
        'gift_name',
        'short_description',
        'from_date',
        'to_date',
        'min_gift_cost',
        'min_redeem_point',
        'redeem_cost',
        'valid_no_of_day',
        'total_coupon_qty',
        'use_coupon_qty',
        'balance_coupon_qty', 
        'status' 
    ];
    protected $casts = [
       // 'from_date' => 'date:d-m-Y',
        'to_date' => 'datetime:d-m-Y',
        'created_at'=>'datetime:d-m-Y h:i a',
    ];
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(128)
            ->height(128);
    }
    static function getAll($condition = null)
    {
        if (createAction()) {
            return self::active()->orderBy('gift_name')->pluck('gift_name', 'id');
        }  
         return self::where($request)->orderBy('gift_name')->pluck('gift_name', 'id');
    }

    static function scopeActive($query)
    {
        return $query->where('status', 1);
    }
   
    function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $s = $request->s;
            if ($s) {
                $query->where('gift_name', 'LIKE', "%$s%");
            }
            $from_date = ($request->from_date)?Carbon::parse($request->from_date)
            ->toDateTimeString():'';
             $to_date = ($request->to_date)?Carbon::parse($request->to_date)
            ->toDateTimeString():'';
           if($from_date && $to_date)
            {
                $query->whereBetween('to_date', [
                    $from_date, $to_date
                  ]);    
            }
            if ($request->has('status')) {
                $status = $request->status;
                if ($status) {
                    $query->where('status', $status);
                } else if ($status === '0') {
                    $query->where('status', $status);
                }
            }
        });
    }

    public function users()
    {
         return $this->belongsToMany(User::class)->withPivot(['redeem_gift_id','user_id','redeem_code','redeem_point','use_date','status'])->withTimestamps();
    }

}
