<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Otp extends Model
{
    use HasFactory;

    static function generateOtp($userId, $mobile, $otp)
    {
        $data = false;
        $otpObj = new Otp();
        $otpObj->user_id = $userId;
        $otpObj->otp = $otp;
        $otpObj->mobile = $mobile;
        $otpObj->expired_at =  Carbon::now()->addMinutes(2)->format('Y-m-d H:i:s');
        if ($otpObj->save()) {
            $data = true;
        }
        return $data;
    }

    // Function to generate OTP
    static function generateNumericOTP($smsOtp = true, $n = 4)
    {
        $result = "";
        if ($smsOtp) {
            $generator = "1357209468";
            for ($i = 1; $i <= $n; $i++) {
                $result .= substr($generator, (rand() % (strlen($generator))), 1);
            }
        } else {
            $result = 1234;
        }
        return $result;
    }

    /**
     * OTP verified
     */
    static function verify($mobile, $otp)
    {
        $data = false;
        $otp =  self::select('id', 'status', 'verified_at')
            ->where(['mobile' => $mobile, 'otp' => $otp])
            //->where('expired_at', '>=', now())
            ->where('created_at', '>=', subMinutes(2))
            ->latest()
            ->first();
        if ($otp) {
            $otp->status = inActive();
            $otp->verified_at = now();
            if ($otp->save()) {
                $data = true;
            }
        }
        return $data;
    }
}
