<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Vehicle extends Model
{
    use HasFactory,SoftDeletes,LogsActivity;
    protected $fillable = [
        'user_id', 
        'registration_number',
        'vehicle_type',
        'fuel_type',
        'term_accepted',
        'status'
    ];
    protected $casts = [
        
        'created_at'=>'datetime:d-m-Y h:i a',
    ];
     /* Relation one-one with Vehicle */
     public function user()
     {
         return $this->belongsTo(User::class);
     }

     function scopeCondition($query, $request)
    {
        $query->where(function ($query)  use ($request) {
            $user_id = $request->user_id;
            $s = $request->s;
            if ($user_id) {
                $query->where('user_id', $user_id);
            }
            if ($request->fuel_type) {
                $query->where('fuel_type', $request->fuel_type);
            }
            if ($request->vehicle_type) {
                $query->where('vehicle_type', $request->vehicle_type);
            }
            if ($s) {
                $query->where('registration_number', 'LIKE', "%$s%");
            }
            
        });
    }
}
