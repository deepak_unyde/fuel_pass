<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Link extends Component
{

    public $class_name;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($class = "btn-primary")
    {

        $this->class_name = $class;
        // print_r($class);
        // exit;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.link');
    }
}
