<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Table extends Component
{
    public $tableHeaders, $paginationData, $pagination;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($tableHeader, $data, $pagination = true)
    {
        $this->tableHeaders = $tableHeader;
        $this->paginationData = $data;
        $this->pagination = $pagination;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.table');
    }
}
