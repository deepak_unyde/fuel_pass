<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Dt extends Component
{
    /**
     * Public Datetime
     */
    public $datetime, $format;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($dateTime, $format = null)
    {
        $this->datetime = $dateTime;
        $this->format = $format;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.dt');
    }
}
