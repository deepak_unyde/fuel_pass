<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Button extends Component
{
    /**
     * Button Type
     */
    public $type;

    /**
     * Button Text
     */
    public $text;

    /**
     * Button Type
     */
    public $button_type;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type = "primary", $text = "Save", $buttonType = null)
    {
        $this->type = $type;
        $this->text = $text;
        $this->button_type = $buttonType;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.button');
    }
}
