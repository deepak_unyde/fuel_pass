<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Select extends Component
{
    /**
     * Input field name
     */
    public $name;

    /**
     * Input field title
     */
    public $title;

    /**
     * Label class
     */
    public $label_class = "";

    /**
     * Option list
     */
    public $options;

    /**
     * Select option selected value
     */
    public $selected = null;

    /**
     * Form field position 
     * @h for Horizontal Form
     * @v for Vertical Form
     */
    public $form_type;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $title, $options,  $formType = 'h', $selected = null)
    {
        $this->name  = $name;
        $this->title = $title;
        $this->options  = $options;
        $this->selected = $selected;
        $this->form_type = $formType;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.select');
    }
}
