<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    /**
     * Input field name
     */
    public $name;

    /**
     * Input field title
     */
    public $title;

    /**
     * Selected value
     */
    public $selected;

    /**
     * Label class
     */
    public $label_class = "";

    /**
     * Input Type
     */
    public $type;

    /**
     * Form field position 
     * @h for Horizontal Form
     * @v for Vertical Form
     */
    public $form_type;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $title, $selected = null, $type = null, $formType = "h")
    {
        $this->name  = $name;
        $this->title = $title;
        $this->selected = $selected;
        $this->type  = $type;
        $this->form_type = $formType;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input');
    }
}
