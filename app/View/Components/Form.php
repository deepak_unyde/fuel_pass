<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Form extends Component
{
    /**
     * Form end with button
     */
    public $with_button;

    /**
     * Form with model
     */
    public $model_name;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($withButton = "save", $modelName = null)
    {

        $this->with_button = $withButton;
        $this->model_name = $modelName;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form');
    }
}
