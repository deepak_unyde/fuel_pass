<?php 
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str; 
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
if (!function_exists('generateNumber')) {
    function generateNumber($sType=null,$output='Number')
    {
       
       /* $query = "CALL usp_GenerateNumber(:sType, @your_output_data);";
        $bind = [
            'sType' => 'ORDER'
        ];
        DB::statement($query, $bind);
        $return_query = "SELECT @your_output_data AS your_output_data";

        $result = DB::select($return_query);
        DB::statement($return_query);
        return $result; */  

        $query = "CALL usp_GenerateNumber('$sType', @Number)";
        DB::statement($query);
        $return_query = "SELECT @Number AS $output";
        $result = DB::select($return_query);
        DB::statement($return_query);
        return ($result)?$result[0]->$output:'';

    }
}

if (!function_exists('getProcedureData')) 
{
    function getProcedureData($statement)
    {
        $query = "CALL $statement";
        $result= DB::select($query);
        return $result;
    }
}
if (!function_exists('loginUserId')) {
    function loginUserId()
    {
        return Auth::id();
    }
}
if (!function_exists('loginUserRoleId')) 
{
    function loginUserRoleId()
    {
        return  Auth::user()->roles()->first()->id;  
    }
}
if (!function_exists('loginUserCompanyId')) 
{
    function loginUserCompanyId()
    {
        $company_id = (Auth::user()->company_id)?Auth::user()->company_id:0;
        return $company_id;
    }
}
if (!function_exists('loginUserFieldbyName')) 
{
    function loginUserFieldbyName($fieldName)
    {
        return Auth::user()->$fieldName;
    }
}
if (!function_exists('loginUserSessionArrayList')) 
{
    function loginUserSessionArrayList()
    {
        return Auth::user();
    }
}



 

function createAction()
{
    if (basename(url()->current()) == "create") {
        return true;
    }
}

 
/**
 * Get Random password
 */
function getRandomPassword($length = 8)
{
    $random = Str::random($length);
    return ['password' => $random, 'password_hash' => Hash::make($random)];
}

 


/**
 * Get date
 */
function getDateFromDate($date)
{
    if ($date) {
        return date('Y-m-d', strtotime($date));
    }
    return null;
}
/**
 * User Role
 */
function userRole()
{
    return 2;
}
 

/**
 * Check Attachment Input
 */
 

/**
 * Check Attachment Input
 */
function checkInputField($formField, $selectedFieldName)
{
    $input = "";
    if (is_array($formField)) {
        foreach ($formField as $key => $field) {
            if (is_array($field)) {
                $field = (object) $field;
            }
            $field_name = $field->field_name;
            if ($field_name == $selectedFieldName) {
                $input = (object) $field;
                break;
            }
        }
    }
    return $input;
}


 
function createdBy()
{
    return "Created By";
}

function updatedBy()
{
    return "Updated By";
}
 

/**
 * Category Form Field Option Value
 */
function optionValue($options)
{
    $data = [];
    if (is_array($options)) {
        foreach ($options as $option) {
            $optionValue = $option->value;
            $optionLabel = $option->text;
            $data[$optionValue] = $optionLabel;
        }
    }
    return $data;
}

if (!function_exists('vehicleType')) {
    function vehicleType()
    { 
        $vehicleType = array(
            'auto'=>'Auto Riksha',
            'bike'=>'Bike',
            'car'=>'Car',
            'scooter'=>'Scooter'
        );
        return $vehicleType;
    }
}

if (!function_exists('vehicleTypeList')) {
    function vehicleTypeList()
    { 
        $vehicleType = array(
            ['name'=>'Auto Riksha','value'=>'auto','icon_path'=>asset('img/auto.png')],
            ['name'=>'Bike','value'=>'bike','icon_path'=>asset('img/bike.png')],
            ['name'=>'Car','value'=>'car','icon_path'=>asset('img/logo.png')],
            ['name'=>'Scooter','value'=>'scooter','icon_path'=>asset('img/bike.png')] 
        );
        return $vehicleType;
    }
}

if (!function_exists('vehicleIcon')) {
    function vehicleIcon($value=null)
    { 
        $path = asset('img/default.png');
        if($value=='auto')
        {
            $path = asset('img/auto.png');
        }
        elseif($value=='bike')
        {
            $path = asset('img/bike.png'); 
        }
        elseif($value=='car')
        {
            $path = asset('img/logo.png'); 
        }
        elseif($value=='scooter')
        {
            $path = asset('img/bike.png');
        }
        
        return $path;
    }
}


if (!function_exists('fuelType')) {
    function fuelType()
    { 
        $fuelType = array(
            'cng'=>'CNG',
            'diesel'=>'Diesel',
            'petrol'=>'Petrol',
        );
        return $fuelType;
    }
}

function superAdmin()
{
    return 1;
}

function active()
{
    return 1;
}
function inActive()
{
    return 0;
}
function subMinutes($minutes)
{
    return  Carbon::now()->subMinutes($minutes)->format('Y-m-d H:i:s');
}