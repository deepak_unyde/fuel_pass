<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 
Route::get('/', function () {
    return redirect('login');
});

Auth::routes(['register' => true]);
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')
        ->name('home');
    Route::resource('users', 'UserController');
    Route::resource('wallets', 'WalletController');
    Route::resource('vehicles', 'VehicleController');
    Route::resource('mileages', 'MileageController');
    Route::resource('rewards', 'RewardController');
    Route::resource('redeem-gifts', 'RedeemGiftController');
    Route::resource('ratings', 'RatingController');


    Route::resource('companies', 'CompanyController');
    Route::resource('petrol-pumps', 'PetrolPumpController');
    Route::resource('packages', 'PackageController');

    Route::get('change-password', 'UserController@changePassword')->name('change-password');
    Route::post('change-password', 'UserController@updateChangePassword')->name('update-password');
    Route::resource('permissions', 'PermissionController');
   
    Route::resource('roles', 'RoleController');
    Route::resource('prices', 'PriceController');

    Route::resource('transactions', 'TransactionController');
    Route::resource('promotions', 'PromotionController');
    Route::get('ajax/request', 'CompanyController@ajaxRequest')->name('ajax.request');

    Route::resource('logger', 'LoggerController'); 
    Route::resource('services', 'ServiceController'); 
    Route::DELETE('service-detail/{serviceDetail}', 'ServiceController@delete')->name('services.delete');

    Route::post('add-service-detail', 'ServiceController@addServiceDetail')->name('services.add');
     

Route::get('logger-graph', 'LoggerController@graph')->name('logger.graph');

     
});
