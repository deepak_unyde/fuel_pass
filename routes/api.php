<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'middleware' => []], function () {
/** Service Product*/ 
Route::get('service-list', 'ServiceController@index'); 
Route::get('service-detail', 'ServiceController@serviceDetail'); 
Route::get('our-feature', 'PackageController@ourFeature'); 
Route::post('send-otp', 'UserController@sendOtp');
Route::post('verify-otp', 'UserController@otpVerify');

Route::get('petrol-pump', 'PetrolPumpController@index');
/** Service Product  */
    
    Route::post('login', 'UserController@login');
    Route::post('verify', 'UserController@verify');
    Route::post('metro', 'VehicleController@metro');
    Route::post('logger', 'VehicleController@logger');
    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::get('user', 'UserController@user');
        Route::get('profile', 'UserController@profile');
        Route::post('profile-update', 'UserController@updateProfile');
        Route::post('edit-profile', 'UserController@editProfile');

        Route::get('address', 'UserController@getAddress');
        Route::post('add-address', 'UserController@addAddress');
        Route::post('remove-address', 'UserController@deleteAddress');

        Route::get('home', 'UserController@home');
        Route::post('rating', 'UserController@rating');
        Route::get('package', 'PackageController@index'); 
       


        Route::resource('payment', 'PaymentController');
        Route::post('purchase', 'PaymentController@purchasePackage');
        Route::post('purchase-package', 'PaymentController@purchasePackageFromWallet');
        Route::get('initialize', 'PaymentController@initialize');
        Route::post('fuel-pay', 'PaymentController@purchasePetrol');
        Route::get('transaction', 'PaymentController@index');

        


        Route::get('wallet', 'WalletController@index');
        Route::post('add-wallet', 'WalletController@store');

        Route::get('vehicle', 'VehicleController@index');
        Route::get('fuel-type', 'VehicleController@fuelType');
        Route::get('vehicle-type', 'VehicleController@vehicleType');
        Route::post('add-vehicle', 'VehicleController@store');
       
        Route::get('fuel-price', 'PetrolPumpController@price');

        Route::get('redeem-offer', 'RedeemGiftController@index'); 
        Route::post('redeem', 'RedeemGiftController@redeemPoint'); 
        Route::get('redeem-list', 'RedeemGiftController@userRedeemList'); 
        
        
        Route::post('service-request', 'ServiceController@store');
        Route::get('user-service-list', 'ServiceController@userServiceList');

        Route::post('service-pay', 'PaymentController@servicePayment');
        Route::get('mileage', 'MileageController@index');
        Route::post('add-mileage', 'MileageController@store');


        Route::get('promotion', 'PackageController@promotion'); 
        Route::get('logout', 'UserController@logout');
    });
});
