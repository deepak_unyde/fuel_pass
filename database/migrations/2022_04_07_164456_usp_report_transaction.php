<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UspReportTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        DROP procedure IF EXISTS `usp_ReportTransaction`;
        CREATE PROCEDURE `usp_ReportTransaction`(`LoginId` INT, `RoleId` INT, `CompanyId` INT, `PetrolPumpId` INT,`PackageId` INT, `PaymentNo` VARCHAR(100), `FromDate` VARCHAR(30), `ToDate` VARCHAR(30), `sStatus` VARCHAR(50),`UserId` INT)
        BEGIN 
SET @t1 = CONCAT('SELECT 0 AS Seq, 
udf_GetGeneralFields(''PackageName'', package_id, 0)   as Package, udf_GetGeneralFields(''PetrolPumpCompanyName'', petrol_pump_id, 0)   as Company,  udf_GetGeneralFields(''PetrolPumpName'', petrol_pump_id, 0)   as Petrol_Pump, udf_GetGeneralFields(''UserName'', user_id, 0) as User, status, payment_no,  date_format(created_at, ''%d-%b-%Y'')  as created_at,
 fuel_quantity as Qty, fuel_cost, amount as Total_Cost, reward_point, cashback, id
from fu_petrol_users WHERE 1=1 '); 

IF PackageId <> '' and PackageId > 0 THEN
	SET @t1 = CONCAT(@t1, ' AND package_id = ', PackageId);
END IF;

IF CompanyId <> '' and CompanyId > 0 THEN
	SET @t1 = CONCAT(@t1, ' AND udf_GetGeneralFields(''PetrolPumpCompanyId'', petrol_pump_id, 0) = ', CompanyId);
END IF;
IF PetrolPumpId <> '' and PetrolPumpId > 0 THEN
	SET @t1 = CONCAT(@t1, ' AND petrol_pump_id = ', PetrolPumpId);
END IF;
IF sStatus <> '' THEN
	SET @t1 = CONCAT(@t1, ' AND status = ''', sStatus, '''');
END IF;

IF PaymentNo <> '' THEN
	SET @t1 = CONCAT(@t1, ' AND payment_no LIKE ''%', PaymentNo, '%'' ');
END IF; 

IF FromDate <> '' AND ToDate <> '' THEN
SET @t1 = CONCAT(@t1, ' AND date_format(created_at, ''%Y-%m-%d'')  BETWEEN ''', FromDate, ''' AND ''', ToDate, ''' ');
END IF;

IF UserId <> '' AND UserId > 0 THEN
SET @t1 = CONCAT(@t1, ' AND user_id = ', UserId);
END IF;
  
SET @t2 = CONCAT('SELECT 1 AS Seq, ''Total'' AS Package,
COUNT(*) AS Company,
'''' as Petrol_Pump,
'''' AS User,
'''' AS status,
'''' AS payment_no,
'''' AS created_at,
SUM(fuel_quantity) AS Qty,
SUM(fuel_cost) AS fuel_cost,
SUM(amount) AS Total_Cost,
SUM(reward_point) AS reward_point,
SUM(cashback) AS cashback,
id 
from fu_petrol_users WHERE 1=1');


IF PackageId <> '' and PackageId > 0 THEN
	SET @t2 = CONCAT(@t2, ' AND package_id = ', PackageId);
END IF;

IF CompanyId <> '' and CompanyId > 0 THEN
	SET @t2 = CONCAT(@t2, ' AND udf_GetGeneralFields(''PetrolPumpCompanyId'', petrol_pump_id, 0) = ', CompanyId);
END IF;
IF PetrolPumpId <> '' and PetrolPumpId > 0 THEN
	SET @t2 = CONCAT(@t2, ' AND petrol_pump_id = ', PetrolPumpId);
END IF;
IF sStatus <> '' THEN
	SET @t2 = CONCAT(@t2, ' AND status = ''', sStatus, '''');
END IF;

IF PaymentNo <> '' THEN
	SET @t2 = CONCAT(@t2, ' AND payment_no LIKE ''%', PaymentNo, '%'' ');
END IF; 

IF FromDate <> '' AND ToDate <> '' THEN
SET @t2 = CONCAT(@t2, ' AND date_format(created_at, ''%Y-%m-%d'')  BETWEEN ''', FromDate, ''' AND ''', ToDate, ''' ');
END IF;

IF UserId <> '' AND UserId > 0 THEN
SET @t2 = CONCAT(@t2, ' AND user_id = ', UserId);
END IF;

if RoleId = 1 THEN 
SET @t1 = CONCAT('SELECT Seq, Package, Company, Petrol_Pump, User , status, payment_no, created_at ,IFNULL(Qty,0) as Qty, IFNULL(fuel_cost,0) as fuel_cost, IFNULL(Total_Cost,0) as Total_Cost, IFNULL(reward_point,0) as reward_point, IFNULL(cashback,0) as cashback FROM (', @t1, ' UNION ALL ', @t2, ') A ORDER BY Seq ASC, id DESC');
else
SET @t1 = CONCAT('SELECT Seq,Package, Company, Petrol_Pump, User, payment_no, created_at ,IFNULL(Qty,0) as Qty, IFNULL(fuel_cost,0) as fuel_cost, IFNULL(Total_Cost,0) as Total_Cost, IFNULL(reward_point,0) as reward_point, IFNULL(cashback,0) as cashback FROM (', @t1, ' UNION ALL ', @t2, ') A ORDER BY Seq ASC, id DESC');
end if;

PREPARE stmt FROM @t1;
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;
         
        END;";



        \DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
