<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('package_name');
            $table->string('package_description',255)->nullable();
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('duration');
            $table->decimal('cost',10,2);
            $table->boolean('discount')->default(0);
            $table->integer('discount_percentage')->default(0);
            $table->decimal('reward_point',10,2)->default(0);
            $table->decimal('cashback',10,2)->default(0);
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamps(); 
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
