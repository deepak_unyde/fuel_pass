<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->unique(["user_id"]);
            $table->unsignedInteger('package_id')->default(0);
            $table->unsignedInteger('package_user_id')->default(0);
            $table->decimal('wallet_amount',10,2)->default(0);
            $table->decimal('reward_point',10,2)->default(0);
            $table->decimal('cashback',10,2)->default(0);
            $table->boolean('term_accepted')->default(1);
            $table->timestamps();
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            //$table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
