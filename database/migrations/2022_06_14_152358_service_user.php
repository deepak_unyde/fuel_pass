<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServiceUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('vehicle_id')->nullable() ;
            $table->unsignedInteger('service_id');
            $table->unsignedInteger('user_id');
            $table->string('latitude',30)->nullable();
            $table->string('longitude',30)->nullable();
            $table->string('pickup_address')->nullable();
            $table->date('service_date')->nullable();
            $table->unsignedInteger('slot_id')->nullable();
            $table->string('service_start_time')->nullable();
            $table->string('service_end_time')->nullable();
            $table->unsignedTinyInteger('service_status')->default(0);
            $table->decimal('payment_amount',10,2)->default(0);
            $table->string('payment_no',50)->nullable();
            $table->string('order_no',50)->nullable();
            $table->datetime('payment_date')->nullable();
            $table->string('payment_status',30)->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_user');
    }
}
