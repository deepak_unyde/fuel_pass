<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UdfGetGeneralFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       /* $procedure = "DROP function IF EXISTS `udf_GetGeneralFields`;
        CREATE FUNCTION `udf_GetGeneralFields`(`sType` VARCHAR(100), `Id` INT(10), `Id1` INT(10)) RETURNS varchar(200) CHARSET latin1
            NO SQL
        BEGIN
        DECLARE sText VARCHAR(200);
        IF sType = 'UserName' THEN
        BEGIN
         SELECT name INTO sText FROM fu_users WHERE fu_users.id = Id;
        END;
        ELSEIF sType = 'PackageName' THEN
        BEGIN
         SELECT package_name INTO sText FROM fu_packages WHERE fu_packages.id = Id;
        END;
        ELSEIF sType = 'CompanyName' THEN
        BEGIN
         SELECT company_name INTO sText FROM fu_companies WHERE fu_companies.id = Id;
        END;
        ELSEIF sType = 'PetrolPumpName' THEN
        BEGIN
         SELECT name INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        ELSEIF sType = 'PetrolPumpCode' THEN
        BEGIN
         SELECT code INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        ELSEIF sType = 'PetrolPumpAddress' THEN
        BEGIN
         SELECT address INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        
        ELSEIF sType = 'PetrolPumpPincode' THEN
        BEGIN
         SELECT pincode INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        ELSEIF sType = 'PetrolPumpBeaconId' THEN
        BEGIN
         SELECT beacon_id INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        
        ELSEIF sType = 'PetrolPumpLatitude' THEN
        BEGIN
         SELECT latitude INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        
        ELSEIF sType = 'PetrolPumpLongitude' THEN
        BEGIN
         SELECT longitude INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        
        ELSEIF sType = 'PetrolPumpStatus' THEN
        BEGIN
         SELECT status INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        
        ELSEIF sType = 'PetrolPumpPriceId' THEN
        BEGIN
         SELECT price_id INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
         
        
        ELSEIF sType = 'PetrolPumpState' THEN
        BEGIN
        SELECT state_name INTO sText FROM fu_prices WHERE fu_prices.id = (SELECT price_id FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id); 
        END;
        
        ELSEIF sType = 'PetrolPumpCompanyId' THEN
        BEGIN
         SELECT company_id INTO sText FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id;
        END;
        
        ELSEIF sType = 'PetrolPumpCompanyName' THEN
        BEGIN
        SELECT company_name INTO sText FROM fu_companies WHERE fu_companies.id = (SELECT company_id FROM fu_petrol_pumps WHERE fu_petrol_pumps.id = Id); 
        END;
         
        ELSE
         SET sText = '';
        END IF;
        
        RETURN sText;
        
        END;";



        \DB::unprepared($procedure); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
