<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GenerateUniqueNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "DROP PROCEDURE IF EXISTS `usp_GenerateNumber`;

        CREATE PROCEDURE `usp_GenerateNumber`(IN `sType` VARCHAR(45), OUT `sNumber` VARCHAR(45))
        NO SQL
    BEGIN
        DECLARE sNo INT DEFAULT 0;
        DECLARE sTransNo VARCHAR(6) DEFAULT '';
        DECLARE sMonth VARCHAR(6) DEFAULT '';
        DECLARE sYear VARCHAR(6) DEFAULT '';
        DECLARE sDay VARCHAR(6) DEFAULT '';
        SELECT form_no,form_no_format INTO sNo,sNumber FROM fu_transaction_numbers WHERE form_head COLLATE utf8mb4_unicode_ci = sType;
         
        SET sNo = sNo + 1;
         
        SET sTransNo = RIGHT(CONCAT('000000', CONVERT(sNo, CHAR)), 6);
        SET sDay = CONVERT(DAY(NOW()), CHAR);
        SET sDay = RIGHT(CONCAT('00', sDay), 2);
        SET sMonth = CONVERT(MONTH(NOW()), CHAR);
        SET sMonth = RIGHT(CONCAT('00', sMonth), 2);
        SET sYear = CONVERT(YEAR(NOW()), CHAR);
        SET sYear = RIGHT(CONCAT('0000', sYear), 4);
        SET sNumber = REPLACE(sNumber, '#DAY#', sDay);
        SET sNumber = REPLACE(sNumber, '#MONTH#', sMonth);
        SET sNumber = REPLACE(sNumber, '#YEAR#', sYear);
        SET sNumber = REPLACE(sNumber, '#NO#', sTransNo);
       
         UPDATE fu_transaction_numbers SET form_no = form_no + 1 WHERE form_head COLLATE utf8mb4_unicode_ci = sType;	
    
    END;";



        \DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
