<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetrolUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petrol_users', function (Blueprint $table) {
            $table->id(); 
            $table->integer('package_id');
            $table->integer('petrol_pump_id');
            $table->integer('user_id');
            $table->string('transaction_no',50);
            $table->string('fuel_type',20)->default('petrol');
            $table->decimal('fuel_quantity',10,2);
            $table->decimal('fuel_cost',10,2)->default(0);
            $table->decimal('amount',10,2)->default(0);
            $table->decimal('reward_point',10,2);
            $table->decimal('cashback',10,2);
            $table->string('status',20)->default('pending');
            $table->string('payment_no',50);
            $table->json('response')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petrol_user');
    }
}
