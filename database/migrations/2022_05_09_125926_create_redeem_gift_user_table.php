<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedeemGiftUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeem_gift_user', function (Blueprint $table) {
            $table->id();
            $table->integer('redeem_gift_id');
            $table->integer('user_id');
            $table->string('redeem_code');
            $table->unique('redeem_code');
            $table->decimal('redeem_point',5,2);
            $table->date('use_date')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeem_gift_user');
    }
}
