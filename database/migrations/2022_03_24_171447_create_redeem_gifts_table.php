<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedeemGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeem_gifts', function (Blueprint $table) {
            $table->id();
            $table->string('gift_name',100);
            $table->string('short_description',100);
            $table->date('from_date');
            $table->date('to_date');
            $table->decimal('min_gift_cost',10,2)->default(0);
            $table->decimal('min_redeem_point',10,2)->default(0);
            $table->decimal('redeem_cost',10,2)->default(0);
            $table->bigInteger('valid_no_of_day');
            $table->bigInteger('total_coupon_qty')->default(0);
            $table->bigInteger('use_coupon_qty')->default(0);
            $table->bigInteger('balance_coupon_qty')->default(0);
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeem_gifts');
    }
}
