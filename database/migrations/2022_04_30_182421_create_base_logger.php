<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseLogger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_logger', function (Blueprint $table) {
            $table->id();
            $table->string('lati')->nullable();
            $table->string('longi')->nullable();
            $table->string('accuracy')->nullable();
            $table->string('vehicle_id')->nullable();
            $table->string('azimuth')->nullable();
            $table->string('elevation')->nullable();
            $table->string('cnhz')->nullable();
            $table->string('constellation_type')->nullable();
            $table->string('constellation_count')->nullable();
            $table->string('device_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_logger');
    }
}
