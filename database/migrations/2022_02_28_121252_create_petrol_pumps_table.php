<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetrolPumpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petrol_pumps', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('price_id');
            $table->string('code',40);
            $table->string('beacon_id',80)->unique();
            $table->unique(["company_id", "code"]);
            $table->string('name',100);
            $table->string('address');
            $table->string('pincode',20);
            $table->decimal('latitude',10,8);
            $table->decimal('longitude',10,8);
            $table->time('opening_time');
            $table->time('closing_time');
            $table->unsignedTinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
           // $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petrol_pumps');
    }
}
