<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTransactionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_transaction_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('service_id');
            $table->unsignedInteger('service_user_id');
            $table->string('pay_source',50)->default('online');
            $table->decimal('amount',10,2)->default(0);
            $table->string('status',20)->default('pending');
            $table->string('payment_no',50)->nullable();
            $table->string('order_no',50)->nullable();
            $table->json('response')->nullable();
            $table->longText('request_payload')->nullable();
            $table->datetime('payment_datetime')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_transaction_user');
    }
}
