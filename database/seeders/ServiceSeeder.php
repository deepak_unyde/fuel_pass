<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Service,ServiceDetail};
use DB;
use Carbon\Carbon;
class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $now = Carbon::now();
         

        $ServiceData = [
            'service_name'=>'Wash Service',
            'amount'=>'1500',
            'convenience'=>49,
            'status'=>1,
            'short_description'=>'Wash Service',
            'description'=>'Wash Service',
        ];
        Service::create($ServiceData);
        $subzonedata[] = ['service_id' => 1, 'slot_start_time' => '06:00', 'slot_end_time'=>'09:00','slot_limit'=>'4'];
        $subzonedata[] = ['service_id' => 1, 'slot_start_time' => '09:00', 'slot_end_time'=>'12:00','slot_limit'=>'6'];
        $subzonedata[] = ['service_id' => 1, 'slot_start_time' => '12:00', 'slot_end_time'=>'15:00','slot_limit'=>'4'];
        $subzonedata[] = ['service_id' => 1, 'slot_start_time' => '15:30', 'slot_end_time'=>'18:30','slot_limit'=>'4'];
         
         foreach ($subzonedata as $pdata) {
            ServiceDetail::create($pdata);
        }
    }
}
