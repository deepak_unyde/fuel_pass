<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder; 
use App\Models\Company;
use DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Hash;
class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        $companyData = ['company_name' => 'Test Company', 'company_email' => 'company@unyde.in', 'company_mobile' => 8888888888, 'company_address' => 'Sector 132, Noida','created_at'=>$now,'updated_at'=>$now];
        Company::create($companyData);
  
        $data[] = ['company_id' => 1, 'code' => 'COM1-28022022-1', 'name' => 'Petrol Pump 1', 'address' => 'Delhi Public School,GBN, Sector 132, NOIDA','pincode'=>'201301', 'latitude' => '28.5158182','longitude'=>'77.3720684','created_at'=>$now,'updated_at'=>$now,'price_id'=>1,'beacon_id'=>rand(100000,999999),'opening_time'=>'04:00:00','closing_time'=>'23:59:00'];

         $data[] = ['company_id' => 1, 'code' => 'COM1-28022022-2', 'name' => 'Petrol Pump 2', 'address' => '62, Sector 63 Rd, A Block, Sector 62, Noida, Uttar Pradesh 201301', 'latitude' => '28.630665','pincode'=>'201301','longitude'=>'77.3799197','created_at'=>$now,'updated_at'=>$now,'price_id'=>1,'beacon_id'=>rand(100000,999999),'opening_time'=>'07:00:00','closing_time'=>'23:00:00'];
          
         foreach ($data as $pdata) {
            DB::table('petrol_pumps')->insert($pdata);
        }
   }
}
