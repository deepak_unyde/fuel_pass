<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            RoleSeeder::class,
            ApiKeySeeder::class,
            CompanySeeder::class,
            PackageSeeder::class,
            RedeemGiftSeeder::class,
            PromotionSeeder::class, 
            ServiceSeeder::class, 
            
        ]);
    }
}
