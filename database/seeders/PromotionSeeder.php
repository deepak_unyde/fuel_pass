<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Promotion;
use DB;
use Carbon\Carbon;
 

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        #1
        $promotion = [
        'title'=>'Unyde System 1', 
        'heading'=>'Testing Promotion 1',
        'url'=>"http://www.google.co.in",
        'from_date'=>date('Y-m-d'),
        'to_date'=>date('Y-m-d',strtotime('+15 days')),
        'short_description'=>'NA',
        'description'=>'NA',
        'status'=>1,
        'created_at'=>$now,
        'updated_at'=>$now
        ];
        Promotion::create($promotion);
        #2
        $promotion = [
            'title'=>'Unyde System 2', 
            'heading'=>'Testing Promotion 2',
            'url'=>"http://www.facebook.com",
            'from_date'=>date('Y-m-d'),
            'to_date'=>date('Y-m-d',strtotime('+20 days')),
            'short_description'=>'NA',
            'description'=>'NA',
            'status'=>1,
            'created_at'=>$now,
            'updated_at'=>$now
            ];
            Promotion::create($promotion);

            #3
            $promotion = [
                'title'=>'Unyde System 3', 
                'heading'=>'Testing Promotion 3',
                'url'=>"http://www.gmail.com",
                'from_date'=>date('Y-m-d'),
                'to_date'=>date('Y-m-d',strtotime('+30 days')),
                'short_description'=>'NA',
                'description'=>'NA',
                'status'=>1,
                'created_at'=>$now,
                'updated_at'=>$now
                ];
                Promotion::create($promotion);
    }
}
