<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\{Role, Permission};
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Models\{User, Profile};

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        //Admin role and user create
        $role = Role::create(['name' => 'Admin', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now]);
        $permissoins = Permission::where('admin', 1)->pluck('id', 'id');
        $role->syncPermissions($permissoins);

        $userData = ['company_id'=>0, 'name' => 'Admin', 'email' => 'admin@unyde.in', 'mobile' => 1234567890, 'password' => Hash::make('admin@123')];
        $user = User::create($userData);
        $user->assignRole($role);


         

        //User role and user create
        $role = Role::create(['name' => 'User', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now]);
        $permissoins = Permission::where('user', 1)->pluck('id', 'id');
        $role->syncPermissions($permissoins);
        $userData = ['company_id'=>0,'name' => 'User', 'email' => 'user@unyde.in', 'mobile' => 9999999999, 'password' => Hash::make('user@123')];
        $user = User::create($userData);
        $user->profile()->updateOrCreate(['user_id'=>$user->id],['user_id'=>$user->id,'package_id'=>1,'package_user_id'=>1,'term_accepted'=>1,'reward_point'=>'60.25','cashback'=>'6.03','wallet_amount'=>'3850']);
        $user->assignRole($role);


        /**
         * Company role and user create
         */
        $role = Role::create(['name' => 'Company', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now]);
        $permissoins = Permission::where('company', 1)->pluck('id', 'id');
        $role->syncPermissions($permissoins);
        $userData = ['company_id' => 1, 'name' => 'Test Company','email' => 'company@unyde.in', 'mobile' => 8888888888, 'password' => Hash::make('company@123')];
        $user = User::create($userData);
        $user->assignRole($role);


        /**
         * Petrol Pump role and user create
         *  
        $role = Role::create(['name' => 'Petrol Pump', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now]);
        $permissoins = Permission::where('petrol_pump', 1)->pluck('id', 'id');
        $role->syncPermissions($permissoins);
        $userData = ['name' => 'Test Company', 'company_id' => 1, 'email' => 'company@unyde.in', 'mobile' => 8888888888, 'password' => Hash::make('company@123')];
        $user = User::create($userData);
        $user->assignRole($role);
        */


      
    }
}
