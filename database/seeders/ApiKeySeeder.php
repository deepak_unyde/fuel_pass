<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ApiKey;

class ApiKeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $key = "Fb60rZsq3MWOApbWtwP9KY2dcUxUrJHptWNtfJRTfZ4BdI0T52OI2oDSaBVgMx1K";
        $data = ['name' => 'android', 'key' => $key];
        ApiKey::create($data);
    }
}
