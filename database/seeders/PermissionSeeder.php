<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        //Permission management
        $data[] = ['name' => 'permission-list', 'group_name' => 'Permission', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'permission-create', 'group_name' => 'Permission', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'permission-edit', 'group_name' => 'Permission', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'permission-delete', 'group_name' => 'Permission', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

        //Role management
        $data[] = ['name' => 'role-list', 'group_name' => 'Role', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'role-create', 'group_name' => 'Role', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'role-edit', 'group_name' => 'Role', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'role-delete', 'group_name' => 'Role', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

        //User management
        $data[] = ['name' => 'user-list', 'group_name' => 'User', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'user-create', 'group_name' => 'User', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'user-edit', 'group_name' => 'User', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'user-delete', 'group_name' => 'User', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'user-view', 'group_name' => 'User', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        $data[] = ['name' => 'user-change-password', 'company' => 1, 'petrol_pump' => 1, 'group_name' => 'User', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

        
        //Home or admin dashboard 
        $data[] = ['name' => 'home', 'group_name' => 'Home', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];


         //Company Master
         $data[] = ['name' => 'company-list', 'group_name' => 'Company', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now,'company' => 1];
         $data[] = ['name' => 'company-create', 'group_name' => 'Company', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'company-edit', 'group_name' => 'Company', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now,'company' => 1];
         $data[] = ['name' => 'company-delete', 'group_name' => 'Company', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now,'company' => 0];

        /* Package Plan */
         $data[] = ['name' => 'package-list', 'group_name' => 'Package Plan', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'package-create', 'group_name' => 'Package Plan', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'package-edit', 'group_name' => 'Package Plan', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'package-delete', 'group_name' => 'Package Plan', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        
         /* Petrol Pump */
         $data[] = ['name' => 'petrol-pump-list', 'group_name' => 'Petrol Pump', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now,'company' => 1];
         $data[] = ['name' => 'petrol-pump-create', 'group_name' => 'Petrol Pump', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now,'company' => 1];
         $data[] = ['name' => 'petrol-pump-edit', 'group_name' => 'Petrol Pump', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now,'company' => 1];
         $data[] = ['name' => 'petrol-pump-delete', 'group_name' => 'Petrol Pump', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now,'company' => 1];


         //Wallet Master
         $data[] = ['name' => 'wallet-list', 'group_name' => 'Wallet', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'wallet-create', 'group_name' => 'Wallet', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'wallet-edit', 'group_name' => 'Wallet', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'wallet-delete', 'group_name' => 'Wallet', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

         //Vehicle Master
         $data[] = ['name' => 'vehicle-list', 'group_name' => 'Vehicle', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'vehicle-create', 'group_name' => 'Vehicle', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'vehicle-edit', 'group_name' => 'Vehicle', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'vehicle-delete', 'group_name' => 'Vehicle', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

         //Reward Master
         $data[] = ['name' => 'reward-list', 'group_name' => 'Reward', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'reward-create', 'group_name' => 'Reward', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'reward-edit', 'group_name' => 'Reward', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'reward-delete', 'group_name' => 'Reward', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

         //Reward Master
         $data[] = ['name' => 'mileage-list', 'group_name' => 'Mileage', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'mileage-create', 'group_name' => 'Mileage', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'mileage-edit', 'group_name' => 'Mileage', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'mileage-delete', 'group_name' => 'Mileage', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

         $data[] = ['name' => 'price-list', 'group_name' => 'Fuel Price', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'price-create', 'group_name' => 'Fuel Price', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'price-edit', 'group_name' => 'Fuel Price', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'price-delete', 'group_name' => 'Fuel Price', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];


         $data[] = ['name' => 'coupon-list', 'group_name' => 'Redeem Gift', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'coupon-create', 'group_name' => 'Redeem Gift', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'coupon-edit', 'group_name' => 'Redeem Gift', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'coupon-delete', 'group_name' => 'Redeem Gift', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];


         $data[] = ['name' => 'company_level', 'admin' => 0, 'company' => 1, 'group_name' => 'Company Level', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'petrol_pump_level', 'admin' => 0, 'petrol_pump' => 1, 'group_name' => 'Petrol Pump Level', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

         $data[] = ['name' => 'transaction-list', 'group_name' => 'Transaction History', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now, 'admin' => 1, 'company' => 1];



         ########## Promotion ##########
         $data[] = ['name' => 'promotion-list', 'group_name' => 'Promotion', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'promotion-create', 'group_name' => 'Promotion', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'promotion-edit', 'group_name' => 'Promotion', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'promotion-delete', 'group_name' => 'Promotion', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];


         ########## Services ##########
         $data[] = ['name' => 'service-list', 'group_name' => 'Services', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
        # $data[] = ['name' => 'service-create', 'group_name' => 'Services', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'service-edit', 'group_name' => 'Services', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];
         $data[] = ['name' => 'service-delete', 'group_name' => 'Services', 'guard_name' => 'web', 'created_at' => $now, 'updated_at' => $now];

        foreach ($data as $pdata) {
            Permission::insert($pdata);
        }
    }
}
