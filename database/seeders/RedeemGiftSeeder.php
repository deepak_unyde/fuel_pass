<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RedeemGift;
use DB;
use Carbon\Carbon;

class RedeemGiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        #1
        $couponData = [
            'gift_name'=>'Coupon 1',
            'short_description'=>'Coupon 1',
            'from_date'=>date('Y-m-d'),
            'to_date'=>date('Y-m-d', strtotime('+30 days')),
            'min_gift_cost'=>250,
            'min_redeem_point'=>1000,
            'redeem_cost'=>1000,
            'valid_no_of_day'=>5,
            'total_coupon_qty'=>100,
            'balance_coupon_qty'=>100,
            'status'=>1
        ];
        RedeemGift::create($couponData);
        #2
        $couponData = [
            'gift_name'=>'Test Coupon',
            'short_description'=>'Coupon 1',
            'from_date'=>date('Y-m-d'),
            'to_date'=>date('Y-m-d', strtotime('+30 days')),
            'min_gift_cost'=>500,
            'min_redeem_point'=>2000,
            'redeem_cost'=>1000,
            'valid_no_of_day'=>10,
            'total_coupon_qty'=>10,
            'balance_coupon_qty'=>10,
            'status'=>1
        ];
        RedeemGift::create($couponData);
        #3
       /* $couponData = [
            'gift_name'=>'Demo Coupon 1',
            'short_description'=>'Demo Coupon 1',
            'from_date'=>date('Y-m-d'),
            'to_date'=>date('Y-m-d', strtotime('+30 days')),
            'min_gift_cost'=>250,
            'min_redeem_point'=>2000,
            'redeem_cost'=>1000,
            'valid_no_of_day'=>15,
             'total_coupon_qty'=>5,
            'balance_coupon_qty'=>5,
            'status'=>1
        ];
        RedeemGift::create($couponData);*/
    }
}
