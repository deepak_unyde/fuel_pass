<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Package;
use DB;
use Carbon\Carbon;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        ####### Create Package ###
        $data = ['package_name' => 'Test 1', 'package_description' => 'NA','duration' => '10', 'cost' => '1000', 'discount' => '0','discount_percentage'=>'0', 'cashback' => '0.50','reward_point'=>'5','from_date'=>date('Y-m-d'),'to_date'=>date('Y-m-30'), 'created_at'=>$now,'updated_at'=>$now];
        Package::create($data);
        
       ####### Add Wallet Against user id 2 ###  
       DB::table('wallets')->insert(
            ['user_id' => 2,'amount' => '5000','created_at' => $now,'updated_at' => $now]
        );

        ####### Add mileages Against user id 2 ###
        DB::table('mileages')->insert(
            ['user_id' => 2,'meter_reading' => '1000','created_at' => $now,'updated_at' => $now]
        );

        ####### Add vehicles Against user id 2 ###
        DB::table('vehicles')->insert(
            ['user_id' => 2,'registration_number' => '1000','vehicle_type' => 'bike','fuel_type' => 'petrol','term_accepted'=>true,'created_at' => $now,'updated_at' => $now]
        );

        ########## Purchase Package Against User Id 2#########
        DB::table('payments')->insert(
            ['user_id' => 2,'package_id'=>1,'transaction_no' =>'UNYDE/TRANS/'.date('Ymd').'/000001','amount' => 1000.00,'payment_no' =>'UNYDE/RECPT/'.date('Ymd').'/000001','status' => 'success','payment_datetime'=>$now, 'created_at' => $now,'updated_at' => $now]
        );
        ####### Package against user id 2 ###
        DB::table('package_user')->insert(
            ['user_id' => 2,'package_id'=>1,'transaction_id' =>1,'from_date' =>date('Y-m-d'),'to_date' =>date('Y-m-d',strtotime('+10 days')),'cost' => 1000,'discount' => 0,'discount_percentage'=>0,'reward_point' =>5,'cashback' =>0.50,'created_at' => $now,'updated_at' => $now]
        );
        
        ####### Payment against fuel by user id 2 ###
        DB::table('petrol_users')->insert(
            [['user_id' => 2,'package_id'=>1,'petrol_pump_id'=>1,'transaction_no' =>'FUEL/TRANS/'.date('Ymd').'/000001','fuel_quantity' => '12.05','fuel_cost' =>'95.41','amount' => 1150.00,'payment_no' => 'RECPT202203030001','status' => 'success',  'reward_point'=>'60.25', 'cashback'=>'6.03', 'created_at' => $now,'updated_at' => $now]]
        );
        ####### Insert Fuel Price  ###
        DB::table('prices')->insert(
            ['state_name' =>'New Delhi','state_code' =>'DEL','petrol_cost' => 95.41,'diesel_cost'=>86.67,   'cng_cost'=>57.01,'created_at' => $now,'updated_at' => $now]
        );

        ####### Transaction Number #######
        DB::table('transaction_numbers')->insert(
            [['form_head' => 'PACKAGEORDERONLINE','form_no'=>1,'form_no_format' =>'UNYDE/P/#YEAR#/#MONTH#/#DAY#/#NO#'],
            ['form_head' => 'SERVICES','form_no'=>0,'form_no_format' =>'SERVICE/ORDER/#YEAR#/#MONTH#/#DAY#/#NO#'],
            ['form_head' => 'FUELPAYMENTWALLET','form_no'=>0,'form_no_format' =>'RECP/FUEL/#YEAR#/#MONTH#/#DAY#/#NO#'],
            ]
        );



    }
}
