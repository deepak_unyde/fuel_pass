<!DOCTYPE html>
<html lang="en">
<?php
include('config.php'); 
$eSql =  mysqli_query($conn,"select * from $table_metro order by id desc");
$num_result = mysqli_num_rows($eSql);
?>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width,height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
  <title>Metro</title>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <style>
  table, td, th {
    border: 1px solid black;
  }
  tr
  {
  	height:30px;
  }
  table {
    width: 100%;
      padding:0;
  	text-align:center;
  }
  </style>
  <script type="text/javascript">
      $(document).ready(function () {
      setTimeout(function () {
        location.reload(true);
      }, 50000);
    });
  </script>
</head>
<body>
  <h3 style="text-align:center; margin-top:20px;margin-botton:30px;"> Packet Data List</h3>
  <div class="split-table">
    <table border="4"  style="border-collapse: collapse;" bordercolorlight="#FF0000" cellpadding="0" cellspacing="0">
      <tr>
        <th>#</th>
        <th>Packet Data</th>
        <th>Insert Datetime</th>
      </tr>
      <?php if($num_result > 0){ $i=1; while ($data =mysqli_fetch_object($eSql))
    	{?>
      <tr>
        <td><?=$i?></td>
        <td><?=$data->packet_data?></td>
        <td><?=$data->created_at?></td>
      </tr>
    <?php $i++; }} else {?>
    <tr>
      <td colspan=3>No data found</td>
    </tr>
    <?php }?>
    </table>
  </div>  
</body>
</html>